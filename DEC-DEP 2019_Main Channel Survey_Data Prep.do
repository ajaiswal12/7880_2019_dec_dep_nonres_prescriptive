/***************************************
Created By: Evan Tincknell
Creation Date: 2019-09-16

Last Modified By: Sher Khashimov
Modified Date: 2019-11-5

Preparation of Duke DEC-DEP NRP Main Channel survey data for
--Part Survey Dispo Summary
--Part Survey FR Analysis
--Part Survey SO Analysis
--Part Survey Process Analysis

***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths ("Globals")
global main "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019"
global work "$main\B-Survey Data\Main Channel\Working Files"
global map "$main\B-Survey Data\Main Channel\Mapping Files"
global clean "$main\B-Survey Data\Main Channel\Analysis Files"
global raw "$main\B-Survey Data\Main Channel\Raw Files"
global sample "$main\B-Sampling\Participant Surveys"
global syntax "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\Data Cleaning\Syntax"
exit



*******************
***FILE PREP***
*******************

***Import the final survey data***
import delimited using "$raw\Duke_Energy_Main_Channel_Participant_2019-10-28.csv", clear 

***Dropping and re-ordering variables in raw data***
drop ïxid start_device start_apple_device sample_pii_program1 sample_pii_program2 sample_pii_meas_count start_time end_time sample_provider_id reentry duration language ///
	 last_activity_time uas_start start_browser_type_version start_os_version uas_end end_browser_type_version end_os_version sample_pii_date
rename (sample_id sample_pii_name sample_pii_address sample_pii_company sample_pii_tech sample_pii_meas_a sample_pii_meas_b sample_pii_meas_c sample_pii_meas_d sample_pii_meas_e) (odcid name address company tech meas_a meas_b meas_c meas_d meas_e)
order odcid name company 
save "$work/DEC-DEP Main Channel Survey_RAW.dta", replace

***Import the field tracker***
import excel using "$sample\DEC-DEP Main Channel Sample_FINAL_2019-10-18.xlsx", clear firstrow 
rename *, lower
gen bounced = 1 if emailstatus1025 == "undeliverable"
keep odcid proj_tech account_id ex_ante_savings bounced stratum email name company address date tech meas_a meas_b meas_c meas_d meas_e meas_count  
merge 1:1 odcid using "$work/DEC-DEP Main Channel Survey_RAW.dta", nogen
save "$work/DEC-DEP Main Channel Survey_RAW_Tracker.dta", replace



***************
***DATA PREP***
***************

use "$work/DEC-DEP Main Channel Survey_RAW_Tracker.dta", clear

//Incorporating DK/NA flags
*br *98* *96* *998* *9998* to see what variables need to be groupped together
replace n5b_1 = 998 if n5b_998 == 1
rename n5b_1 n5b
drop n5b_998

replace lo1_1 = 998 if lo1_998 == 1
rename lo1_1 lo1
drop lo1_998

replace f3a_1 = 9998 if f3a_9998 == 1
rename f3a_1 f3a
drop f3a_9998

//Generate new flags
gen f3a_new_fl = 1 if f3a < 50 & f3a > 0
replace f3a_new_fl = 2 if f3a > 49 & f3a < 9998
replace f3a_new_fl = 1 if f3b < 3
replace f3a_new_fl = 2 if f3b > 2 & f3b < 8
replace f3a_new_fl = . if f3a == .
replace f3a_new_fl = . if f3a == 0 
replace f3a_new_fl = . if f3b == 8
order f3a_new_fl, after(f3a)

gen strata = "L1" if stratum == "DEC-L1" | stratum == "DEP-L1"
replace strata = "L2" if stratum == "DEC-L2" | stratum == "DEP-L2" 
replace strata = "L3" if stratum == "DEC-L3" | stratum == "DEP-L3"
replace strata = "NL1" if stratum == "DEC-NL1" | stratum == "DEP-NL1"
replace strata = "NL2" if stratum == "DEC-NL2" | stratum == "DEP-NL2" 
replace strata = "NL3" if stratum == "DEC-NL3" | stratum == "DEP-NL3"
replace strata = "NL4" if stratum == "DEC-NL4" | stratum == "DEP-NL4"

gen jurisdiction = 1 if stratum == "DEC-L1" 
replace jurisdiction = 1 if stratum == "DEC-L2"
replace jurisdiction = 1 if stratum == "DEC-L3"
replace jurisdiction = 1 if stratum == "DEC-NL1"
replace jurisdiction = 1 if stratum == "DEC-NL2"
replace jurisdiction = 1 if stratum == "DEC-NL3"
replace jurisdiction = 1 if stratum == "DEC-NL4"
replace jurisdiction = 2 if stratum == "DEP-L1"
replace jurisdiction = 2 if stratum == "DEP-L2"
replace jurisdiction = 2 if stratum == "DEP-L3"
replace jurisdiction = 2 if stratum == "DEP-NL1"
replace jurisdiction = 2 if stratum == "DEP-NL2"
replace jurisdiction = 2 if stratum == "DEP-NL3"
replace jurisdiction = 2 if stratum == "DEP-NL4"

//Use "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\C-Process Analysis\Analysis Files\
	  ///DEC-DEP 2019 Participant Survey Weight Development_2019-10-23.xlsx" to generate weight flags
gen weight = 0.35 if stratum == "DEC-L3" 
replace weight = 0.73 if stratum == "DEC-L2" 
replace weight = 2.5 if stratum == "DEC-L1"
replace weight = 0.73 if stratum == "DEC-NL4" 
replace weight = 2.5 if stratum == "DEC-NL3"
replace weight = 0.73 if stratum == "DEC-NL2" 
replace weight = 2.5 if stratum == "DEC-NL1"
replace weight = 0.30 if stratum == "DEP-L3" 
replace weight = 0.39 if stratum == "DEP-L2"
replace weight = 0.63 if stratum == "DEP-L1"
replace weight = 0.73 if stratum == "DEP-NL4" 
replace weight = 2.5 if stratum == "DEP-NL3"
replace weight = 0.73 if stratum == "DEP-NL2" 
replace weight = 2.5 if stratum == "DEP-NL1"

//Export for backcoding
export excel using "$map\DEC-DEP Main Channel Backcoding.xlsx", sheetreplace firstrow(variable) sheet("Original data")

//After the team manually recoded open end responses, we're importing recoded data back
import excel using "$map\DEC-DEP Main Channel Backcoding.xlsx", clear firstrow sheet("Recoded data")
save "$work/DEC-DEP Main Channel Survey_Backcoded_Recoded.dta", replace



*******************
***DISPO SUMMARY***
*******************

//Dispo summary coding 
use "$work/DEC-DEP Main Channel Survey_Backcoded_Recoded.dta", clear
gen dispo = "Complete" if terminate_location == 0
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == 2
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -302
replace dispo = "Partial complete - survey eligibility unknown" if terminate_location == -1
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -2
replace dispo = "Screened out - no knowledgeable contact" if terminate_location == 1
*replace dispo = "Screened out - no knowledgeable contact" if sc2 == 1 & sc3_pii_1_1 == ""
replace dispo = "Bounced" if bounced == 1 & dispo == ""
*replace dispo = "No response" if autoreply == 1 & dispo == ""
replace dispo = "No response" if dispo == ""
*replace dispo = "Added to DNC list" if do_not_contact == 1

//Dispo category coding
gen dispo_category = "I" if dispo == "Complete"
replace dispo_category = "N" if dispo == "Partial complete - survey eligibility confirmed"
replace dispo_category = "U1" if dispo == "Partial complete - survey eligibility unknown"
replace dispo_category = "U1" if dispo == "No response"
replace dispo_category = "U1" if dispo == "Added to DNC list"
replace dispo_category = "X1" if dispo == "Screened out - no knowledgeable contact"
replace dispo_category = "X2" if dispo == "Bounced"
drop if odcid > 999996
order odcid account_id proj_tech name company email address
save "$work/DEC-DEP Main Channel Survey_Final.dta", replace



***************
***DATA EXPORT FOR ANALYSIS***
***************

//Export for dispo summary
use "$work/DEC-DEP Main Channel Survey_Final.dta", clear
order name email acct_id company dispo 
keep name - dispo
export excel using "$main\Dispo Summary\DEC-DEP Main Channel Survey_Dispo Summary.xlsx", firstrow(variable)

//Export for WINCROSS
use "$work/DEC-DEP Main Channel Survey_Final.dta", clear
keep if dispo == "Complete"
drop acct_id name email company start_time end_time dispo
order stratum, before(strata)
export excel using "$clean\DEC-DEP Main Channel Survey_WINCROSS Export.xlsx", replace firstrow(variable)

//Export for SO analysis
use "$work/DEC-DEP Main Channel Survey_Final.dta", clear
drop if sp0 == .
drop acct_id name email company purchase_date stratum start_time end_time adjustedduration dispo strata jurisdiction weight passedfirstscreen sc1 sc2 sc3_pii_1_1 sc3_pii_1_2 ///
	quota_check i1_a i2_a i1_b i2_b i1_c i2_c i1_d i2_d i1_e i2_e vqty_1 vqty_2 vqty_3 vqty_4 vqty_5 i3_a i4_a i3_b i4_b i3_c i4_c i3_d i4_d i3_e i4_e iqty_1 iqty_2 iqty_3 iqty_4 ///
	iqty_5 i5_a i6_a i5_b i6_b i5_c i6_c i5_d i6_d i5_e i6_e fqty_1 fqty_2 fqty_3 fqty_4 fqty_5 i7 i8_a i8_a_0_other i8_b i8_b_0_other i8_c i8_c_0_other i8_d i8_d_0_other i8_e ///
	i8_e_0_other i9_a i9_b i9_c i9_d i9_e i10 v1 v1a v1b v2 v2_rec v2_0_other v3 n1a n1b n2 n2_rec n2_0_other n3_a n3_b n3_c1 n3_c2 n3_d n3_e n3_f n3_g n3_h n3_i n3_j n3_k n3_o ///
	n3oo n3dx n3ix n3ix_rec n3ix_0_other n3jx n3jx_rec n3jx_0_other n4 n5a n5b n_install n5c n6 n6a n6b cc1a cc1b n3b_new n4_new sat1_a sat1_b sat1_c sat1_d sat2a sat2b sat2c ///
	sat2d sat3 f1 f1_rec f1_0_other f2 f3a f3b f3a_new_fl f4 f4_rec f4_0_other
export excel using "$clean\DEC-DEP Main Channel Survey_SO Export.xlsx", replace firstrow(variable)

//Export for FR analysis
use "$work/DEC-DEP Main Channel Survey_Final.dta", clear
keep odcid stratum name email sample_pii_tech sample_pii_meas_a sample_pii_meas_b sample_pii_meas_c sample_pii_meas_d date v1 v1a v1b v2_rec v2_0_other v3 n1a n1b n2_rec n2_0_other n3_a ///
	 n3_b n3_c1 n3_c2 n3_d n3_e n3_f n3_g n3_h n3_i n3_j n3_k n3_o n3oo n3dx n3ix_rec n3ix_0_other n3jx_rec n3jx_0_other n4 n5a n5b n_install n5c n6 n6a n6b cc1a cc1b n3b_new n4_new
rename (sample_pii_tech sample_pii_meas_a sample_pii_meas_b sample_pii_meas_c sample_pii_meas_d) (tech meas1 meas2 meas3 meas4)
export excel using "$clean\DEC-DEP Main Channel Survey_FR Export.xlsx", replace firstrow(variable)






