/***************************************
Created By: Kai Zhou
Creation Date: 2018-04-11

Last Modified By: Kai Zhou
Modified Date: 2018-04-11

This files imports and cleans the DEC DEP NR-Prescriptive Data
For Desk Reviews and Impacts
***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths
global main "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/5 - DEC-DEP 2019/Data Cleaning"
global raw "Q:/7880-Duke Energy Portfolio Evaluation/_Secure Data/7-Non-Residential Prescriptive/DEC-DEP 2019"
global work "$raw/Working Files"
global map "$raw/Mapping Files"
global qc "$main/QC_Outputs"
global fin "$main/Final_Outputs"
global af "$main/Analysis Files"
global syntax "C:/Users/kzhou/Documents/OpDyn Projects/7880_2019_DEC_DEP_NonRes_Prescriptive"
global lut "D:/Report Resources/Lookup Tables"

// Setting Up Documentation
*DOC "Documentation for"
// Setting Up Excel File
global xso "DEC DEP NR-Pres Program Data Summary Output.xlsx"
do "$syntax/Data Cleaning Modules.do"
exit

/*_________              .__                         ___________    .__       .___            
\_   _____/__  _________ |  |   ___________   ____   \_   _____/___ |  |    __| _/___________ 
 |    __)_\  \/  /\____ \|  |  /  _ \_  __ \_/ __ \   |    __)/  _ \|  |   / __ |/ __ \_  __ \
 |        \>    < |  |_> >  |_(  <_> )  | \/\  ___/   |     \(  <_> )  |__/ /_/ \  ___/|  | \/
/_______  /__/\_ \|   __/|____/\____/|__|    \___  >  \___  / \____/|____/\____ |\___  >__|   
        \/      \/|__|                           \/       \/                   \/    */       
// Create Folders
cd "$main"
*mkfolders
ls

   /*___                .__                         __________                  __    __________                                            
  /  _  \   ____ _____  |  | ___.__.________ ____   \______   \_____    _______/  |_  \______   \ ____   ____ _____    _____   ____   ______
 /  /_\  \ /    \\__  \ |  |<   |  |\___   // __ \   |     ___/\__  \  /  ___/\   __\  |       _// __ \ /    \\__  \  /     \_/ __ \ /  ___/
/    |    \   |  \/ __ \|  |_\___  | /    /\  ___/   |    |     / __ \_\___ \  |  |    |    |   \  ___/|   |  \/ __ \|  Y Y  \  ___/ \___ \ 
\____|__  /___|  (____  /____/ ____|/_____ \\___  >  |____|    (____  /____  > |__|    |____|_  /\___  >___|  (____  /__|_|  /\___  >____  >
        \/     \/     \/     \/           \/    \/                  \/     \/                 \/     \/     \/     \/      \/     \/     */ 
// Rename Analysis Part 1
cd "C:\Users\kzhou\Documents\OpDyn Projects\9999_Archived\7880_2016_Duke_NonRes_Prescriptive"
renanalyze using "DEC DEP Tracking Data Original\DEI SmartSaver Tracking Data Cleaning Part 2.do"
// Save
save "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 1.dta", replace
// Rename Analysis Part 2
renanalyze using "DEC DEP Tracking Data Original\DEC DEP EEB Tracking Data Cleaning.do"
// Save
save "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 2.dta", replace
// Rename Analysis Part 3
renanalyze using "DEC DEP Tracking Data Original\DEI SmartSaver Tracking Data Cleaning Part 1.do"
// Save
save "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 3.dta", replace
// Rename Analysis Part 4
renanalyze using "DEC DEP Tracking Data Wave 2\DEC DEP NR Pres Master Data Wave 2.do"
// Save
save "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 4.dta", replace
// Rename Analysis Part 5
renanalyze using "DEC DEP Tracking Data Wave 2\DEC DEP NR Pres Master Data Wave 2 Revisions.do"
// Save
save "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 5.dta", replace

// Append All Renames
clear
append using "$work/DEC DEP Non-Res Pres Tracking Data Renames Part 1.dta" ///
	"$work/DEC DEP Non-Res Pres Tracking Data Renames Part 2.dta" ///
	"$work/DEC DEP Non-Res Pres Tracking Data Renames Part 3.dta" ///
	"$work/DEC DEP Non-Res Pres Tracking Data Renames Part 4.dta" ///
	"$work/DEC DEP Non-Res Pres Tracking Data Renames Part 5.dta", gen(data)
spreshape data, i(var_orig var_new) dropmissing
shrink data?, gen(data)
duplicates drop
sort var_orig var_new ren_group resh_ord
by var_orig var_new: gen rank = _n
keep if rank == 1
drop if var_orig == ""
dupit var_orig, keep
foreach var in date_recv name2 qty meas_unit ds_name ta_email enr_no meas_cat part_id ///
	ta_phone contr_type prog_name addr city state zip state_code state ///
	meas_cat match merge1 contact technology m_id2 name_fin name_orig ///
	contact acct_full contact_alt pgm_meas_id phone_orig phone_s qty1 qty2 ///
	source_det state2 spm_id ta_orig {
	drop if var_new == "`var'" & dup != 0
}
rename var_orig variable
// Save
save "$work/DEC-DEP NR-Pres Rename Table.dta", replace


// DEI Data Cleaning
cd "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/3 - DEI 2018/Data Cleaning/Working Files"
ls *Rename*
use "DEI NR-Pres Manual Renames.dta", clear
save "$work/DEI NR-Pres Manual Renames.dta", replace
use "DEO NR-Pres Rename Table.dta", clear
save "$work/DEO NR-Pres Rename Table.dta", replace

// DEO Data Cleaning
cd "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/4 - DEO 2018/Data Cleaning/Working Files"
ls *Rename*
use "DEC-DEP NR-Pres Manual Renames.dta", clear
save "$work/DEC-DEP NR-Pres Manual Renames.dta", replace
ls *Technology*
use "DEO NR-Pres Technology Map.dta", clear
// Save
save "$work/DEC DEP NR-Pres Technology Map.dta", replace
ls *Vendor*

use "DEO NR-Pres Vendor ID Map.dta", clear
// Save
save "$work/DEC DEP NR-Pres Vendor ID Map.dta", replace

/*__   ____            .__      ___.   .__           .____    .__          __   
\   \ /   /____ _______|__|____ \_ |__ |  |   ____   |    |   |__| _______/  |_ 
 \   Y   /\__  \\_  __ \  \__  \ | __ \|  | _/ __ \  |    |   |  |/  ___/\   __\
  \     /  / __ \|  | \/  |/ __ \| \_\ \  |_\  ___/  |    |___|  |\___ \  |  |  
   \___/  (____  /__|  |__(____  /___  /____/\___  > |_______ \__/____  > |__|  
               \/              \/    \/          \/          \/       */      
// Import Files
cd "$raw/_Original Data"
ls
import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx", desc
import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx", ///
	sheet("Main Channel") firstrow clear
sumstat
// Save
save "$work/DEC DEP NonRes Data Main Variable List 20190313.dta", replace

// Midstream and Other Channels
import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx", ///
	sheet("Midstream and other channels") firstrow clear
sumstat
// Save
save "$work/DEC DEP NonRes Data MidSt Variable List 20190313.dta", replace

// Import Files
cd "$raw/_Original Data"
ls
import excel using "DEC DEP NonRes Prescriptive data request 5.13.19.xlsx", desc
import excel using "DEC DEP NonRes Prescriptive data request 5.13.19.xlsx", ///
	sheet("Main Channel") firstrow clear
sumstat
// Save
save "$work/DEC DEP NonRes Data Main Variable List 20190513.dta", replace

// Midstream and Other Channels
import excel using "DEC DEP NonRes Prescriptive data request 5.13.19.xlsx", ///
	sheet("Midstream and other channels") firstrow clear
sumstat
// Save
save "$work/DEC DEP NonRes Data MidSt Variable List 20190513.dta", replace

// ******************************************************************** //
// Compare Datasets Variable List
// ******************************************************************** //
// Varlist and Obervation Compare
use "$work/DEC DEP NonRes Data Main Variable List 20190513.dta", clear
gen data = 513
append using "$work/DEC DEP NonRes Data Main Variable List 20190313.dta"
replace data = 313 if data == .
drop valuelabel type populated
reshape wide order observations, i(variable varlabel) j(data)
sort order313
list if observations313 != observations513

// Varlist and Obervation Compare
use "$work/DEC DEP NonRes Data MidSt Variable List 20190513.dta", clear
gen data = 513
append using "$work/DEC DEP NonRes Data MidSt Variable List 20190313.dta"
replace data = 313 if data == .
drop valuelabel type populated
reshape wide order observations, i(variable varlabel) j(data)
sort order313 order513
list if observations313 != observations513

use "$work/DEC DEP NonRes Data Main Variable List 20190204.dta", clear
merge 1:1 variable using "$work/DEC DEP NonRes Data MidSt Variable List 20190204.dta"
sort order _merge
order _merge
drop _merge
// Save
save "$work/Varlist 20190204.dta", replace

use "$work/DEC DEP NonRes Data Main Variable List 20190313.dta", clear
merge 1:1 variable using "$work/DEC DEP NonRes Data MidSt Variable List 20190313.dta"
sort order _merge
order _merge
drop _merge
// Save
save "$work/Varlist 20190313.dta", replace

use "$work/DEC DEP NonRes Data Main Variable List 20190513.dta", clear
merge 1:1 variable using "$work/DEC DEP NonRes Data MidSt Variable List 20190513.dta"
sort order _merge
order _merge
drop _merge
// Save
save "$work/Varlist 20190513.dta", replace

// Compare
use "$work/Varlist 20190204.dta", clear
rename (populated observations)(pop0204 obs0204)
merge 1:1 variable using "$work/Varlist 20190313.dta"
rename (populated observations)(pop0313 obs0313)
list variable varlabel _merge if _merge != 3
// Version 0313 Has 1 Extra Variable (Source Participation ID)
// 13 Fewer Observations, 9 Fewer Observations On The Contact Side

  /*_______                                  .__                   _____  .__  .__      __  .__             ________          __          
 /   _____/__ __  _____   _____ _____ _______|__|_______ ____     /  _  \ |  | |  |   _/  |_|  |__   ____   \______ \ _____ _/  |______   
 \_____  \|  |  \/     \ /     \\__  \\_  __ \  \___   // __ \   /  /_\  \|  | |  |   \   __\  |  \_/ __ \   |    |  \\__  \\   __\__  \  
 /        \  |  /  Y Y  \  Y Y  \/ __ \|  | \/  |/    /\  ___/  /    |    \  |_|  |__  |  | |   Y  \  ___/   |    `   \/ __ \|  |  / __ \_
/_______  /____/|__|_|  /__|_|  (____  /__|  |__/_____ \\___  > \____|__  /____/____/  |__| |___|  /\___  > /_______  (____  /__| (____  /
        \/            \/      \/     \/               \/    \/          \/                       \/     \/          \/     \/          */
// Import Files
cd "$raw"
import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx", ///
	sheet("Main Channel") firstrow clear
scalar ct = 1
foreach var of varlist _all {
	rename `var' var`=ct'
	scalar ct = ct + 1
}
MassSum
rename variable order
destring order, replace ignore("var")
merge m:1 order using "$work/DEC DEP NonRes Data Main Variable List 20190313.dta", keepusing(variable)
order variable, after(order)
sum length(variable)
drop _merge
// Save
save "$work/DEC DEP NonRes Main Data MassSum 20190313.dta", replace

// Import Files
cd "$raw"
import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx", ///
	sheet("Midstream and other channels") firstrow clear
scalar ct = 1
foreach var of varlist _all {
	rename `var' var`=ct'
	scalar ct = ct + 1
}
MassSum
rename variable order
destring order, replace ignore("var")
merge m:1 order using "$work/DEC DEP NonRes Data MidSt Variable List 20190313.dta", keepusing(variable)
order variable, after(order)
sum length(variable)
drop _merge
// Save
save "$work/DEC DEP NonRes MidSt Data MassSum 20190313.dta", replace

// MassSum Summary
use "$work/DEC DEP NonRes Main Data MassSum 20190313.dta", clear
append using "$work/DEC DEP NonRes MidSt Data MassSum 20190313.dta"
*bro if variable == "AU"

// Try AutoRename
use "$work/DEC DEP NonRes Data Main Variable List 20190313.dta", clear
gen data = "Main"
append using "$work/DEC DEP NonRes Data MidSt Variable List 20190313.dta"
replace data = "MidStream" if data == ""
merge m:1 variable using "$work/DEC-DEP NR-Pres Rename Table.dta"
rename (variable var_new)(var_orig var_new)
order var_new, after(var_orig)
drop if _merge == 2
drop valuelabel type populated observations
order data
drop _merge
// Export
*export excel using "$map/DEC DEP NR-Pres Variable Dictionary.xlsx", ///
*	sheet("Variable Dictionary") firstrow(variables) sheetreplace

  /*______                                   __           __________                                      _________                                           .___      
 /  _____/  ____   ____   ________________ _/  |_  ____   \______   \ ____   ____ _____    _____   ____   \_   ___ \  ____   _____   _____ _____    ____    __| _/______
/   \  ____/ __ \ /    \_/ __ \_  __ \__  \\   __\/ __ \   |       _// __ \ /    \\__  \  /     \_/ __ \  /    \  \/ /  _ \ /     \ /     \\__  \  /    \  / __ |/  ___/
\    \_\  \  ___/|   |  \  ___/|  | \// __ \|  | \  ___/   |    |   \  ___/|   |  \/ __ \|  Y Y  \  ___/  \     \___(  <_> )  Y Y  \  Y Y  \/ __ \|   |  \/ /_/ |\___ \ 
 \______  /\___  >___|  /\___  >__|  (____  /__|  \___  >  |____|_  /\___  >___|  (____  /__|_|  /\___  >  \______  /\____/|__|_|  /__|_|  (____  /___|  /\____ /____  >
        \/     \/     \/     \/           \/          \/          \/     \/     \/     \/      \/     \/          \/             \/      \/     \/     \/      \/    */ 
// Renames
import excel using "$map/DEC DEP NR-Pres Variable Dictionary.xlsx", ///
	sheet("Variable Dictionary") firstrow clear
// Save
save "$work/DEC DEP NR-Pres Manual Renames.dta", replace
drop if length(var_orig) < 3
gen cmd = "capture rename " + var_orig + " " + var_new
keep cmd
// Export
export delimited using "$work/NR-Pres Variable Renames 1.do", replace novarnames

// Generate Lists to Have
import excel using "$map/DEC DEP NR-Pres Variable Dictionary.xlsx", ///
	sheet("Variable Dictionary") cellrange(A1) firstrow clear
rename _all, lower
keep var_new level group
spreshape var_new, i(level group)
shrink var_new*, gen(list) sep(" ")
duplicates drop
dfencode level ("Drop" "Measure" "Project" "Record" "Site" "Vendor") = (9 4 3 1 2 5)
order level group
sort level group
autoformat
// Save
save "$work/DEC DEP NR-Pres Variable List Grouped.dta", replace
toexcel "var_group"
use "$work/DEC DEP NR-Pres Variable List Grouped.dta", clear

   /*___          __               .__    .___                              __   
  /  _  \   _____/  |_ __ _______  |  |   |   | _____ ______   ____________/  |_ 
 /  /_\  \_/ ___\   __\  |  \__  \ |  |   |   |/     \\____ \ /  _ \_  __ \   __\
/    |    \  \___|  | |  |  // __ \|  |__ |   |  Y Y  \  |_> >  <_> )  | \/|  |  
\____|__  /\___  >__| |____/(____  /____/ |___|__|_|  /   __/ \____/|__|   |__|  
        \/     \/                \/                 \/|_*/                       
// Import Files
cd "$raw/_Original Data"
*import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx"
import excel using "DEC DEP NonRes Prescriptive data request 5.13.19.xlsx", ///
	sheet("Main Channel") firstrow clear
do "$work/NR-Pres Variable Renames 1.do"
rename Soure_Partiipation_Id sp_id2
rename AU meas_desc3
rename (BN BO BP BR)(first_contact2 last_contact2 phone_contact email_contact)
rename (ServiceCity ServiceState ServiceZipCode)(scity2 sstate2 szip2)
labelfix
strtrim
compress
autoformat
gen index = _n
order index
tab tech
gen channel = "Main"
// Save
save "$work/DEC DEP NonRes Prescriptive Import Main Renames 20190513.dta", replace

cd "$raw/_Original Data"
*import excel using "DEC DEP NonRes Prescriptive data request 2.28.19.xlsx",
import excel using "DEC DEP NonRes Prescriptive data request 5.13.19.xlsx", ///
	sheet("Midstream and other channels") firstrow clear
do "$work/NR-Pres Variable Renames 1.do"
rename SourceSystemCode sys_code2
rename _all, lower
*rename R sp_id4
labelfix
strtrim
compress
autoformat
gen index = _n
order index
tab tech
gen channel = "Midstream"
// Save
save "$work/DEC DEP NonRes Prescriptive Import MidSt Renames 20190513.dta", replace

use "$work/DEC DEP NonRes Prescriptive Import Main Renames 20190513.dta", clear
nfappend using "$work/DEC DEP NonRes Prescriptive Import MidSt Renames 20190513.dta"
replace index = _n
order index
tab channel
unique channel, by(acct_id)
*bro if channel_ct == 2
drop channel_ct
dupit partid, assert
// Save
save "$work/DEC DEP NonRes Prescriptive Import Renames 20190513.dta", replace

// Variables to Extract and Bandaid
use "$work/DEC DEP NonRes Prescriptive Import MidSt Renames 20190513.dta", clear
dupit partid
keep partid manuf model date_sales dist_notes payto pname paddr vendorid pcity pstate pzip ocname ocphone ocemail
count if pname != payto
drop pname
format date_sales %td
replace date_sales = date_sales + td(01jan1900)
strremove "0"
autoformat
rename vendorid vend_id
order vend_id payto, after(dist_notes)
// Save
save "$work/DEC DEP MidSt Variables To Add.dta", replace

use "$work/DEC DEP MidSt Variables To Add.dta", clear
keep vend_id payto
duplicates drop
drop if vend_id == .
dist_clean
split payto, parse("~")
rename (payto1 payto2)(vend_name vend_loc)
drop payto
merge 1:1 vend_id using "$work/DEC DEP NR-Pres Vendor ID Map.dta"
replace vend_name = "Leidos – Sam's Club" if vend_name == "Leidos – Sam’s Club"
replace channel = "Midstream" if channel == ""
drop _merge
// Save
save "$work/DEC DEP NR-Pres Vendor ID Map Rev.dta", replace

// Records
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190513.dta", clear
sum meas_qty
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
sum meas_qty

// Differences in Records
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190513.dta", clear
merge 1:1 partid using "$work/DEC DEP NonRes Prescriptive Import Renames 20190204.dta"
keep if _merge != 3
order _merge
sort acct_id
tab util _merge
// Looks Like They Collapsed And Updated Around 200 Records, 192 Accounts IDs
codebook acct_id, c

  /*_______      .__                  _________.__  __           .____                      .__   
 /   _____/ ____ |  |___  __ ____    /   _____/|__|/  |_  ____   |    |    _______  __ ____ |  |  
 \_____  \ /  _ \|  |\  \/ // __ \   \_____  \ |  \   __\/ __ \  |    |  _/ __ \  \/ // __ \|  |  
 /        (  <_> )  |_\   /\  ___/   /        \|  ||  | \  ___/  |    |__\  ___/\   /\  ___/|  |__
/_______  /\____/|____/\_/  \___  > /_______  /|__||__|  \___  > |_______ \___  >\_/  \___  >____/
        \/                      \/          \/               \/          \/   \/          */      
// Account Data
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index acct* tenid
combine acct acct5
tab tenid
drop tenid
replace acct_mngr = "" if acct_mngr == "0"
order acct_id acct
unique acct, by(acct_id)
spreshape acct, i(acct_id) dropmissing
spreshape acct_mngr, i(acct_id) dropmissing
shrink acct_mngr1 acct_mngr2, gen(acct_mngr)
replace acct_ct = 1 if acct2 == .
tab acct2
drop index
label variable acct1 "Account Number 1"
label variable acct2 "Account Number 2"
label variable acct_mngr "Assigned Account Manager"
displaylabel
label variable acct_ct "Unique Count of Accounts by Account ID"
duplicates drop
dupit acct_id
compress
autoformat
// Save
save "$work/DEC DEP NRP - Part 1 - Account List.dta", replace

// Names and Address
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep acct_id *name* *active* *cust* *addr* *city* *state* *unit* *zip*
drop ta_name meas_name meas_unit2 qty_unit
rename cust2 cust
order acct_id
rename saddr saddrw
rename (saddr1 saddr2)(saddrx saddry)
duplicates drop
magicdedup _all, uid(acct_id)
drop over10 deduped
sort acct_id
compress
// Save
save "$work/DEC DEP NRP Name and Address Dededuped.dta", replace

use "$work/DEC DEP NRP Name and Address Dededuped.dta", clear
gen acct_full = regexs(1) if regexm(cust, "([0-9]+)$")
order acct_full, after(acct_id)
replace cust = regexr(cust, "- " + acct_full, "")
strtrim cust, proper
compress
autoformat
*bro *addr*
replace saddrw = "" if saddrx == saddrw
drop saddrw saddrw_alt
rename saddrx saddr
rename saddry unit
replace unit = "" if unit == "NULL"
unitparse saddr
combine unit saddr_unit
drop saddr_old
tab sstate prem_state, m
replace sstate = prem_state if sstate == ""
tab state_code sstate, m
drop state_code state prem_state
replace prem_city = "" if scity == prem_city
replace scity = prem_city if scity == ""
drop prem_city
replace szip_str = substr(szip_str, 1, 5)
destring szip_str, replace
replace szip = szip_str if szip == .
replace szip = prem_zip if szip == .
drop prem_zip szip_str
strtrim name, proper
replace cust = "" if cust == name
rename cust cust_alt
rename name cust
order acct_id acct_full active cust cust_alt saddr unit scity sstate szip
compress
autoformat
destring acct_full, replace
replacecmd active
drop active
merge 1:1 acct_id using "$work/DEC DEP NRP - Part 1 - Account List.dta"
order acct? acct_ct acct_mngr, after(acct_full)
assert _merge == 3
drop _merge
rename unit sunit
strtrim
compress
autoformat
drop acct_ct acct_mngr
replace acct_full = . if acct_full == acct1
dropempty acct_full
strtrim scity, proper
*label variable acct_full "Full Account Number"
label variable cust "Customer Name"
label variable cust_alt "Alternate Customer Name"
label variable saddr "Premise Address"
label variable sunit "Premise Unit"
label variable scity "Premise City"
label variable sstate "Premise State"
label variable szip "Premise Zip"
// Save
save "$work/DEC DEP NRP - Part 2 - Full Name Address.dta", replace

// Geocodes
use "$work/DEC DEP NRP - Part 2 - Full Name Address.dta", clear
keep acct_id saddr scity sstate szip
*export delimited using "$work/DEC DEP Addresses For Geocoding.csv", replace
*export delimited using "D:\Temporary\Geocodes\DEC DEP Addresses For Geocoding.csv", replace

// Quick Site Level
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
spreshape channel, i(acct_id)
shrink channel?, gen(channel)
gen records = 1
collapse (count) records, by(channel acct_id)
collapse (count) acct_id (sum) records, by(channel)
// Export
toexcel "acct_chan"

// Quick Site Level
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep acct_id date* util
replace date_upd = dofm(mofd(date_upd))
contract acct_id date_upd util
collapse (count) acct_id, by(date_upd util)
reshape wide acct_id, i(date_upd) j(util) string
rename (acct_idNC01 acct_idNC02 acct_idSC01 acct_idSC02) ///
	(acct_id_dec_nc acct_id_dep_nc acct_id_dec_sc acct_id_dep_sc)
// Export
toexcel "acct_my"

use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep spm* pm* m_* *kw*
collapse (sum) *kw*, by(m_id pm_id spm_id3 m_id3)
dupit m_id
dupit pm_id
dupit spm_id3

use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep tech channel *kw*
collapse (sum) exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc, by(tech channel)
sort channel tech
// Export
toexcel "tech_sav"

// Import
import delimited using "D:\Temporary\Geocodes\GR_DEC DEP Addresses For Geocoding.csv", clear
geofill
keep if geo_qual == "None"
keep acct_id address
rename *_ *
*export excel using "$map/Geocode LUTs.xlsx", sheet("LUT") firstrow(variables) sheetreplace
openit "$map/Geocode LUTs.xlsx"

// LUTs
import excel using "$map/Geocode LUTs.xlsx", sheet("LUT") firstrow clear
split Link, parse("/")
split Link7, parse("," "@")
rename (Link72 Link73)(latitude longitude)
keep acct_id latitude longitude
destring latitude longitude, replace
// Save
save "$work/Long Lat LUTs 1.dta", replace

import delimited using "D:\Temporary\Geocodes\GR_DEC DEP Addresses For Geocoding.csv", clear
geofill
rename *_ *
merge 1:1 acct_id using "$work/Long Lat LUTs 1.dta", update
replace geo_qual = "Google" if _merge >= 4
tab geo_qual
keep acct_id block longitude latitude geo_qual
replace block = floor(block / 10000)
rename block geoid
autoformat
labelfix
// Save
save "$work/DEC DEP NRP - Part 3 - Geocoded Addresses.dta", replace

// Geo Map Summary
use "$work/DEC DEP NRP - Part 3 - Geocoded Addresses.dta", clear
replace longitude = round(longitude, .05)
replace latitude = round(latitude, .05)
merge 1:1 acct using "$work/DEC DEP NRP - Part 2 - Full Name Address.dta", keepusing(sstate)
drop if _merge == 1
assert _merge == 3
drop _merge
collapse (count) acct, by(longitude latitude sstate)
gsort longitude latitude -acct_id sstate
replace sstate = sstate[_n-1] if longitude == longitude[_n-1] & latitude == latitude[_n-1]
collapse (sum) acct, by(longitude latitude sstate)
toexcel "rel_map"


// Contact Information
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index acct_id *cont* *email* *phone*
// Drop Trade Ally
order first_contact2 last_contact2 phone_contact ta_phone_mob email_contact, last
strtrim contact_prim, proper
gen contact = first_contact + " " + last_contact
gen ta_contact = first_contact2 + " " + last_contact2
drop first_contact last_contact first_contact2 last_contact2
order index acct_id contact
strtrim contact ta_contact, proper
phoneclean *phone*
emailclean *email*
replace email2 = "glen.weckbacher@cms.k12" if email2 == "glen.weckbacher@cms.k12"
replace email = "" if email == "null"
drop *_orig *_ext *_valid
autoformat
rename phone2 phone_gen
rename contact_prim contact_type
rename email2 email_gen
rename (email phone)(email_biz phone_biz)
rename phone_contact ta_phone
rename email_contact ta_email
order ta_phone ta_phone_mob ta_email, last
spreshape index, i(acct_id contact phone_gen phone_mob email_gen contact_type email_biz phone_biz phone_prem)
shrink index*, gen(indexlist)
duplicates drop
dupit acct_id, keep
strtrim contact, proper
drop dup
labelfix
autoformat
// Save
save "$work/Contact Information Cleaned.dta", replace

// Main Phones People Should Be Using
use "$work/Contact Information Cleaned.dta", clear
keep indexlist phone_gen phone_mob phone_biz phone_prem
duplicates drop
reshape long phone, i(indexlist) j(phone_type) string
drop if phone == .
keep phone
duplicates drop
// Save
save "$work/Primary Phones List.dta", replace

// Unique Contact Per
use "$work/Contact Information Cleaned.dta", clear
drop ta_*
gen index = _n
shapelong email_gen email_biz, gen(email)
shapelong phone_gen phone_mob phone_biz phone_prem, gen(phone)
order indexlist acct_id contact contact_type email email_ord phone phone_ord
gen group_id = _n
order group_id
forvalues i = 1/10 {
	foreach var of varlist acct_id contact phone email {
		sort `var' group_id
		if regexm("`:type `var''", "str") {
			by `var': replace group_id = group_id[1] if `var' != ""
		}
		else {
			by `var': replace group_id = group_id[1] if `var' != .
		}
	}
}
dupit group_id, keep // bro
sort group_id contact
// Save
*save "$work/Unique Contact Group ID Assigned.dta", replace

// Phone Types
use "$work/Unique Contact Group ID Assigned.dta", clear
rename phone_ord ph_type
rename email_ord em_type
spreshape ph_type, i(phone)
shrink ph_type?, gen(ph_type)
keep phone ph_type
duplicates drop
replacecmd ph_type
replace ph_type = "General" if ph_type == "1"
replace ph_type = "Mobile" if ph_type == "1/2"
replace ph_type = "Customer" if ph_type == "1/2/3"
replace ph_type = "Customer" if ph_type == "1/2/3/4"
replace ph_type = "Mobile" if ph_type == "1/2/4"
replace ph_type = "Customer" if ph_type == "1/3"
replace ph_type = "Customer" if ph_type == "1/3/4"
replace ph_type = "Premise" if ph_type == "1/4"
replace ph_type = "Mobile" if ph_type == "2"
replace ph_type = "Customer" if ph_type == "2/3"
replace ph_type = "Customer" if ph_type == "2/3/4"
replace ph_type = "Mobile" if ph_type == "2/4"
replace ph_type = "Customer" if ph_type == "3"
replace ph_type = "Customer" if ph_type == "3/4"
replace ph_type = "Premise" if ph_type == "4"
dfencode ph_type, prep
dfencode ph_type ("General" "Mobile" "Premise" "Customer") = (4 3 2 1)
order phone
tab ph_type
// Save
save "$work/Phone Types.dta", replace

// Copy and Paste
use "$work/Unique Contact Group ID Assigned.dta", clear
drop contact_type email_ord phone_ord index dup
unique email, by(group_id)
replace email = "" if email_ct > 10
drop email_ct
spreshape email, i(group_id) dropmissing
shrink email*, gen(email_list)
unique phone, by(group_id)
replace phone = . if phone_ct > 10
drop phone_ct
spreshape phone, i(group_id) dropmissing
shrink phone*, gen(phone_list)
keep group_id contact phone_list email_list
duplicates drop
compress
autoformat
sort group_id contact
gen contact_new = contact
order contact_new, after(contact)
replace contact_new = ""
// Export
*export excel using "$map/DEC DEP Contact Name Cleaning.xlsx", sheet("Contact Map") firstrow(variables) sheetreplace

import excel using "$map/DEC DEP Contact Name Cleaning.xlsx", sheet("Contact Map") firstrow clear
keep if contact != contact_new & contact_new != "" & contact != ""
keep contact contact_new
// Save
save "$work/Contact Name Cleaning.dta", replace

use "$work/Unique Contact Group ID Assigned.dta", clear
merge m:1 contact using  "$work/Contact Name Cleaning.dta"
replace contact = contact_new if contact_new != ""
flagbiz contact
replace flag_biz = 0 if contact == "James Crossingham"
replace flag_biz = 0 if contact == "Jim Street"
replace flag_biz = 0 if contact == "Paul Streeter"
replace flag_biz = 0 if contact == "Street Jones"
replace flag_biz = 0 if contact == "Walter Health"
replace contact = "Victor Campos" if contact == "Victor M Campos C Ampos Properties"
replace flag_biz = 0 if contact == "Victor Campos"
replace contact = "" if flag_biz == 1
replace contact = regexr(contact, "\*", "")
strtrim contact
// Save
save "$work/DEC DEP Round 1 Cleaned Contacts.dta", replace

// Fill Contacts by Phone Group
use "$work/DEC DEP Round 1 Cleaned Contacts.dta", clear
drop if contact == ""
contract phone group contact
dupit group_id phone, keep
keep if dup == 0
drop _freq dup
// Save
save "$work/Contact Match 1 - Group Phone.dta", replace

// Fill Contacts by Email Group
use "$work/DEC DEP Round 1 Cleaned Contacts.dta", clear
drop if contact == ""
contract email group contact
dupit group_id email, keep
keep if dup == 0
drop _freq dup
// Save
save "$work/Contact Match 2 - Group Email.dta", replace

// Fill Contacts by Phone
use "$work/DEC DEP Round 1 Cleaned Contacts.dta", clear
drop if contact == ""
contract phone contact
dupit phone, keep
keep if dup == 0
drop _freq dup
// Save
save "$work/Contact Match 3 - Phone.dta", replace

// Fill Contacts by Email
use "$work/DEC DEP Round 1 Cleaned Contacts.dta", clear
drop if contact == ""
contract email contact
dupit email, keep
keep if dup == 0
drop _freq dup
// Save
save "$work/Contact Match 4 - Email.dta", replace

// Fill Contacts by Email
use "$work/DEC DEP Round 1 Cleaned Contacts.dta", clear
gen has_contact = contact != ""
tab _merge
drop _merge
merge m:1 group phone using "$work/Contact Match 1 - Group Phone.dta", update
drop _merge
merge m:1 group email using "$work/Contact Match 2 - Group Email.dta", update
drop _merge
merge m:1 phone using "$work/Contact Match 3 - Phone.dta", update
drop _merge
merge m:1 email using "$work/Contact Match 4 - Email.dta", update
drop _merge
preserve
	drop if contact == ""
	contract acct_id contact
	dupit acct_id, keep
	keep if dup == 0
	drop _freq dup
	// Save
	save "$work/Contact Match 5 - Account.dta", replace
restore
merge m:1 acct_id using "$work/Contact Match 5 - Account.dta", update
drop _merge
preserve
	drop if contact == ""
	contract indexlist contact
	dupit indexlist, keep
	keep if dup == 0
	drop _freq dup
	// Save
	save "$work/Contact Match 6 - Indexlist.dta", replace
restore
merge m:1 indexlist using "$work/Contact Match 6 - Indexlist.dta", update
drop _merge
preserve
	keep group_id acct_id contact
	duplicates drop
	sort group_id acct_id contact
	dupit contact, keep
	gen modlen = mod(length(contact), 3)
	tab modlen
	recode modlen (2 = 1)
	sort group_id acct_id dup modlen
	by group_id: gen rank = _n
	bysort contact: egen min_rank = min(rank)
	drop min_rank
	gen double contact_id = group_id * 1000 + rank
	// New Group ID
	save "$work/Revised Contact ID.dta", replace
restore
merge m:1 group_id acct_id contact using "$work/Revised Contact ID.dta"
assert _merge == 3
drop _merge
order contact_id
keep contact_id indexlist group_id acct_id contact has_contact contact_type
duplicates drop
dupit indexlist, keep
replace contact = "" if has_contact == 0 & dup != 0
sort indexlist contact_id
by indexlist: replace contact_id = contact_id[_n-1] if _n != 1
duplicates drop
sort indexlist
drop dup
// Save
save "$work/DEC DEP NR-Pres Contact ID Assigned.dta", replace

// Optimize Phones
use "$work/Unique Contact Group ID Assigned.dta", clear
merge m:1 indexlist using "$work/DEC DEP NR-Pres Contact ID Assigned.dta"
keep contact_id phone
drop if phone == .
duplicates drop
merge m:1 phone using "$work/Phone Types.dta"
drop if _merge == 2
tab ph_type
sort contact_id ph_type
by contact_id: gen rank = _n
drop if rank > 2
drop _merge
reshape wide ph_type phone, i(contact_id) j(rank)
rename (*1 *2)(* *_alt)
tab ph_type
// Save
save "$work/DEC DEP Phones by Contact ID.dta", replace

// Optimize Emails
use "$work/Unique Contact Group ID Assigned.dta", clear
merge m:1 indexlist using "$work/DEC DEP NR-Pres Contact ID Assigned.dta"
keep contact_id email
drop if email == ""
duplicates drop
sort contact_id email
by contact_id: gen rank = _n
tab rank
drop if rank > 2
reshape wide email, i(contact_id) j(rank)
rename (*1 *2)(* *_alt)
// Save
save "$work/DEC DEP Emails by Contact ID.dta", replace

// Index to Contact ID
use "$work/DEC DEP NR-Pres Contact ID Assigned.dta", clear
split indexlist, parse("/")
gen order = _n
drop indexlist
reshape long indexlist, i(order) j(lala)
drop if indexlist == ""
rename indexlist index
destring index, replace
order index contact_id
sort index
dupit index
keep index contact_id contact
// Save
save "$work/Index to Contact IDs.dta", replace

// Accounts
use "$work/DEC DEP NR-Pres Contact ID Assigned.dta", clear
drop if contact == ""
unique acct_id, by(contact)
drop if contact_type == ""
collapse (count) obs = acct_id, by(contact contact_type acct_id_ct)
gen type_combo = contact_type
spreshape type_combo, i(contact)
*bro if type_combo2 != ""
replace contact_type = "Customer" if type_combo2 != "" & acct_id_ct <= 3
tab acct_id_ct if type_combo2 != ""
replace contact_type = "Trade Ally" if type_combo2 != "" & acct_id_ct > 3
keep contact contact_type
duplicates drop
// Save
save "$work/Contact Type Rules.dta", replace

// Merge in Phones
use "$work/Index to Contact IDs.dta", clear
merge m:1 contact_id using "$work/DEC DEP Phones by Contact ID.dta"
assert _merge != 2
drop _merge
merge m:1 contact_id using "$work/DEC DEP Emails by Contact ID.dta"
assert _merge != 2
drop _merge
merge m:1 contact using "$work/Contact Type Rules.dta", update replace
assert _merge != 2
drop _merge
// Labels
label variable index "Labels"
label variable contact_id "Contact ID"
label variable contact "Contact"
label variable phone "Phone"
label variable ph_type "Phone Type"
label variable phone_alt "Phone Alternate"
tab ph_type_alt
drop ph_type_alt
label variable email "Email"
label variable email_alt "Alternate Email"
label variable contact_type "Primary Contact"
sort index
// Save
save "$work/DEC DEP NRP - Part 4 - Contact List With IDs.dta", replace

// Compare Cleaned and Raw
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index acct_id *cont* *email* *phone*
rename (email phone)(email_orig phone_orig)
drop first_contact2 last_contact2 phone_contact ta_phone_mob email_contact
merge 1:1 index using "$work/DEC DEP NRP - Part 4 - Contact List With IDs.dta"
// OK Checked

// Account Roll Ups
use "$work/DEC DEP NRP - Part 2 - Full Name Address.dta", clear
rename s* *
shapelong cust cust_alt, gen(cust)
shapelong acct1 acct2, gen(acct)
drop *_ord
dropemptyr addr unit city zip
compress
autoformat
labelfix
merge m:1 acct_id using "$work/DEC DEP NRP - Part 3 - Geocoded Addresses.dta"
drop if _merge == 2
assert _merge == 3
drop _merge
rename cust name
// Save
save "$work/Business Premise Roll Up Prep.dta", replace

// Account to ODCID Map
use "$work/Business Premise Roll Up Prep.dta", clear
gen index = _n
gen name_orig = name
autoformat
strtrim name, proper
compress
duplicates drop
wgtname name, uid(index)
drop name
rename name_wtd name
partname name
partaddr addr
// Unit Numbers
gen unit_n = regexs(1) if regexm(unit, "([0-9]+)")
replace unit_n = regexs(1) if regexm(unit, "^([A-Z]+) ")
replace unit_n = regexs(1) if regexm(unit, "^([A-Z]+)$")
replace unit_n = regexs(1) if regexm(unit, " ([A-Z]+)$")
replace unit_n = regexs(1) if regexm(unit, "^([A-Z][0-9]+) ")
replace unit_n = regexs(1) if regexm(unit, "([A-Z][0-9]+)$") 
replace unit_n = regexs(1) if regexm(unit, "([A-Z][0-9]+) ")
replace unit_n = regexs(1) if regexm(unit, "^([0-9]+[A-Z]) ")
replace unit_n = regexs(1) if regexm(unit, "([0-9]+[A-Z])$") 
replace unit_n = regexs(1) if regexm(unit, "([0-9]+[A-Z]) ")
replace unit_n = regexs(1) if regexm(unit, "[a-z] ([0-9]+)$")
compress
// City Part
strtrim city, proper
gen city_pt = city
replace city_pt = regexr(city_pt, "^North ", "")
replace city_pt = regexr(city_pt, "^South ", "")
replace city_pt = regexr(city_pt, "^East ", "")
replace city_pt = regexr(city_pt, "^West ", "")
replace city_pt = regexr(city_pt, "^N ", "")
replace city_pt = regexr(city_pt, "^S ", "")
replace city_pt = regexr(city_pt, "^E ", "")
replace city_pt = regexr(city_pt, "^W ", "")
replace city_pt = substr(city_pt, 1, 4)
// Geo Estimate
gen double long4 = round(longitude, .0001)
gen double lat4 = round(latitude, .0001)
gen odcid = index + 600000
order odcid
// Save
save "$work/DEC DEP NRP Names and Address Match Prep.dta", replace

capture program drop match_flags
program match_flags
	capture drop fs_*
	// Flags For Same Same But Different
	foreach var of varlist _all {
		if "`var'" != "odcid" & "`var'" != "geo_qual" & "`var'" != "index" {
			if regexm(`"`:type `var''"', "str") {
				gen fs_`var' = (`var' == `var'[_n-1] & `var' != "")
			}
			else if !regexm(`"`:type `var''"', "str") {
				gen fs_`var' = (`var' == `var'[_n-1] & `var' != .)
			}
		}
	}
	// Unit Blank is a Wild Card
	replace fs_unit_n = unit_n == unit_n[_n-1] | (unit_n == "" & unit_n[_n-1] != "") | (unit_n != "" & unit_n[_n-1] == "")
	// Name contonent
	gen cf_abs = fs_acct
	gen cf_name = fs_name
	gen cf_pname = ((fs_name_f3 | fs_name_w3) & (fs_name_l3 | fs_name_w3)) | ((fs_nsname_f3 | fs_nsname_w3) & (fs_nsname_l3 | fs_nsname_w3))
	gen cf_addr = fs_addr
	gen cf_paddr = (fs_addr_np2 & fs_addr_w & fs_long4 & fs_lat4) | (fs_addr_n & fs_addr_w3)
	gen cf_addr_sup = (fs_long4 & fs_lat4)
	gen cf_geo = (fs_longitude & fs_latitude) & geo_qual == "GeoLytics"
	gen cf_cityzip = (fs_city_pt | fs_zip)
	gen cf_unit = fs_unit_n
end

capture program drop match_tag
program match_tag
	sort `0'
	match_flags
	// Absolute
	replace odcid = odcid[_n-1] if cf_abs
	// Name, Partial Address
	replace odcid = odcid[_n-1] if cf_name & cf_paddr & cf_cityzip
	// Partial Name, Address
	replace odcid = odcid[_n-1] if cf_pname & cf_addr & cf_cityzip
	// Partial Name, and Geocode
	replace odcid = odcid[_n-1] if cf_pname & cf_geo & cf_cityzip
	// Partial Name, and Partial Address
	replace odcid = odcid[_n-1] if cf_pname & cf_paddr & cf_cityzip
	drop fs_* cf_*
end

// Create Flags to Generate Business ID
use "$work/DEC DEP NRP Names and Address Match Prep.dta",clear
*use "$work/CPUC OBF Small Sample.dta", clear
forvalues i = 1/10 {
	match_tag name addr_n addr_w3 city_pt unit_n odcid
	match_tag name addr_n addr_w3 zip unit_n odcid
	match_tag addr name_f3 city_pt odcid
	match_tag addr name_f3 zip odcid
	match_tag addr name_w3 city_pt odcid
	match_tag addr name_w3 zip odcid
	match_tag addr name_l3 city_pt odcid
	match_tag addr name_l3 zip odcid
	match_tag acct name addr
	match_tag acct addr name
}
// Checking Duplicates
dupit odcid, keep
unique acct_id, by(odcid)
tab acct_id_ct
sort odcid
order dup, after(odcid)
duplicates tag odcid, gen(flag_ma)
recode flag_ma (1/2500 = 1)
tab flag_ma
label variable flag_ma "Multiple Accounts Per Business Premise"
label variable odcid "Unique Business and Premise"
format odcid %15.0g
autoformat
sort odcid
// Save
save "$work/DEC DEP NRP Customers Matched by ODCID.dta", replace

// ID Map
use "$work/DEC DEP NRP Customers Matched by ODCID.dta", clear
keep odcid acct_id acct_id_ct flag_ma
rename acct_id_ct acct_ct
replace flag_ma = acct_ct > 1
duplicates drop
tab flag_ma
// Save
save "$work/DEC DEP NRP Account to ODCID Map.dta", replace

// Full Name and Address Merge
use "$work/DEC DEP NRP - Part 2 - Full Name Address.dta", clear
merge 1:1 acct_id using "$work/DEC DEP NRP Account to ODCID Map.dta"
order odcid acct_id acct_ct flag_ma
assert _merge == 3
drop _merge
stdlabels
label values flag_ma YN
sort odcid acct_id
strtrim saddr, proper smart
merge 1:1 acct_id using "$work/DEC DEP NRP - Part 3 - Geocoded Addresses.dta"
drop if _merge == 2
assert _merge == 3
drop _merge
// Save
save "$work/DEC DEP NRP - Part 5 - Full Account Level Data.dta", replace // Kai Stopped Here 3/15/2019
openit map


// Firmographics
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index acct_id acct_mngr bldg_type fac_desc util lead_source or_rep
drop index
duplicates drop
strremove "0"
dupit acct_id
replace fac_desc = "" if fac_desc == bldg_type
replace fac_desc = "" if fac_desc == "School" & regexm(bldg_type, "Educ")
replace fac_desc = "" if fac_desc == "Other"
replace bldg_type = fac_desc if bldg_type == "Office" & fac_desc == "Medium Office"
replace fac_desc = "" if fac_desc == bldg_type
replace fac_desc = "" if fac_desc == "Retail" & regexm(bldg_type, "Ret")
replace fac_desc = "" if fac_desc == "Health" & regexm(bldg_type, "Health")
replace fac_desc = "" if fac_desc == "Health" & regexm(bldg_type, "Care")
replace fac_desc = "" if fac_desc == "Restaurant" & regexm(bldg_type, "Rest")
replace bldg_type = fac_desc if bldg_type == "Other" & fac_desc == "Retail"
replace fac_desc = "" if fac_desc == bldg_type
replace fac_desc = "" if fac_desc == "Hotel" & regexm(bldg_type, "Lodg")
rename bldg_type seg
rename fac_desc seg_alt
order seg_alt, after(seg)
replace lead_source = "Website (Duke Energy)" if lead_source == "Web site (Duke Energy)"
replace lead_source = "Unknown" if lead_source == "0" | lead_source == ""
tab lead_source, gen(lead)
labelfix
drop lead_source
rename lead* lead_*
stubren lead_ "1 2 3 4 5 6 7 8 9 10 11" "vend rep email fb socm print radio uk dukeweb web word"
replace seg = regexr(seg, "Education/K-12", "Education, K-12")
replace seg = regexr(seg, "Water / ", "Water, ")
replace seg = regexr(seg, "Worship/", "Worship, ")
replace seg = regexr(seg, "Church/", "Church, ")
tab seg
replace seg = regexr(seg, "/", " or ")
replace seg = regexr(seg, ",", " or ")
strtrim seg
magicdedup util acct_mngr seg seg_alt or_rep, uid(acct_id)
drop over10
gen dedup = deduped != ""
labcollapse (max) lead_*, by(acct_id util acct_mngr seg seg_alt or_rep dedup)
dupit acct_id, assert
replace seg = regexr(seg, "/Other$", "")
replace seg = regexr(seg, "^Other/", "")
replace seg = regexr(seg, "/Other/", "/")
replace seg = regexr(seg, "^/", "")
replace seg = "Medium Office" if seg == "Medium Office/Office"
replace seg = seg + "/" + seg_alt if seg_alt != ""
replace seg = regexr(seg, "^/", "")
drop seg_alt
tab seg
label variable acct_id "Account ID"
label variable acct_mngr "Assigned Account Manager"
label variable seg "Building Type"
label variable or_rep "Outreach Representative"
label variable lead_vend "Lead Source is Contractor/Vendor"
label variable lead_rep "Lead Source is Duke Energy Representative"
label variable lead_email "Lead Source is Email/Newsletter"
label variable lead_socm "Lead Source is Other Social Media"
label variable lead_radio "Lead Source is Radio"
label variable lead_uk "Lead Source is Unknown"
label variable lead_dukeweb "Lead Source is Website (Duke Energy)"
label variable lead_web "Lead Source is Website (Other)"
label variable lead_word "Lead Source is Word of Mouth"
label variable lead_fb "Lead Source is Facebook"
label variable lead_print "Lead Source is Print Ads"
order lead_uk, last
egen any_lead = rowtotal(lead_vend lead_rep lead_email lead_socm lead_radio lead_dukeweb lead_web lead_word lead_fb lead_print)
replace lead_uk = 0 if any_lead != 0
recode lead_vend lead_rep lead_email lead_socm lead_radio lead_dukeweb lead_web lead_word lead_fb lead_print (0 = .) if lead_uk
stdlabels
label values dedup lead_* YN
drop any_lead
label variable dedup "Firmographic Information Deduped"
strtrim
compress
autoformat
// Save
save "$work/DEC DEP NRP - Part 6 - Account Firmographics No Map.dta", replace

// Export Segment Map
use "$work/DEC DEP NRP - Part 6 - Account Firmographics No Map.dta", clear
keep if regexm(seg, "/")
keep acct_id seg
merge 1:1 acct_id using  "$work/DEC DEP NRP - Part 5 - Full Account Level Data.dta"
drop if _merge == 2
sort odcid
drop odcid acct_ct flag_ma acct1 acct2
drop cust_alt geoid longitude latitude geo_qual _merge
gen seg_new = seg
order seg_new, after(seg)
// Export
*export excel using "$map/Segment Mapping by Account ID.xlsx", sheet("Seg Map") firstrow(variables) sheetreplace
openit map

// Apply Segment Mapping
import excel using "$map/Segment Mapping by Account ID.xlsx", sheet("Seg Map") firstrow clear
keep acct_id seg_new
tab seg_new
// Save
save "$work/Revised Segments.dta", replace

// Merge in New
use "$work/DEC DEP NRP - Part 6 - Account Firmographics No Map.dta", clear
merge 1:1 acct_id using "$work/Revised Segments.dta"
replace seg = seg_new if seg_new != ""
drop seg_new
tab seg
assert _merge != 2
drop _merge
// Save
save "$work/DEC DEP NRP - Part 6 - Account Firmographics.dta", replace

// Projects
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge m:1 acct_id using "$work/DEC DEP NRP Account to ODCID Map.dta"
order odcid
keep index acct_id odcid date_app date_check date_inst date_ship date_start date_upd vend_id
/* Check
histogram date_check
*/
fix_date_ship
// Quick Summary
dates_summary
toexcel "date_range"

// Projects
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge m:1 acct_id using "$work/DEC DEP NRP Account to ODCID Map.dta"
order odcid
keep index acct_id odcid date_app date_check date_inst date_ship date_start date_upd vend_id
/* Check
histogram date_check
*/
fix_date_ship
dropemptyr date_ship date_inst date_app date_check
drop if date_ship == . | date_inst == . | date_check == .
sum
// Quick Summary
dates_summary
toexcel "date_range_comp"

// Projects - Round 1
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge m:1 acct_id using "$work/DEC DEP NRP Account to ODCID Map.dta"
order odcid
assert _merge == 3
drop _merge
merge 1:1 partid using "$work/DEC DEP MidSt Variables To Add.dta", update replace
assert _merge != 2
drop _merge
keep index acct_id odcid date_app date_check date_inst date_start date_upd date_stat date_ship date_sales check_no enr_no vend_id
fix_date_ship
foreach var of varlist date_* {
	replace `var' = round(`var')
	replace `var' = . if `var' < td(01jan2010)
	replace `var' = . if `var' > td(01jan2019)
}
// Define Projects - Find Project Clusters
sort odcid date_start index
gen proj_id = _n + 30000
order proj_id
forvalues i = 1/10 {
	foreach var of varlist date_start check_no enr_no {
		sort odcid vend_id `var' proj_id
		if regexm("`:type `var''", "str") {
			by odcid vend_id `var': replace proj_id = proj_id[1] if `var' != ""
		}
		else {
			by odcid vend_id `var': replace proj_id = proj_id[1] if `var' != .
		}
	}
}
// Unique Counts
unique date_start, by(proj_id)
tab date_start_ct
unique proj_id, by(odcid)
tab proj_id_ct
sort odcid proj_id date_start
rename date_start_ct start_ct
autoformat
// Enrollment Numbers - Different For Different Projects
unique enr_no, by(proj_id)
tab enr_no_ct
// Save
save "$work/Projects IDs With Index.dta", replace


// Drop Index
use "$work/Projects IDs With Index.dta", clear
drop index acct_id
duplicates drop
foreach var of varlist date_start date_upd date_inst date_app date_check date_stat date_ship date_sales check_no enr_no {
	spreshape `var', i(proj_id)
}
foreach var in date_start date_upd date_inst date_app date_check date_stat date_ship {
	egen min_`var' = rowmin(`var'*)
	egen max_`var' = rowmax(`var'*)
	format max_`var' min_`var' %tdNN/DD/CCYY
	gen diff_`var' = max_`var' - min_`var'
	sum diff_`var'
}
sort diff_date_start
duplicates drop
dupit proj_id, assert
gen date_proj = date_start1
format date_proj %tdNN/DD/CCYY
rename start_ct dates_ct
rename (min_* max_*)(*_min *_max)
shrink enr_no?, gen(enr_no)
shrink check_no?, gen(check_no)
rename proj_id_ct proj_ct
order odcid proj_id proj_ct date_proj dates_ct vend_id enr_no check_no
keep odcid proj_id proj_ct date_proj dates_ct vend_id enr_no check_no
*replace proj_id = proj_id
merge m:1 vend_id using "$work/DEC DEP NR-Pres Vendor ID Map Rev.dta"
order vend_name channel, after(vend_id)
drop if _merge == 2
assert _merge == 3
drop _merge
strtrim
compress
autoformat
// Labels
label variable proj_id "Project ID - ODCID, Date, Vendor"
label variable proj_ct "Unique Count of proj_ids by odcid"
label variable date_proj "Project Start Date"
label variable dates_ct "Unique Projects by ODCID"
label variable vend_id "Vendor ID"
label variable enr_no "Associated Enrollment IDs"
label variable check_no "Associated Check Numbers"
order vend_loc, after(vend_name)
label variable vend_name "Vendor/Distributor Name"
label variable vend_loc "Vendor/Distributor Location"
// Save
save "$work/DEC DEP NRP - Part 7 - Project ID Level.dta", replace


// Determine Which SPM IDs Go with which project
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge 1:1 index using "$work/Projects IDs With Index.dta", keepusing(proj_id odcid)
replace meas_qty = -abs(meas_qty) if incen < 0
assert _merge == 3
drop _merge
gen spm_id = spm_id3
spreshape index, i(odcid proj_id m_id spm_id)
shrink index*, gen(indexlist)
keep odcid proj_id m_id m_id3 pm_id serial sp_id sp_id2 spm_id spm_id2 spm_id3 ///
	exag_kw_nc exag_kw_sum exag_kw_wint exag_kwh incen meas_qty meas_unit2 ///
	qty_unit tg_kw_nc tg_kw_sum tg_kw_wint tg_kwh indexlist
order indexlist odcid proj_id m_id m_id3 spm_id spm_id2 spm_id3 exag_kwh exag_kw_nc exag_kw_sum exag_kw_wint ///
	incen meas_qty meas_unit2 qty_unit tg_kw_nc tg_kw_sum tg_kw_wint tg_kwh
autoformat
drop m_id3 spm_id2 spm_id3
// Revised Quantity - 2,632,724
collapse (sum) exag_kw_nc exag_kw_sum exag_kw_wint exag_kwh tg_kwh tg_kw_nc tg_kw_sum tg_kw_wint incen meas_qty, by(indexlist odcid proj_id m_id pm_id spm_id meas_unit)
dupit odcid proj_id m_id spm_id, assert
order pm_id, after(m_id)
rename meas_qty qty_rev
rename (exag_kw_nc exag_kw_sum exag_kw_wint exag_kwh incen)(exag_kw_nc_raw exag_kw_sum_raw exag_kw_wint_raw exag_kwh_raw incen_raw)
order exag_kwh_raw, after(meas_unit2)
rename meas_unit2 meas_unit
order qty_rev, before(meas_unit)
sort qty_rev
sum
drop tg_kwh tg_kw_nc tg_kw_sum tg_kw_wint // These have issues
strtrim
compress
autoformat
// Save
save "$work/Measure Level Raw Totals.dta", replace


// Measure Order with Incentives
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge 1:1 index using "$work/Projects IDs With Index.dta", keepusing(proj_id odcid)
assert _merge == 3
drop _merge
keep index odcid proj_id spm_id3 incen meas_qty
rename *3 *
browseby odcid if incen < 0, nobrowse
gen double abs_incen = abs(incen)
sort odcid proj_id spm_id abs_incen index
order index odcid proj_id spm_id incen
drop flag abs_incen
sort odcid proj_id spm_id index
by odcid proj_id spm_id: gen meas_ord = _n
tab meas_ord
labelfix
// Save
save "$work/Index Level Measure Order Incentives.dta", replace


// Measure Level Data
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
gen spm_id = spm_id3
keep m_id m_id3 pm_id spm_id spm_id2 spm_id3 exag_kw_nc exag_kw_sum exag_kw_wint ///
	exag_kwh incen meas_qty meas_unit2 ///
	meas_desc2 meas_name meas_stat meas_kind ///
	inst_type meas_type prod_code tech 
drop m_id3 spm_id2 spm_id3
foreach var of varlist exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc incen {
	gen double `var'_unit = `var'
	drop `var'
}
drop meas_qty incen incen_unit
duplicates drop
order spm_id
replace tech = "Food Service" if tech == "Food Service Products"
replace tech = "IT" if tech == "Information Technology"
replace tech = "Process" if tech == "Process Equipment"
order tech, after(meas_stat)
merge m:1 tech using "$work/DEC DEP NR-Pres Technology Map.dta"
drop if _merge == 2
assert _merge == 3 | tech == "Agricultural"
drop _merge
order tech_rev, after(tech)
tab tech_rev
replace tech_rev = tech if tech_rev == ""
*dupit spm_id, keep bro
strremove "NULL"
strremove "0"
rename *2 *
magicdedup meas_stat tech tech_rev meas_type meas_kind meas_desc inst_type meas_name prod_code meas_unit, uid(spm_id)
autoformat
dupit spm_id
foreach var of varlist meas_stat tech tech_rev meas_type meas_kind meas_desc inst_type meas_name prod_code meas_unit {
	replacecmd `var' if regexm(`var', "/")
}
replace meas_stat = "Paid" if meas_stat == "Paid/Rejected"
drop meas_stat
replace meas_kind = "Cool Roof New Replace on Burnout School and DCV Retrofit Retail" if meas_kind == "CoolRoof New Replace on Burnout School/DCV Retrofit Retail"
drop meas_kind meas_desc
drop meas_type
drop over10 deduped
order meas_unit, after(meas_name)
autoformat
rename meas_name meas_desc
rename meas_unit qty_unit
order spm_id pm_id m_id prod_code tech tech_rev meas_desc ///
	exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc qty_unit inst_type
format exag_kwh %10.1fc
format exag_kw_wint exag_kw_sum exag_kw_nc %9.4fc
// Labels
label variable spm_id "Source Program Measure ID"
label variable m_id "Measure ID"
label variable prod_code "Product Code"
label variable tech "Technology"
label variable tech_rev "Revised Technology (Roll-Up)"
label variable meas_desc "Measure Name (Readable Detailed)"
label variable exag_kwh "Gross kWh Savings"
label variable exag_kw_wint "Gross Winter kW Savings"
label variable exag_kw_sum "Gross Summer kW Savings"
label variable exag_kw_nc "Gross Non-Coincident kW Savings"
// Save
save "$work/DEC DEP NRP - Part 8 - Per Unit Savings by SPM ID.dta", replace


// Measure Level Data
use "$work/Measure Level Raw Totals.dta", clear
merge m:1 spm_id using "$work/DEC DEP NRP - Part 8 - Per Unit Savings by SPM ID.dta"
assert _merge == 3
drop _merge
rename incen_raw incen
order incen qty_rev qty_unit, after(meas_desc)
foreach var in exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc {
	gen double `var' = `var'_unit * qty_rev
}
order inst_type, after(meas_desc)
sum exag_kw_nc_raw exag_kw_sum_raw exag_kw_wint_raw exag_kwh_raw exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc
drop exag_kw_nc_raw exag_kw_sum_raw exag_kw_wint_raw exag_kwh_raw
// Labels
label variable proj_id "Project ID"
label variable incen "Incentives"
label variable qty_rev "Revised Quantity"
label variable qty_unit "Customer Measure Quantity Unit Name"
label variable exag_kwh_unit "Gross Per Unit kWh Savings"
label variable exag_kw_wint_unit "Gross Per Unit Winter kW Savings"
label variable exag_kw_sum_unit "Gross Per Unit Summer kW Savings"
label variable exag_kw_nc_unit "Gross Per Unit Non-Coincident kW Savings"
label variable exag_kwh "Total Gross kWh Savings"
label variable exag_kw_wint "Total Gross Winter kW Savings"
label variable exag_kw_sum "Total Gross Summer kW Savings"
label variable exag_kw_nc "Total Gross Non-Coincident kW Savings"
autoformat
format exag_kwh %10.0fc
format exag_kw_wint exag_kw_sum exag_kw_nc %9.2fc
// Save
save "$work/DEC DEP NRP - Part 9 - Measure Level Total Savings.dta", replace

// Accounts to Projects
use "$work/Projects IDs With Index.dta", clear
keep proj_id odcid acct_id
duplicates drop
dupit proj_id, keep // bro
// Save
save "$work/Accounts to Projects Map.dta", replace

// Combine All Datasets
use "$work/DEC DEP NRP - Part 5 - Full Account Level Data.dta", clear
keep odcid acct_id cust cust_alt saddr sunit scity sstate szip geoid longitude latitude geo_qual
magicdedup _all, uid(odcid)
autoformat
drop saddr_alt
drop over10 deduped
drop geoid
foreach var of varlist longitude latitude {
	split `var', parse("/")
	destring `var' `var'?, replace force
	egen `var'avg = rowmean(`var'?)
	replace `var' = `var'avg
	drop `var'avg `var'?
}
replace szip = regexr(szip, "/\.", "")
replace scity = "Chester" if scity == "Chester/Chester"
replace scity = "Clemmons" if scity == "Clemmons/Clemmons"
replace scity = "Durham" if scity == "Durham/Rtp"
replace scity = "Hendersonville" if scity == "E Flat Rock/Hendersonville"
replace scity = "Graham" if scity == "Graham/Graham"
// Save
save "$work/DEC DEP NRP - Part 10 - Full ODCID Level Data.dta", replace

// Additional Details
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
order index partid eer eff eff_type eul fl_tons fr_yr1 lamp_make lamp_model make manuf model op_hrs op_hrs1 op_hrs2 pl_tons
strremove "0"
recode eer eff fl_tons (0 = .)
replace meas_qty = -abs(meas_qty) if incen < 0
gen double qty_rev = meas_qty
// Revised Quantity - 2,632,724
// Update Manufacturer and Model
merge 1:1 partid using "$work/DEC DEP MidSt Variables To Add.dta", update replace
assert _merge != 2
drop _merge
order date_upd date_sales, after(index)
keep index date_upd date_sales eer eff eff_type eul fl_tons fr_yr1 lamp_make lamp_model make manuf model op_hrs op_hrs1 op_hrs2 pl_tons
recode op_hrs op_hrs1 op_hrs2 pl_tons (0 = .)
sort index
combine make manuf
combine make lamp_make
combine model lamp_model
order index make model
compress
autoformat
// Save
save "$work/DEC DEP NRP - Part 11 - Measure Details Gran.dta", replace

// Use Unique Contact ID
use "$work/DEC DEP NRP - Part 4 - Contact List With IDs.dta", clear
spreshape contact, i(contact_id) dropmissing
spreshape contact_type, i(contact_id) dropmissing
rename *1 *
keep contact_id contact contact_type phone ph_type phone_alt email email_alt
duplicates drop
dupit contact_id
spreshape email_alt, i(contact phone email) dropmissing
clonevar contact_id_rev = contact_id
sort contact phone email contact_id
replace contact_id_rev = contact_id_rev[_n-1] if contact == contact[_n-1] & phone == phone[_n-1] & email == email[_n-1]
// Save
save "$work/Unique Contact IDs.dta", replace

// Revised Contact IDs
use "$work/Unique Contact IDs.dta", clear
replace contact_id = contact_id_rev
drop contact_id_rev
duplicates drop
drop email_alt2 email_alt3 email_alt4
rename *1 *
drop phone_alt
duplicates drop
dupit contact_id, keep bro
duplicates drop
strtrim
compress
duplicates drop
// Save
save "$work/Unique Contact IDs Revised.dta", replace

use "$work/DEC DEP NRP - Part 4 - Contact List With IDs.dta", clear
keep index contact_id
// Save
save "$work/Index to Contact ID Map.dta", replace

// Contacts
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index spm_id3
rename spm_id3 spm_id
merge 1:1 index using "$work/Index to Contact ID Map.dta"
drop if _merge == 1
drop _merge
merge 1:1 index using "$work/Projects IDs With Index.dta", keepusing(proj_id odcid)
drop if _merge == 2
assert _merge == 3
drop _merge
drop if contact_id == .
order odcid proj_id contact_id
drop index spm_id
merge m:1 contact_id using "$work/Unique Contact IDs.dta", keepusing(contact_id_rev)
replace contact_id = contact_id_rev
assert _merge == 3
drop contact_id_rev _merge
duplicates drop
sort odcid proj_id contact_id
dupit contact_id, keep
sort odcid proj_id dup contact_id
by odcid proj_id: gen rank = _n
tab rank
merge m:1 contact_id using "$work/Unique Contact IDs Revised.dta"
browseby proj_id if rank == 2, nobrowse
drop if contact == "" & phone == . & flag == 1
sort odcid proj_id dup contact_id
drop if _merge == 2
drop _merge
// Merge in Revisions Here - None
drop if contact == "drop"
sort odcid proj_id rank
by odcid proj_id: replace rank = _n
tab rank
drop dup flag
*reshape wide contact_id contact contact_type phone ph_type phone_alt phone_dump email email_alt1 email_alt2 email_alt3, i(odcid proj_id) j(rank)
*reshape wide contact_id contact contact_type phone phone_alt ph_type email email_alt1 email_alt2 email_alt3, i(odcid proj_id) j(rank)
reshape wide contact_id contact contact_type phone ph_type email email_alt, i(odcid proj_id) j(rank)
capture drop phone_dump2
rename *2 *_alt
drop *3
replace email_alt1 = email_alt if email_alt1 == ""
replace email_alt = "" if email_alt == email_alt1
drop email_alt email_alt_alt
rename *1 *
order odcid proj_id contact_id
label variable odcid "Unique Business and Premise"
label variable proj_id "proj_id"
label variable contact_id "Contact ID"
label variable contact "Contact"
label variable contact_type "Primary Contact"
label variable phone "Phone"
label variable ph_type "Phone Type"
*label variable phone_dump "Phone Dump"
label variable email "Email 1"
label variable email_alt "Email 2"
label variable contact_id_alt "Alternate Contact ID"
label variable contact_alt "Alternate Contact Name"
label variable contact_type_alt "Alternate Contact Type"
label variable phone_alt "Alternate Phone"
label variable ph_type_alt "Alternate Phone Type"
// Save
*save "$work/DEC DEP NRP - Part 12 - Contact List Project Measure.dta", replace
save "$work/DEC DEP NRP - Part 12 - Contact List Project.dta", replace

// Part 13 - Original Contact Information
use "$work/Contact Information Cleaned.dta", clear
drop ta_contact ta_phone ta_phone_mob ta_email
order contact_type, after(contact)
order phone_gen phone_mob phone_biz phone_prem, before(email_gen)
split indexlist, parse("/")
rename indexlist uid
gen order = _n
reshape long indexlist, i(uid order) j(lala)
drop if indexlist == ""
order indexlist
destring indexlist, replace
rename indexlist index
drop uid order lala acct_id
rename * orig_*
rename orig_index index
// Index
duplicates drop
// Save
save "$work/DEC DEP NRP - Part 13 - Contact List Raw.dta", replace

// Target kWh
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index tg_kwh tg_kw_nc tg_kw_sum tg_kw_wint
sort index
// Save
save "$work/DEC DEP NRP - Part 14 - Target Annual Savings.dta", replace

/*_______               ___.   .__                   _____  .__  .__   
\_   ___ \  ____   _____\_ |__ |__| ____   ____     /  _  \ |  | |  |  
/    \  \/ /  _ \ /     \| __ \|  |/    \_/ __ \   /  /_\  \|  | |  |  
\     \___(  <_> )  Y Y  \ \_\ \  |   |  \  ___/  /    |    \  |_|  |__
 \______  /\____/|__|_|  /___  /__|___|  /\___  > \____|__  /____/____/
        \/             \/    \/        \/     \/          */
use "$work/DEC DEP NRP - Part 10 - Full ODCID Level Data.dta", clear
merge 1:m odcid using "$work/DEC DEP NRP - Part 7 - Project ID Level.dta"
assert _merge == 3
drop _merge
merge 1:m proj_id using "$work/DEC DEP NRP - Part 9 - Measure Level Total Savings.dta"
assert _merge == 3
drop _merge
drop incen
merge 1:m odcid proj_id spm_id using "$work/Index Level Measure Order Incentives.dta"
assert _merge == 3
drop _merge
order incen, after(inst_type)
order index
order meas_ord, after(proj_id)
drop qty_rev
// Measure Details
merge 1:1 index using "$work/DEC DEP NRP - Part 11 - Measure Details Gran.dta"
drop if _merge == 2
assert _merge == 3
drop _merge
drop indexlist
rename meas_qty qty_rev
order qty_rev, before(qty_unit)
total qty_rev
foreach var in exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc {
	replace `var' = `var'_unit * qty_rev
}
sum exag_kwh
di %15.0fc r(sum)
// Raw Contacts
merge 1:1 index using "$work/DEC DEP NRP - Part 13 - Contact List Raw.dta"
assert _merge == 3
drop _merge
order orig_contact orig_contact_type orig_phone_gen orig_phone_mob orig_phone_biz orig_phone_prem orig_email_gen orig_email_biz, after(geo_qual)
// Contacts
merge m:1 odcid proj_id using "$work/DEC DEP NRP - Part 12 - Contact List Project.dta"
order contact_id contact contact_type phone ph_type phone_alt /*phone_dump*/ email email_alt contact_id_alt contact_alt ph_type_alt contact_type_alt, after(geo_qual)
drop if _merge == 2
drop _merge
order make model eer eff eff_type eul fl_tons fr_yr1 op_hrs op_hrs1 op_hrs2 pl_tons, after(qty_unit)
replace check_no = "" if check_no == "0"
gen proj_tech = string(proj_id) + tech_rev
order proj_tech, after(geo_qual)
label variable proj_tech "Project Technology"
rename odcid bizprem
// Merge in Target Savings
merge 1:1 index using "$work/DEC DEP NRP - Part 14 - Target Annual Savings.dta"
order tg_kwh tg_kw_nc tg_kw_sum tg_kw_wint, after(exag_kw_nc)
order date_upd date_sales, after(date_proj)
assert _merge == 3
drop _merge
strtrim
compress
autoformat
displaylabel index proj_ct spm_id
label variable proj_ct "Unique Count of Project IDs by Business Premise"
label variable spm_id "Source Program Measure ID"
assert tech == tech_rev
drop tech_rev
// Save
save "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", replace


// Export Raw Data
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
merge 1:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset.dta", keepusing(proj_id bizprem meas_ord proj_tech tech)
gen dropped = _merge == 1
stdlabels
label values dropped YN
drop _merge
order index bizprem acct_id proj_id meas_ord proj_tech tech dropped
label variable index "Index"
label variable dropped "Dropped"
drop proj_id bizprem meas_ord proj_tech tech dropped
// Save
save "$work/DEC DEP NonRes Prescriptive Raw Data With Project IDs.dta", replace
labelfix
export excel using "$raw/DEC DEP NRP Raw Dataset Tagged.xlsx" in 1, sheet("Raw Data") firstrow(varlabels) sheetreplace
export excel using "$raw/DEC DEP NRP Raw Dataset Tagged.xlsx", sheet("Raw Data") cell(A2) firstrow(variables) sheetreplace
sumstat
keep order variable varlabel
export excel using "$raw/DEC DEP NRP Raw Dataset Tagged.xlsx", sheet("Variable Dictionary") cell(A3) firstrow(varlabels) sheetmodify


/* Testing
use "$work/DEC DEP NonRes Prescriptive Raw Data With Project IDs.dta", clear
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
merge m:1 proj_id using "$work/Negative Incentive Accounts.dta"
keep if _merge == 3
*/

openit "$map"
oprenit "$af"

