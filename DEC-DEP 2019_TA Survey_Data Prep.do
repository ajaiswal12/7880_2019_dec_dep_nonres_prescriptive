/***************************************
Created By: Evan Tincknell
Creation Date: 2019-09-16

Last Modified By: Sher Khashimov
Modified Date: 2019-09-26

Preparation of DEC-DEP 2019 NRP TA survey data for
-- Process Analysis
-- TA SO Analysis

***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths ("Globals")
global main "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019"
global work "$main\B-Survey Data\TA Survey\Working Files"
global map "$main\B-Survey Data\TA Survey\Mapping Files"
global clean "$main\B-Survey Data\TA Survey\Analysis Files"
global raw "$main\B-Survey Data\TA Survey\Raw Data"
global sample "$main\B-Sampling\TA Survey"
global syntax "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\Data Cleaning\Syntax"
exit



*********************
***GET SAMPLE FILE***
*********************
//Sample includes participant details not included in the survey data; we will need those addiotional details for dispo summary and perhaps more

/*
import excel using "$sample\TA Sampling_FINAL_2019-08-15.xlsx", clear firstrow case(lower) sheet("Sample") cellrange(A2:I956)
drop program1 program2 utility evalperiod
rename (tradeally_name count) (ta_name proj_ct)
drop if ta_email == ""
save "$work/DEC-DEP TA Survey_Final Sample.dta", replace

import excel using "$sample\TA Sampling_FINAL_2019-08-15.xlsx", clear firstrow case(lower) sheet("Project Level") cellrange(A4:D11203)
rename jurisdiction util
gen double proj_ct_dec = util == "DEC"
gen double proj_ct_dep = util == "DEP"
gen double proj_ct_l = tech == "Lighting"
gen double proj_ct_nl = tech != "Lighting"
collapse (sum)proj_ct_dec proj_ct_dep proj_ct_l proj_ct_nl, by(ta_cont_id)
tostring ta_cont_id, replace
gen util = "DEC" if proj_ct_dec > 0
	replace util = "DEP" if proj_ct_dep > 0
	replace util = "DEC/DEP" if proj_ct_dec > 0 & proj_ct_dep > 0
duplicates drop
save "$work/DEC-DEP TA Survey_Project TypeCountJurisdiction by TA Contact ID from Sample.dta", replace
*/



***************
***DATA PREP***
***************

***Import***

import delimited using "$raw\Duke_Energy_Trade_Ally_7880.101.3 (all).csv", clear

***Dropping and re-ordering variables***

drop ïxid start_device start_apple_device sample_pii_program1 sample_pii_program2 sample_pii_utility sample_pii~t sample_pii_evalperiod ///
	sample_provider_id language uas_start last_activity_time start_browser_type_version start_os_version uas_end end_browser_type_version end_os_version
order sample_id sample_pii_name sample_pii_tradeally_name 

***Renaming variables***

rename (sample_pii_name sample_pii_tradeally_name) (name ta_name)

***Incorporate sample fields***
merge 1:1 ta_name name using "$work/DEC-DEP TA Survey_Final Sample.dta", keep(2 3) nogen
merge 1:1 ta_cont_id using "$work/DEC-DEP TA Survey_Project TypeCountJurisdiction by TA Contact ID from Sample.dta", assert(2 3) keep(3) nogen
order ta_cont_id ta_email proj_ct name ta_name util
save "$work/DEC-DEP TA Survey_ALL_Merged.dta", replace

***Incorporate DK/NA flags***

use "$work/DEC-DEP TA Survey_ALL_Merged.dta", clear

*br *98* *96* *998* *9998* to see what variables need to be groupped together
replace sc1a_1 = 9998 if sc1a_9998 == 1
replace ta1_a_1 = 998 if ta1_a_998 == 1
replace ta1_b_1 = 998 if ta1_b_998 == 1
replace ta1_c_1 = 998 if ta1_c_998 == 1
replace ta2b_1 = 998 if ta2b_998 == 1
replace nl2_1 = "Unsure" if nl2_98 == 1
replace f1_1 = 9998 if f1_9998 == 1
replace sc3_pii_1_1 = "Refused" if sc3_pii_96_1 == 1
replace sc3_pii_1_2 = "Refused" if sc3_pii_96_1 == 1
replace l3_1 = "None" if l3_96 == 1
replace nl1a_a = "None" if nl1a_96 == 1
replace nl1b_1 = "None" if nl1b_96 == 1
rename sc1a_1 sc1_a
rename ta1_a_1 ta1a
rename ta1_b_1 ta1b
rename ta1_c_1 ta1c
rename ta2b_1 ta2b
rename nl2_1 nl2
rename f1_1 f1
rename l3_1 l3
rename nl1a_a nl1a
rename nl1b_1 nl1b
recode sat1_a sat1_b sat1_c sat1_d sat1_e sat1_f (12=96)

drop sc1a_9998 ta1_a_998 ta1_b_998 ta1_c_998 ta2b_998 nl2_98 f1_9998 sc3_pii_96_1 sc3_pii_96_1 l3_96 nl1a_96 nl1b_96

save "$work/DEC-DEP TA Survey_ALL_Prep1.dta", replace



*******************
***DISPO SUMMARY***
*******************

//Dispo summary coding 
use "$work/DEC-DEP TA Survey_ALL_Prep1.dta", clear
gen dispo = "Complete" if terminate_location == 0
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == 2
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -302
replace dispo = "Partial complete - survey eligibility unknown" if terminate_location == -1
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -2
replace dispo = "Screened out - no knowledgeable contact" if terminate_location == 1
replace dispo = "Screened out - no knowledgeable contact" if sc2 == 1 & sc3_pii_1_1 == ""
*replace dispo = "Partial complete - completed spillover module" if terminate_location == -302 & pq1 != .
//Exporting the resulting dataset for manual dispo coding
sort ta_cont_id
order ta_cont_id ta_name ta_email dispo
export excel using "$map/DEC-DEP TA Survey_Dispo.xlsx", sheetreplace firstrow(variables) sheet ("Original data")
	//After the team exported the dispo summary with completes and partial completes, 
	//the team manually recorded bounced survey invitation emails to determine 
	//which respondents should be coded as "Bounced email" and which should be 
	//coded as "Auto reply". The respondents that did not qualify for
	//"Complete", "Partial complete", "Bounced email", or "Auto reply" were coded as "No response". 

//Import back and merge the manual dispo coding for final coding and export
import excel using "$map/DEC-DEP TA Survey_Dispo.xlsx", clear firstrow sheet ("Recoded data")
keep ta_cont_id ta_email dispo_final
merge 1:1 ta_cont_id ta_email using "$work/DEC-DEP TA Survey_ALL_Prep1.dta", assert(3) nogen
gen dispo_category = "I" if dispo_final == "Complete"
replace dispo_category = "N" if dispo_final == "Partial complete - survey eligibility confirmed"
replace dispo_category = "U1" if dispo_final == "Partial complete - survey eligibility unknown"
replace dispo_category = "U1" if dispo_final == "No response"
replace dispo_category = "U1" if dispo_final == "Auto reply"
replace dispo_category = "X2" if dispo_final == "Bounced email"

order ta_cont_id name ta_email dispo_final dispo_category
save "$work/DEC-DEP TA Survey_ALL_Prep2.dta", replace 


//Export open end data for backcoding but keep only the relevant variables
use "$work/DEC-DEP TA Survey_ALL_Prep2.dta", clear
drop if sample_id == .
export excel using "$map/DEC-DEP TA Survey_Backcoding.xlsx", sheetreplace firstrow(variables) sheet ("Original data")

//Import and incorporate open end backcoding
import excel using "$map/DEC-DEP TA Survey_Backcoding_2019-09-26.xlsx", clear firstrow sheet("Recoded data")
keep ta_cont_id ta_email scoa_rec* scob_0_rec* scob_1_rec* scob_2_rec* scob_3_rec* scob_4_rec* scob_5_rec* scob_6_rec* scob_7_rec* scob_8_rec* scob_96_rec* ///
	l1_a_rec* l1_b_rec* l1_c_rec* l1_d_rec* l1_e_rec* l1_f_rec* l1_g_rec* l1_o_rec* ///
	f4_0_rec* f4_1_rec* f4_2_rec* f4_3_rec* f4_4_rec* f4_5_rec* f4_6_rec* f4_7_rec* f4_8_rec* f4_9_rec* f4_10_rec*
rename *_rec *_new
save "$work/DEC-DEP TA Survey_Backcoding Imported.dta", replace
use "$work/DEC-DEP TA Survey_ALL_Prep2.dta", clear
merge 1:1 ta_cont_id ta_email using "$work/DEC-DEP TA Survey_Backcoding Imported.dta", assert(1 3) nogen
save "$work/DEC-DEP TA Survey_ALL_Prep3.dta", replace
	
//Recoding
use "$work/DEC-DEP TA Survey_ALL_Prep3.dta", clear

gen f1_rec = 1 if f1 < 10
replace f1_rec = 3 if f1 >50
replace f1_rec = 2 if f1_rec == .
replace f1_rec = 9998 if f1 == 9998

gen f2_rec = 1 if f2 < 10 
replace f2_rec = 3 if f2 >50
replace f2_rec = 2 if f2_rec == .

order f1_rec, after(f1)
order f2_rec, after(f2)

//WINCROSS flags
gen light_nonlight_fl = proj_ct_nl > 0
gen proj_fl = proj_ct >= 5
*gen sat_f1 = sat1_f >= 8
	*replace sat_f1 = 96 if sat1_f > 10
gen dec_dep_fl = 0 if proj_ct_dec > proj_ct_dep
	replace dec_dep_fl = 1 if proj_ct_dec < proj_ct_dep
gen local_fl = 0 if f3 == 1
	replace local_fl = 1 if f3 == 2 | f3 == 3 | f3 == 4
//Clean up
order ta_cont_id sample_id name ta_email ta_name dispo_final dispo_category util proj_ct proj_ct_dec proj_ct_dep proj_ct_l proj_ct_nl
order proj_ct_dec proj_ct_dep proj_ct_l proj_ct_nl, after(proj_ct)
order scoa_new, before(scoa_0_other)
order scob_1_new scob_2_new scob_3_new scob_4_new scob_5_new scob_6_new scob_7_new scob_8_new scob_96_new scob_0_new, before(scob_0_other)
order l1_a_new l1_b_new l1_c_new l1_d_new l1_e_new l1_f_new l1_g_new l1_o_new, before(l1_o)
order f4_1_new f4_2_new f4_3_new f4_4_new f4_5_new f4_6_new f4_7_new f4_8_new f4_9_new f4_10_new f4_0_new, before(f4_0_other)

rename pi5a pi5_o
rename (*_a *_b *_c *_d *_e *_f *_g) (*a *b *c *d *e *f *g)
rename (*_a_new *_b_new *_c_new *_d_new *_e_new *_f_new *_g_new) (*a_new *b_new *c_new *d_new *e_new *f_new *g_new)
autoformat

***Save/export***

//Save ALL
save "$work/DEC-DEP TA Survey_ALL_Clean.dta", replace

//Save slimmed for WINCROSS
use "$work/DEC-DEP TA Survey_ALL_Clean.dta", clear
keep if terminate == 0 | pq1 != .
drop scoa scob_0 scob_1 scob_2 scob_3 scob_4 scob_5 scob_6 scob_7 scob_8 scob_96 ///
	passedfirstscreen sc3_pii_1_1 sc3_pii_1_2 quota_check c1_pii_1 c1_pii_2 ///
	terminate_location start_time end_time reentry duration adjustedduration
save "$work/DEC-DEP TA Survey_Clean Final Data.dta", replace
save "$clean/DEC-DEP TA Survey_Clean Final Data.dta", replace
export excel using "$clean/DEC-DEP TA Survey_Clean Final Data.xlsx", sheetreplace firstrow(variables)
cd "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\C-Process Analysis"
export excel using "DEC-DEP TA Survey_Clean Final Data.xlsx", sheetreplace firstrow(variables)

//Export SO Completes
use "$work/DEC-DEP TA Survey_ALL_Clean.dta", clear
keep if terminate == 0 | pq1 != .
keep sample_id ta_cont_id ta_name sc1 sc1a ///
	pi1a pi1b pi1c pi1d pi1e pi2 pi3a pi3b pi3c pi3d pi3e pi4a pi4b pi5a pi5b pi5_o ///
	ta1a ta1b ta1c ta2a ta2b ta3a ta3b ///
	so1a so1b so1c rs1a rs1b rs1c
order sample_id ta_cont_id ta_name sc1 sc1a ///
	pi1a pi1b pi1c pi1d pi1e pi2 pi3a pi3b pi3c pi3d pi3e pi4a pi4b pi5a pi5b pi5_o ///
	ta1a ta1b ta1c ta2a ta2b ta3a ta3b ///
	so1a so1b so1c rs1a rs1b rs1c
export excel using "$clean\7880 DEC-DEP TA Survey_SO Export_2019-09-25.xlsx", sheetreplace firstrow(variable)




