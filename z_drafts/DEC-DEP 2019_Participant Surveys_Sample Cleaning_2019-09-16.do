/***************************************
Created By: Evan Tincknell
Creation Date: 2019-09-16

Last Modified By: Evan Tincknell
Modified Date: 2019-09-16

Finalize Midstream and Main Channel Participant Survey Samples
(sampling completed, just formatting needed)
--Clean names, company names, addresses, emails
--Confirm all sample variables present in correct order and format

***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths
global main "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019\_Sampling"
global work "$main/Working Files"
exit


/*__  __       _          _____ _                            _ 
 |  \/  |     (_)        / ____| |                          | |
 | \  / | __ _ _ _ __   | |    | |__   __ _ _ __  _ __   ___| |
 | |\/| |/ _` | | '_ \  | |    | '_ \ / _` | '_ \| '_ \ / _ \ |
 | |  | | (_| | | | | | | |____| | | | (_| | | | | | | |  __/ |
 |_|  |_|\__,_|_|_| |_|  \_____|_| |_|\__,_|_| |_|_| |_|\___|*/




/*__  __ _     _     _                            
 |  \/  (_)   | |   | |                           
 | \  / |_  __| |___| |_ _ __ ___  __ _ _ __ ___  
 | |\/| | |/ _` / __| __| '__/ _ \/ _` | '_ ` _ \ 
 | |  | | | (_| \__ \ |_| | |  __/ (_| | | | | | |
 |_|  |_|_|\__,_|___/\__|_|  \___|\__,_|_| |_| |*/

//Get sample
import excel using "", clear firstrow sheet("")

//Clean company names
strtrim company, smart proper
format company %-50s
sort company
replace company = "Kroger" if regexm(company,"KROG")
replace company = "Kroger" if company == "KRO-014-408 C/O S M R"
replace company = "Walmart" if regexm(company,"WAL-MART")
replace company = "Walgreens" if regexm(company,"WALGREEN")
replace company = "ORIENTAL WALK REST" if company == "YEU S WONG ORIENTAL WALK REST"
replace company = "CINCINNATI REDS ACCOUNTING DEPARTMENT" if company == "CINCINNATI REDS ATTN: ACCOUNTING DEPT"
replace company = "Blind Ambition" if company == "Dale Digiovenale DBA Blind Ambition"
replace company = "Cincinnati Recreation Dept" if company == "Cincinnati, City of Recreation Dept Caldwell Rec Field Lighting #3"
replace company = "Kenwood Road" if company == "11111 Kenwood Road LLC"
replace company = "Community Multicare Center" if company == "Aaron Handler- Community Multicare Center"
replace company = "5 Mile Center" if company == "5 Mile Center C/O Keller Prope"
replace company = "Abra Auto Body Repair of America" if company == "A B R a Auto Body and Glas"
replace company = "Lazarus Federated Store" if company == "Lazarus Federated Store # Fla 005"
replace company = "Life Time Fitness" if company == "Life Time Fitness Inc Site 16200000"
replace company = "Loveland Self Storage" if company == "Loveland Self Storage C/O Herman & Kittle Properties"
replace company = "Lyondell Basell Equistar Chemicals" if company == "Lyondell/Basell Equistar Chemicals, Lp Debtor in Possession"
replace company = "Cincinnati State Technical and Community College" if company == "Cincinnati State Technical and Community College Tech Inst"
replace company = "CSXT" if company == "Csxt # 16572"
replace company = "1250 Building" if company == "Dave Brooks DBA 1250 Building"
replace company = "Meineke" if company == "Cedric Newberry DBA Meineke Di"
replace company = "Hickory Woods Golf Course" if company == "Dennis Acomb DBA: Hickory Wood"
replace company = "Tylersville Incorporated" if company == "Kilroy Arhces Inc DBA Tylersville 32235 Incorporated"
replace company = "Speedway LLC" if company == "Speedway LLC- 9176"
replace company = "Dave L Withers" if company == "Dave L Withers DBA Withers Imp"
replace company = "Subway" if company == "Bob Moberg DBA"
replace company = "United Sign" if company == "Anthony Maier DBA United Sign"
replace company = "Car Wash Plus" if company == "Bill Austin DBA Car Wash P"
replace company = "Tailwaggers Doggy Daycare" if company == "Ohio Valley Pet Care DBA:Tailwaggers Doggy Daycare"
replace company = "Earls Towing Service" if company == "DBA Earls"
replace company = "Curves" if acct == 140030930
replace company = regexr(company,"Dds","DDS")
replace company = regexr(company,"Bd of Education","Board of Education")
replace company = "Cushman and Wakefield" if regexm(company,"Cushman and Wakefield")
replace company = "ALDI" if regexm(company,"A L D I")
replace company = "Buffalo Wild Wings" if regexm(company,"Buffalo Wild Wings")
replace company = "American Legion" if regexm(company,"American Legion")
replace company = regexr(company,"Ctr","Center")
replace company = regexr(company,"Co$","Company")
replace company = regexr(company,"Inc$","Incorporated")
replace company = regexr(company,"Inc.$","Incorporated")
replace company = regexr(company,"Dsw","DSW")
replace company = "Cincinnati Recreation Dept" if regexm(company,"Cincinnati, City of Rec")
replace company = "Columbia Towers" if regexm(company,"Columbia Towers")
replace company = "Branch Banking & Trust" if regexm(company,"Branch Banking & Trust")
replace company = regexr(company,"Abt","ABT")
replace company = "Bethart Enterprise" if regexm(company,"Bethart Enterprise")
replace company = "McDonald's" if regexm(company,"Kilroy Arches") | regexm(company,"Kilroy Arhces")
replace company = "Valvoline" if regexm(company,"Valvoline")
replace company = "Ohio Pike" if regexm(company,"Ohio Pike")
replace company = "Shantam Inc" if regexm(company,"Shantam Inc")
replace company = "Sinclair Media" if regexm(company,"Sinclair Media")
replace company = "Procter & Gamble" if regexm(company,"Procter & Gamble")
replace company = "Procter & Gamble" if regexm(company,"Procter & Gamble")
replace company = "Ohio Realty Advisors" if regexm(company,"Ohio Realty Advisors")
replace company = "Metro Sewer District" if regexm(company,"Metro Sewer Dist")
replace company = "Allergy & Astma Assoc, LLC" if regexm(company,"DBA Allergy & Astma Assoc, LLC")
replace company = "Meijer" if regexm(company,"Meijer")
replace company = "Real Estate North" if regexm(company,"Real Estate North")
replace company = "Fairfield Lincoln Mercury" if regexm(company,"Fairfield Lincoln Mercury")
replace company = "Pennsylvania Tool Sales & Service" if regexm(company,"Pennsylvania Tool Sales & Serv")

//Addr cleaning - if saddr != maddr, use saddr with "Valued Customer" for name
*br if saddr1 == maddr1 & mstate != "IL" //1 case where mailing address state mistakenly listed as DC
replace mstate = "IL" if maddr1 == "32907 N STONEMANOR DR" & mcity == "GRAYSLAKE"
br maddr1 maddr2 saddr1 saddr2 mcity scity if substr(maddr1,1,3) != substr(saddr1,1,3) ///
	& substr(maddr2,1,3) != substr(saddr1,1,3) ///
	& substr(maddr1,1,3) != substr(saddr2,1,3) ///
	& scity == mcity 
	//match maddr to saddr where actually the same for sake of applying Valued Customer
	replace saddr1 = maddr1 if saddr1 == "UNIT  103, *" 
	replace maddr1 = saddr1 if saddr1 == "6844 W 111TH PL APT 1A"
	replace maddr1 = saddr1 if saddr1 == "1001 SPIROS CT APT 4"
	replace maddr1 = saddr1 if saddr1 == "1101 INDIAN AVE UNIT 1"
	replace maddr1 = saddr1 if saddr1 == "515 GUNDERSEN DR # 106"
	replace maddr1 = saddr1 if saddr1 == "128 N CHURCH ST UNIT 2"
	replace maddr1 = saddr1 if saddr1 == "208 SUNSHINE DR" & maddr1 == "DEBRA CARON"
	replace maddr1 = saddr1 if saddr1 == "1400 E 85TH ST APT 2"
	replace maddr1 = saddr1 if saddr1 == "9104 KENNEDY CT APT 306"
	replace maddr1 = saddr1 if saddr1 == "2216 E 70TH PL BF"
	replace maddr1 = saddr1 if saddr2 == "7836 S HONORE ST"
		replace maddr2 = saddr2 if saddr2 == "7836 S HONORE ST"
	replace maddr1 = saddr1 if saddr1 == "7850 S CONSTANCE AVE APT 204"
	replace maddr1 = saddr1 if saddr1 == "4250S PRINCETON"
	replace maddr1 = saddr1 if saddr1 == "3524 W 64TH ST"
	replace maddr1 = saddr1 if saddr2 == "4259 W WILCOX ST"
	replace maddr1 = saddr1 if saddr2 == "3517 W AINSLIE ST"
	replace maddr1 = saddr1 if saddr1 == "3333 S CARPENTER ST # 1R"
	replace maddr1 = saddr1 if saddr1 == "6921 S CRANDON AVE 1"
	replace maddr1 = saddr1 if saddr1 == "5050 S LAKE SHORE DR"
	replace maddr1 = saddr1 if saddr2 == "3440 S COTTAGE GROVE AVE"
	replace saddr1 = maddr1 if maddr1 == "21 KING ARTHUR CT # 5B"
	replace saddr1 = maddr1 if maddr1 == "385 E DEERPATH RD"
		replace saddr2 = "" if maddr1 == "385 E DEERPATH RD"
	replace saddr1 = maddr1 if maddr1 == "5125 S ABERDEEN ST"
		replace saddr2 = "" if maddr1 == "5125 S ABERDEEN ST"
	replace saddr1 = maddr1 if maddr1 == "18350 W 3000N RD"
		replace saddr2 = "" if maddr1 == "18350 W 3000N RD"
		br
//Addr formatting
gen addr = saddr1 + ", " + saddr2
	replace addr = regexr(addr,", $","")
gen city = scity
gen state = sstate
gen zip = szip
destring zip, replace
strtrim name* addr* city, proper
*partaddr addr
unitparse addr
drop addr_old
rename addr_unit unit
replace unit = "Unit " + unit if regexm(unit,"^[0-9]")
split addr, parse(" ")
	replace unit = addr1 + " " + addr2 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & unit == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr6 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr6 != "" & addr7 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 + " " + addr7 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr7 != "" & addr8 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 + " " + addr7 + " " + addr8 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr8 != ""
br *addr* if addr1 == "Unit" & regexm(addr4,"[0-9]") & unit == ""
	replace unit = "Unit BF 0" if addr == "Unit Bf 0 2046 W Iowa St"
		replace addr = "2046 W Iowa St" if addr == "Unit Bf 0 2046 W Iowa St"
	replace unit = "Unit 1X" if addr == "Unit 1 X 7754 S Aberdeen St"
		replace addr = "7754 S Aberdeen St" if addr == "Unit 1 X 7754 S Aberdeen St"
	replace unit = "Unit 252C, Bldg C" if addr == "Unit 252C Bldg 7 1365 N Hudson Ave"
		replace addr = "1365 N Hudson Ave" if addr == "Unit 252C Bldg 7 1365 N Hudson Ave"
	replace unit = "Unit 540C, Bldg 5" if addr == "Unit 540C Bldg 5 1450 N Sedgwick St"
		replace addr = "1450 N Sedgwick St" if addr == "Unit 540C Bldg 5 1450 N Sedgwick St"
	replace unit = "Unit 101 Key 30347" if addr == "Unit 101 Key 30347 3540 S State St"
		replace addr = "3540 S State St" if addr == "Unit 101 Key 30347 3540 S State St"
	replace unit = "Unit 3123A, Bldg 4" if addr == "Unit 3123A Bldg 4 1450 N Sedgwick St"
		replace addr = "1450 N Sedgwick St" if addr == "Unit 3123A Bldg 4 1450 N Sedgwick St"
	replace unit = "Unit 162A, Bldg 9" if addr == "Unit 162A Bldg 9 1365 N Hudson Ave"
		replace addr = "1365 N Hudson Ave" if addr == "Unit 162A Bldg 9 1365 N Hudson Ave"
	replace unit = "Unit 1" if addr == "Unit 1 1519 8Th Ave"
		replace addr = "1519 8Th Ave" if addr == "Unit 1 1519 8Th Ave"
	replace unit = "Unit 9W 0" if addr == "Unit 9W 0 755 S Nelson Ave"
		replace addr = "755 S Nelson Ave" if addr == "Unit 9W 0 755 S Nelson Ave"
	replace unit = "Unit 2X" if addr == "Unit 2 X 129 E 59Th St"
		replace addr = "129 E 59Th St" if addr == "Unit 2 X 129 E 59Th St"
	replace unit = "Unit 1104X" if addr == "Unit 1104 X 2111 S Clark St"
		replace addr = "2111 S Clark St" if addr == "Unit 1104 X 2111 S Clark St"
replace unit = regexr(unit,"# ", "#")
replace unit = "Unit A" if unit == "A"
replace unit = "Unit B" if unit == "B"
replace unit = "Unit C" if unit == "C"
replace unit = "Unit D" if unit == "D"
replace unit = "Unit E" if unit == "E"
replace unit = "Unit F" if unit == "F"
replace unit = "Unit G" if unit == "G"
replace unit = "Unit H" if unit == "H"
replace unit = "Unit I" if unit == "I"
replace unit = "Unit J" if unit == "J"
replace unit = "Unit K" if unit == "K"
replace unit = regexr(unit, "Ft$", "FT")
replace unit = regexr(unit, "Bw$", "BW")
replace unit = regexr(unit, "Gn$", "GN")
replace unit = regexr(unit, "Sto$", "STO")
replace unit = regexr(unit, "Fbd$", "FBD")
replace unit = regexr(unit, "Frt$", "FRT")
replace unit = regexr(unit, "Grdn$", "GRDN")
replace unit = regexr(unit, "Bse$", "BSE")
replace unit = regexr(unit, "Gs$", "GS")
replace unit = regexr(unit, "Ge$", "GE")
replace unit = regexr(unit, "Dn$", "DN")
replace unit = regexr(unit, "Gark", "")
replace unit = regexr(unit, "Bf$", "BF")
replace unit = regexr(unit, "Rh$", "RH")
replace unit = regexr(unit, "Bn$", "BN")
replace unit = regexr(unit, "Bs$", "BS")
replace unit = regexr(unit, "Bd$", "BD")
replace unit = regexr(unit, "Lr$", "LR")
replace unit = regexr(unit, "Gc$", "GC")
replace unit = regexr(unit, "Unit Publ$", "Unit PUBL")
replace unit = "" if regexm(unit, "Prv Outdr Lght$")
replace unit = "" if unit == "Res"
replace unit = "" if unit == "Rbd"
replace unit = "" if unit == "Hse"
replace unit = "" if unit == "Bldg"
replace unit = "" if unit == "Temp"
replace unit = "" if unit == "Uppr" | unit == "Upr" | unit == "Uppr Upr"
replace unit = regexr(unit, "#", "") if regexm(unit,"#[A-Z]")
drop addr?
br
replace addr = regexr(addr,"Th ","th ")
replace addr = regexr(addr,"Rd ","rd ")
	replace addr = regexr(addr," rd"," Rd")
replace addr = regexr(addr,"St ","st ")
	replace addr = regexr(addr," st"," St")

//Add any missing sample fields
gen incentive = "one of two $100 prizes"
gen double completes = 150
gen tech = "LED" if regexm(meas_desc,"LED")
gen meas_cat = "Standard LED" if regexm(meas_desc,"Standard")
	replace meas_cat = "Reflector LED" if regexm(meas_desc,"Reflector")
	replace meas_cat = "Specialty LED" if regexm(meas_desc,"Specialty")
	replace meas_cat = "LED Fixture" if meas_desc == "Downlight 10W LED (Fixture)"
gen m = month(date)
	gen month = "January" if m==1
		replace month = "February" if m==2
		replace month = "March" if m==3
		replace month = "April" if m==4
		replace month = "May" if m==5
		replace month = "June" if m==6
		replace month = "July" if m==7
		replace month = "August" if m==8
		replace month = "September" if m==9
		replace month = "October" if m==10
		replace month = "November" if m==11
		replace month = "December" if m==12
gen y = year(date)
gen monthyear = month + " of " + string(y)
drop m y month
gen lastyear = "2018"
sort prog odcid date

//Format
order odcid prog phone date acct company address city state zip qty meascount meas1 qty1 meas2 qty2 meas3 qty3 meas4 qty4 meas5 qty5 program1 program2 drop select
keep odcid - select
sort odcid

//Save and export
save "$work/", replace
export excel using "$main/", firstrow(variables) sheetreplace
