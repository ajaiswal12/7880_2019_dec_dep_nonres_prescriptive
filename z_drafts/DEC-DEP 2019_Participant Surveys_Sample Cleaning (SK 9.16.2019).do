/***************************************
Created By: Evan Tincknell
Creation Date: 2019-09-16

Last Modified By: Sher Khashimov
Modified Date: 2019-09-16

Finalize Midstream and Main Channel Participant Survey Samples
(sampling completed, just formatting needed)
--Clean names, company names, addresses, emails
--Confirm all sample variables present in correct order and format

***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths
global main "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019\_Sampling"
global work "$main/Working Files"
exit


/*__  __       _          _____ _                            _ 
 |  \/  |     (_)        / ____| |                          | |
 | \  / | __ _ _ _ __   | |    | |__   __ _ _ __  _ __   ___| |
 | |\/| |/ _` | | '_ \  | |    | '_ \ / _` | '_ \| '_ \ / _ \ |
 | |  | | (_| | | | | | | |____| | | | (_| | | | | | | |  __/ |
 |_|  |_|\__,_|_|_| |_|  \_____|_| |_|\__,_|_| |_|_| |_|\___|*/




/*__  __ _     _     _                            
 |  \/  (_)   | |   | |                           
 | \  / |_  __| |___| |_ _ __ ___  __ _ _ __ ___  
 | |\/| | |/ _` / __| __| '__/ _ \/ _` | '_ ` _ \ 
 | |  | | | (_| \__ \ |_| | |  __/ (_| | | | | | |
 |_|  |_|_|\__,_|___/\__|_|  \___|\__,_|_| |_| |*/

//Get sample
import excel using "$main/Sampling copy.xlsx", clear firstrow

//Clean contact names
replace NAME = trim(NAME)
replace NAME = proper(NAME)

//Clean company names
replace COMPANY = trim(COMPANY)
replace COMPANY = proper(COMPANY)
format COMPANY %-50s
sort COMPANY


replace COMPANY = regexr(COMPANY,"Ymca","YMCA")
replace COMPANY = regexr(COMPANY,"Dds","DDS")
replace COMPANY = regexr(COMPANY,"Mgt","Management")
replace COMPANY = regexr(COMPANY,"Bd Of Ed","Board of Education")
replace COMPANY = regexr(COMPANY,"Dds","DDS")
replace COMPANY = regexr(COMPANY,"Ii","")
replace COMPANY = regexr(COMPANY,"Llc","LLC")
replace COMPANY = regexr(COMPANY,"Pllc","PLLC")
replace COMPANY = regexr(COMPANY,"Bd Ed","Board of Education")
replace COMPANY = regexr(COMPANY," Bapt Ch$","Baptist Church")
replace COMPANY = regexr(COMPANY," Ch$"," Church")
replace COMPANY = regexr(COMPANY," Of Nc$"," of North Carolina")
replace COMPANY = regexr(COMPANY," Of Sc$"," of South Carolina")
replace COMPANY = regexr(COMPANY," Nc$"," North Carolina")
replace COMPANY = regexr(COMPANY,"Us ","Us ")
replace COMPANY = regexr(COMPANY," Of "," of ")
replace COMPANY = regexr(COMPANY,"'S ","'s ")
replace COMPANY = regexr(COMPANY,"Clt","Charlotte")

replace COMPANY = "First Assembly of God" if COMPANY == "1St Assm Of God Wilmington"
replace COMPANY = "Alexan North Hills Apartments" if COMPANY == "4209 Lassiter Mill Rd Apts"
replace COMPANY = "42nd Street Oyster Bar" if COMPANY == "42Nd Street Oyster Bar Company"
replace COMPANY = "Fourth Presbyterian Church" if COMPANY == "4Th Presbyterian Ch"
replace COMPANY = "84 Lumber" if COMPANY == "84 Lumber Company"
replace COMPANY = "AAA - Ballantyne" if COMPANY == "Aaa Car Care Center"
replace COMPANY = "ABF Freight" if COMPANY == "Ab F"
replace COMPANY = "Abbott's Frozen Custard" if COMPANY == "Abbotts Frozen Custard"
replace COMPANY = "ABF Freight" if COMPANY == "Abf Freight Systems"
replace COMPANY = "Accident Reconstruction Analysis, PLLC" if COMPANY == "Accident Reconst Analys"
replace COMPANY = "Ace Energy Corporation" if COMPANY == "Ace Energy Corportation"
replace COMPANY = "ACI Industries LLC" if COMPANY == "Aci Industries"
replace COMPANY = "Advantage Trim & Lumber Co" if COMPANY == "Advantage Trim And Lumber Company"
replace COMPANY = "AGI Shorewood Group US" if COMPANY == "Agi Shorewood Group Us"
replace COMPANY = "AC Corporation" if COMPANY == "Air Condition Corp"
replace COMPANY = "Airgas Operations" if COMPANY == "Airgas Specialty Products"
replace COMPANY = "Yogi Package Shop" if COMPANY == "Akul"
replace COMPANY = "Aldi Distribution Center" if COMPANY == "Aldi"
replace COMPANY = "Drake Dentistry" if COMPANY == "Alex Drake Dds Pa"
replace COMPANY = "Alina V. Peters, DDS, PA" if COMPANY == "Alina V Peters Dds Pa"
replace COMPANY = "Am Pm Enterprises" if COMPANY == "Am Pm Enterprises Ii"
replace COMPANY = "American Legion" if COMPANY == "American Legion Bldg Fd"
replace COMPANY = "American Uniforms Sales, Inc." if COMPANY == "American Uniforms Of Sales"
replace COMPANY = "Amerisource Bergen" if COMPANY == "Amerisourcebergen Drug Corpora"
replace COMPANY = "Ameritek Inc." if COMPANY == "Ameritek Lasrct Dies"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Amfp Iv Governors Point"
replace COMPANY = "Fred Anderson Nissan of Asheville" if COMPANY == "Anderson Nissan Lincoln Mercur"
replace COMPANY = "Anson County Board of Education" if COMPANY == "Anson County Bd Of Education"
replace COMPANY = "Antolin Interiors USA" if COMPANY == "Antolin Interiors Usa"
replace COMPANY = "BB&T Center" if COMPANY == "Arepii Bbt"
replace COMPANY = "Ascension Lutheran Church" if COMPANY == "Ascension Luth Ch"
replace COMPANY = "City of Asheville" if COMPANY == "Asheville City Of"
replace COMPANY = "Smith Campus Ministries" if COMPANY == "Assoc Camp Ministry"
replace COMPANY = "Town of Atlantic Beach" if COMPANY == "Atlantic Beach Town Of"
replace COMPANY = "Augusta Road United Methodist Church" if COMPANY == "Augusta Rd Method Ch"
replace COMPANY = "Auto Research Center of Mooresville NC" if COMPANY == "Auto Research Center Of Mooresville Nc"
replace COMPANY = "Babyfish" if COMPANY == "Babyfish Iii"
replace COMPANY = "Bad Daddy's Burger Bar" if COMPANY == "Bad Daddy Burger Bar Seabd"
replace COMPANY = "Baity's Precision Machining" if COMPANY == "Baitys Precison Machine"
replace COMPANY = "Baker's City Tire" if COMPANY == "Baker'S City Tire"
replace COMPANY = "Baptist State Convention of NC" if COMPANY == "Baptist State Convention Of Nc"
replace COMPANY = "Barker's Creek Baptist Church" if COMPANY == "Barkers Ck Bapt Ch"
replace COMPANY = "Bek Construction Co Inc." if COMPANY == "Bek Const Company"
replace COMPANY = "Benson Area Medical Center Inc." if COMPANY == "Benson Area Med Ctr"
replace COMPANY = "Bethel Baptist Church" if COMPANY == "Bethel Bapt Ch"
replace COMPANY = "Bloomsbury Estates Condo Homeowners Association" if COMPANY == "Bloomsbury Est Condo Hoa"
replace COMPANY = "Blue Doors Storage Fund" if COMPANY == "Blue Doors Storage Fund Ii"
replace COMPANY = "Blue Ridge Pain Management" if COMPANY == "Blue Ridge Pain Management & P C Pa"
replace COMPANY = "BRP Us Inc." if COMPANY == "Brp Us"
replace COMPANY = "BSN Medical, Inc." if COMPANY == "Bsn Medical"
replace COMPANY = "Camp Tekoa" if COMPANY == "Camp Tekoa Inc Of The Wnc Conference Of Umc"
replace COMPANY = "Town of Carolina Beach" if COMPANY == "Carolina Beach Town Of"
replace COMPANY = "Carolina Farm Credit" if COMPANY == "Carolina Farm Credit Aca"
replace COMPANY = "CarolinaEast Physicians" if COMPANY == "Carolinaeast Physicians"
replace COMPANY = "Carteret Vision Center" if COMPANY == "Carteret Eye Center Od Pa"
replace COMPANY = "Town of Cary" if COMPANY == "Cary Town Of"
replace COMPANY = "Roman Catholic Diocese of Charlotte Pastoral Center" if COMPANY == "Catholic Diocese Of"
replace COMPANY = "Jersey Mike's Subs" if COMPANY == "Cb Sub Shop"
replace COMPANY = "Centerville Fire Station" if COMPANY == "Centerville Fire Sta"
replace COMPANY = "Central Park" if COMPANY == "Central Park Ii"
replace COMPANY = "Central Presbyterian Church" if COMPANY == "Central Pres Ch"
replace COMPANY = "CGR Products" if COMPANY == "Cgr Products"
replace COMPANY = "Metroview Pharmacy" if COMPANY == "Chp Metroview-Charlotte Nc Mob Owner"
replace COMPANY = "La France Church of God" if COMPANY == "Church Of God/Church"
replace COMPANY = "CIP Real Estate Property Services" if COMPANY == "Cip Real Estate Property Services"
replace COMPANY = "ClarkPowell" if COMPANY == "Clark-Powell Assoc"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Cmbe"
replace COMPANY = "CMC Corporation" if COMPANY == "Cmc Corporation"
replace COMPANY = "College Place United Methodist Church" if COMPANY == "College Pl Meth Ch"
replace COMPANY = "Community Baptist Church" if COMPANY == "Community Bapt Ch"
replace COMPANY = "Haywood County Solid Waste Convenience Center" if COMPANY == "Consolidatd Waste Svcs Haywood"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Couchell,George"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Courtney Nc"
replace COMPANY = "CPVF II Palisades I LLC" if COMPANY == "Cpvf  Palisades I"
replace COMPANY = "C R Brown Feed" if COMPANY == "Cr Brown"
replace COMPANY = "First Baptist Church" if COMPANY == "Creedmoor Baptist Ch"
replace COMPANY = "Custom A/V Rack" if COMPANY == "Custom Av Rack"
replace COMPANY = "CVP Operations NC" if COMPANY == "Cvp Operations Nc"
replace COMPANY = "Daughtry Jewelers of Goldsboro" if COMPANY == "Daughtry Jewelers Of Golds"
replace COMPANY = "Andrew Perry, DDS" if COMPANY == "Davis And Perry DDS PLLC"
replace COMPANY = "Delaney Radiology" if COMPANY == "Delaney Radiologists Pa"
replace COMPANY = "Denny's Corporate Headquarters" if COMPANY == "Denny'S Corporation"
replace COMPANY = "Diversified Plastics" if COMPANY == "Diversified Plastics In"
replace COMPANY = "Dom Bakeries NС" if COMPANY == "Dom Bakeries Nc"
replace COMPANY = "Drpfc I LLC" if COMPANY == "Drpfc I"
replace COMPANY = "Jackson County Democratic Party Headquarters" if COMPANY == "Dwj C-Democratic Hqts"
replace COMPANY = "East Side Baptist Church" if COMPANY == "East Side Baptist Ch"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Eighty Eight Sb8 Dev"
replace COMPANY = "Elizabeth Baptist Church" if COMPANY == "Elizabeth Bap Ch"
replace COMPANY = "The Employers Association" if COMPANY == "Employers Assoc Caro"
replace COMPANY = "Greenville First Assembly of God" if COMPANY == "First Assem Of God"
replace COMPANY = "First Baptist Church-Salisbury" if COMPANY == "First Bapt Ch"
replace COMPANY = "Full Moon Oyster Bar - Atlantic Beach" if COMPANY == "Fmobab"
replace COMPANY = "Franklin Baptist Church" if COMPANY == "Franklin Baptist Ch"
replace COMPANY = "Friendship Baptist Church" if COMPANY == "Friendship Baptist Church Of R"
replace COMPANY = "Town of Garner" if COMPANY == "Garner Town Of"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Gmri"
replace COMPANY = "City of Goldsboro" if COMPANY == "Goldsboro City Of"
replace COMPANY = "Goodwill Retail Store" if COMPANY == "Goodwill Industries Of Nw Nc"
replace COMPANY = "Gorman Baptist Church" if COMPANY == "Gorman Bapt Ch"
replace COMPANY = "Grace Baptist Church" if COMPANY == "Grace Chapel Bapt Ch"
replace COMPANY = "Greene County Board of Education" if COMPANY == "Greene Co Board of Education"
replace COMPANY = "Greensboro Jewish Federation" if COMPANY == "Greensboro Jewish Fe"
replace COMPANY = "Greenwood District 51 Schools" if COMPANY == "Greenwood Dist 51 Schools"
replace COMPANY = "Harbor Specialties of Beaufort" if COMPANY == "Harbor Specialties Of Bfrt"
replace COMPANY = "He's Alive Church" if COMPANY == "He'S Alive Church"
replace COMPANY = "Heath Church" if COMPANY == "Heath Community Ch"
replace COMPANY = "Hendersonville Golf and Country Club" if COMPANY == "Hendersonvl Cntryclb"
replace COMPANY = "Holiday Inn Raleigh-Durham Airport" if COMPANY == "Hol Rdu Hotel"
replace COMPANY = "Holy Communion Lutheran Church" if COMPANY == "Holy Communion Luthr"
replace COMPANY = "Hopkins Road Animal Hospital" if COMPANY == "Hopkins Road Animal Hospial"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Hra The U Raleigh"
replace COMPANY = "Hta Mpoc LLC" if COMPANY == "Hta-Mpoc"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Lb-Ubs Commerical Mortgage Tr Cmpt Certs Series 2007-C6 Remic"
replace COMPANY = "Marshall Park Apartments & Townhomes" if COMPANY == "Simpson Woodfield Mrshl Pk"
replace COMPANY = "Valued Duke Energy Customer" if COMPANY == "Spc- Tca Bdc"
replace COMPANY = "TCS Event Rentals" if COMPANY == "Tes"
replace COMPANY = "TSRS" if COMPANY == "Tsrs"
replace COMPANY = "UNC Institute Of Marine Science" if COMPANY == "Unc Institute Of Marine Sci"
replace COMPANY = "Town of Weaverville" if COMPANY == "Weaverville Town Of"
replace COMPANY = "William Jae Lee DDS, III PA" if COMPANY == "William Jae Lee DDS i, Pa"
replace COMPANY = "City of Wilmington" if COMPANY == "Wilmington City Of"
replace COMPANY = "Winston's Grille" if COMPANY == "Winstons Oneal"
replace COMPANY = "6501 Weston Owner" if COMPANY == "Wl Ck 6501 Weston Owner"
replace COMPANY = "Wpfii Reiv LLC" if COMPANY == "Wpfii Reiv"

export excel using "$main/Sampling Company Cleanup.xlsx", firstrow(variables) sheetreplace
import excel using "$main/Sampling Company Cleanup.xlsx", clear firstrow

replace COMPANY = regexr(COMPANY,"Nc ","North Carolina ")
replace COMPANY = regexr(COMPANY," Dot"," Department of Transportation")
replace COMPANY = regexr(COMPANY,"Dept"," Department")
replace COMPANY = regexr(COMPANY,"  Department"," Department")

replace COMPANY = "Town of Mt Olive" if COMPANY == "Mt Olive Town Of"
replace COMPANY = "Town of Siler City" if COMPANY == "Siler City Town Of"
replace COMPANY = "City of Oxford" if COMPANY == "Oxford City Of"

//Addr cleaning - if saddr != maddr, use saddr with "Valued Customer" for name
*br if saddr1 == maddr1 & mstate != "IL" //1 case where mailing address state mistakenly listed as DC
replace mstate = "IL" if maddr1 == "32907 N STONEMANOR DR" & mcity == "GRAYSLAKE"
br maddr1 maddr2 saddr1 saddr2 mcity scity if substr(maddr1,1,3) != substr(saddr1,1,3) ///
	& substr(maddr2,1,3) != substr(saddr1,1,3) ///
	& substr(maddr1,1,3) != substr(saddr2,1,3) ///
	& scity == mcity 
	//match maddr to saddr where actually the same for sake of applying Valued Customer
	replace saddr1 = maddr1 if saddr1 == "UNIT  103, *" 
	replace maddr1 = saddr1 if saddr1 == "6844 W 111TH PL APT 1A"
	replace maddr1 = saddr1 if saddr1 == "1001 SPIROS CT APT 4"
	replace maddr1 = saddr1 if saddr1 == "1101 INDIAN AVE UNIT 1"
	replace maddr1 = saddr1 if saddr1 == "515 GUNDERSEN DR # 106"
	replace maddr1 = saddr1 if saddr1 == "128 N CHURCH ST UNIT 2"
	replace maddr1 = saddr1 if saddr1 == "208 SUNSHINE DR" & maddr1 == "DEBRA CARON"
	replace maddr1 = saddr1 if saddr1 == "1400 E 85TH ST APT 2"
	replace maddr1 = saddr1 if saddr1 == "9104 KENNEDY CT APT 306"
	replace maddr1 = saddr1 if saddr1 == "2216 E 70TH PL BF"
	replace maddr1 = saddr1 if saddr2 == "7836 S HONORE ST"
		replace maddr2 = saddr2 if saddr2 == "7836 S HONORE ST"
	replace maddr1 = saddr1 if saddr1 == "7850 S CONSTANCE AVE APT 204"
	replace maddr1 = saddr1 if saddr1 == "4250S PRINCETON"
	replace maddr1 = saddr1 if saddr1 == "3524 W 64TH ST"
	replace maddr1 = saddr1 if saddr2 == "4259 W WILCOX ST"
	replace maddr1 = saddr1 if saddr2 == "3517 W AINSLIE ST"
	replace maddr1 = saddr1 if saddr1 == "3333 S CARPENTER ST # 1R"
	replace maddr1 = saddr1 if saddr1 == "6921 S CRANDON AVE 1"
	replace maddr1 = saddr1 if saddr1 == "5050 S LAKE SHORE DR"
	replace maddr1 = saddr1 if saddr2 == "3440 S COTTAGE GROVE AVE"
	replace saddr1 = maddr1 if maddr1 == "21 KING ARTHUR CT # 5B"
	replace saddr1 = maddr1 if maddr1 == "385 E DEERPATH RD"
		replace saddr2 = "" if maddr1 == "385 E DEERPATH RD"
	replace saddr1 = maddr1 if maddr1 == "5125 S ABERDEEN ST"
		replace saddr2 = "" if maddr1 == "5125 S ABERDEEN ST"
	replace saddr1 = maddr1 if maddr1 == "18350 W 3000N RD"
		replace saddr2 = "" if maddr1 == "18350 W 3000N RD"
		br
//Addr formatting
gen addr = saddr1 + ", " + saddr2
	replace addr = regexr(addr,", $","")
gen city = scity
gen state = sstate
gen zip = szip
destring zip, replace
strtrim name* addr* city, proper
*partaddr addr
unitparse addr
drop addr_old
rename addr_unit unit
replace unit = "Unit " + unit if regexm(unit,"^[0-9]")
split addr, parse(" ")
	replace unit = addr1 + " " + addr2 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & unit == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr6 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr6 != "" & addr7 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 + " " + addr7 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr7 != "" & addr8 == ""
		replace addr = addr3 + " " + addr4 + " " + addr5 + " " + addr6 + " " + addr7 + " " + addr8 if addr1 == "Unit" & !regexm(addr4,"[0-9]") & addr8 != ""
br *addr* if addr1 == "Unit" & regexm(addr4,"[0-9]") & unit == ""
	replace unit = "Unit BF 0" if addr == "Unit Bf 0 2046 W Iowa St"
		replace addr = "2046 W Iowa St" if addr == "Unit Bf 0 2046 W Iowa St"
	replace unit = "Unit 1X" if addr == "Unit 1 X 7754 S Aberdeen St"
		replace addr = "7754 S Aberdeen St" if addr == "Unit 1 X 7754 S Aberdeen St"
	replace unit = "Unit 252C, Bldg C" if addr == "Unit 252C Bldg 7 1365 N Hudson Ave"
		replace addr = "1365 N Hudson Ave" if addr == "Unit 252C Bldg 7 1365 N Hudson Ave"
	replace unit = "Unit 540C, Bldg 5" if addr == "Unit 540C Bldg 5 1450 N Sedgwick St"
		replace addr = "1450 N Sedgwick St" if addr == "Unit 540C Bldg 5 1450 N Sedgwick St"
	replace unit = "Unit 101 Key 30347" if addr == "Unit 101 Key 30347 3540 S State St"
		replace addr = "3540 S State St" if addr == "Unit 101 Key 30347 3540 S State St"
	replace unit = "Unit 3123A, Bldg 4" if addr == "Unit 3123A Bldg 4 1450 N Sedgwick St"
		replace addr = "1450 N Sedgwick St" if addr == "Unit 3123A Bldg 4 1450 N Sedgwick St"
	replace unit = "Unit 162A, Bldg 9" if addr == "Unit 162A Bldg 9 1365 N Hudson Ave"
		replace addr = "1365 N Hudson Ave" if addr == "Unit 162A Bldg 9 1365 N Hudson Ave"
	replace unit = "Unit 1" if addr == "Unit 1 1519 8Th Ave"
		replace addr = "1519 8Th Ave" if addr == "Unit 1 1519 8Th Ave"
	replace unit = "Unit 9W 0" if addr == "Unit 9W 0 755 S Nelson Ave"
		replace addr = "755 S Nelson Ave" if addr == "Unit 9W 0 755 S Nelson Ave"
	replace unit = "Unit 2X" if addr == "Unit 2 X 129 E 59Th St"
		replace addr = "129 E 59Th St" if addr == "Unit 2 X 129 E 59Th St"
	replace unit = "Unit 1104X" if addr == "Unit 1104 X 2111 S Clark St"
		replace addr = "2111 S Clark St" if addr == "Unit 1104 X 2111 S Clark St"
replace unit = regexr(unit,"# ", "#")
replace unit = "Unit A" if unit == "A"
replace unit = "Unit B" if unit == "B"
replace unit = "Unit C" if unit == "C"
replace unit = "Unit D" if unit == "D"
replace unit = "Unit E" if unit == "E"
replace unit = "Unit F" if unit == "F"
replace unit = "Unit G" if unit == "G"
replace unit = "Unit H" if unit == "H"
replace unit = "Unit I" if unit == "I"
replace unit = "Unit J" if unit == "J"
replace unit = "Unit K" if unit == "K"
replace unit = regexr(unit, "Ft$", "FT")
replace unit = regexr(unit, "Bw$", "BW")
replace unit = regexr(unit, "Gn$", "GN")
replace unit = regexr(unit, "Sto$", "STO")
replace unit = regexr(unit, "Fbd$", "FBD")
replace unit = regexr(unit, "Frt$", "FRT")
replace unit = regexr(unit, "Grdn$", "GRDN")
replace unit = regexr(unit, "Bse$", "BSE")
replace unit = regexr(unit, "Gs$", "GS")
replace unit = regexr(unit, "Ge$", "GE")
replace unit = regexr(unit, "Dn$", "DN")
replace unit = regexr(unit, "Gark", "")
replace unit = regexr(unit, "Bf$", "BF")
replace unit = regexr(unit, "Rh$", "RH")
replace unit = regexr(unit, "Bn$", "BN")
replace unit = regexr(unit, "Bs$", "BS")
replace unit = regexr(unit, "Bd$", "BD")
replace unit = regexr(unit, "Lr$", "LR")
replace unit = regexr(unit, "Gc$", "GC")
replace unit = regexr(unit, "Unit Publ$", "Unit PUBL")
replace unit = "" if regexm(unit, "Prv Outdr Lght$")
replace unit = "" if unit == "Res"
replace unit = "" if unit == "Rbd"
replace unit = "" if unit == "Hse"
replace unit = "" if unit == "Bldg"
replace unit = "" if unit == "Temp"
replace unit = "" if unit == "Uppr" | unit == "Upr" | unit == "Uppr Upr"
replace unit = regexr(unit, "#", "") if regexm(unit,"#[A-Z]")
drop addr?
br
replace addr = regexr(addr,"Th ","th ")
replace addr = regexr(addr,"Rd ","rd ")
	replace addr = regexr(addr," rd"," Rd")
replace addr = regexr(addr,"St ","st ")
	replace addr = regexr(addr," st"," St")

//Add any missing sample fields
gen incentive = "one of two $100 prizes"
gen double completes = 150
gen tech = "LED" if regexm(meas_desc,"LED")
gen meas_cat = "Standard LED" if regexm(meas_desc,"Standard")
	replace meas_cat = "Reflector LED" if regexm(meas_desc,"Reflector")
	replace meas_cat = "Specialty LED" if regexm(meas_desc,"Specialty")
	replace meas_cat = "LED Fixture" if meas_desc == "Downlight 10W LED (Fixture)"
gen m = month(date)
	gen month = "January" if m==1
		replace month = "February" if m==2
		replace month = "March" if m==3
		replace month = "April" if m==4
		replace month = "May" if m==5
		replace month = "June" if m==6
		replace month = "July" if m==7
		replace month = "August" if m==8
		replace month = "September" if m==9
		replace month = "October" if m==10
		replace month = "November" if m==11
		replace month = "December" if m==12
gen y = year(date)
gen monthyear = month + " of " + string(y)
drop m y month
gen lastyear = "2018"
sort prog odcid date

//Format
order odcid prog phone date acct company address city state zip qty meascount meas1 qty1 meas2 qty2 meas3 qty3 meas4 qty4 meas5 qty5 program1 program2 drop select
keep odcid - select
sort odcid

//Save and export
save "$work/", replace
export excel using "$main/", firstrow(variables) sheetreplace
