/***************************************
Created By: Kai Zhou
Creation Date: 2018-04-26

Last Modified By: Kai Zhou
Modified Date: 2018-04-26

This files cleans the Trade Ally Information for DEC DEP NRP
***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths
global main "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/5 - DEC-DEP 2019/Data Cleaning"
global raw "Q:/7880-Duke Energy Portfolio Evaluation/_Secure Data/7-Non-Residential Prescriptive/DEC-DEP 2019"
global work "$raw/Working Files"
global map "$raw/Mapping Files"
global qc "$main/QC_Outputs"
global fin "$main/Final_Outputs"
global af "$main/Analysis Files"
global syntax "C:/Users/kzhou/Documents/OpDyn Projects/7880_2019_DEC_DEP_NonRes_Prescriptive"
global lut "D:/Report Resources/Lookup Tables"

// Setting Up Documentation
*DOC "Documentation for"
// Setting Up Excel File
global xso "DEC DEP NR-Pres Program Data Summary Output.xlsx"
do "$syntax/Data Cleaning Modules.do"
exit

// Final Dataset
*use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
*use "$work/DEC DEP NR-Pres Vendor ID Map.dta", clear

  /*_______      .__                  _________.__  __           .____                      .__   
 /   _____/ ____ |  |___  __ ____    /   _____/|__|/  |_  ____   |    |    _______  __ ____ |  |  
 \_____  \ /  _ \|  |\  \/ // __ \   \_____  \ |  \   __\/ __ \  |    |  _/ __ \  \/ // __ \|  |  
 /        (  <_> )  |_\   /\  ___/   /        \|  ||  | \  ___/  |    |__\  ___/\   /\  ___/|  |__
/_______  /\____/|____/\_/  \___  > /_______  /|__||__|  \___  > |_______ \___  >\_/  \___  >____/
        \/                      \/          \/               \/          \/   \/          */      
// Account Data
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
rename (first_contact2 last_contact2 phone_contact)(ta_first ta_last ta_phone)
rename email_contact ta_email
keep index vend_id ta_name ta_email ta_first ta_last ta_phone ta_phone_mob
order index vend_id
strremove "0"
phoneclean ta_phone ta_phone_mob
tab ta_phone_ext
combine ta_phone_mob ta_phone_ext
phoneclean ta_phone_mob
drop ta_phone_orig ta_phone_mob_orig ta_phone_mob_ext
shrink ta_first ta_last, gen(ta_contact) sep(" ")
emailclean ta_email, novalid
drop ta_email_orig
// Save
save "$work/DEC DEP Trade Ally Variables Only.dta", replace


// Clean TA Names
use "$work/DEC DEP Trade Ally Variables Only.dta", clear
replace ta_contact = regexr(ta_contact, "_", " ")
replace ta_contact = regexr(ta_contact, "_", " ")
strtrim ta_contact
foreach var of varlist ta_name ta_contact {
	clonevar `var'_clean = `var'
	order `var'_clean, after(`var')
}
strtrim *_clean, proper
groupit ta_name ta_name_clean ta_contact ta_contact_clean ta_phone ta_phone_mob ta_email, gen(group_id)
// Save
save "$work/TA Group to Index.dta", replace

// Export
use "$work/TA Group to Index.dta", clear
drop index vend_id
duplicates drop
dropemptyr ta_name ta_name_clean ta_contact ta_contact_clean ta_phone ta_phone_mob ta_email
sort group_id ta_name ta_contact
strtrim ta_name_clean, proper smart
// Save
save "$work/DEC DEP Trade Ally Name Contacts Grouped.dta", replace
// Export
*export excel using "$map/TA Name Mapping.xlsx", sheet("TA Map") firstrow(variables) sheetreplace
/*
openit "$map/TA Name Mapping.xlsx"
*/

// Name Mapping
import excel using "$map/TA Name Mapping.xlsx", sheet("TA Map") firstrow clear
keep group_id ta_phone ta_email ta_name ta_name_clean ta_contact ta_contact_clean
order group_id ta_phone ta_email ta_name ta_name_clean ta_contact ta_contact_clean
duplicates drop
split ta_name_clean, parse(" - ")
rename (ta_name_clean2)(ta_loc)
drop ta_name_clean
rename *1 *
strtrim ta_name_clean
dupit group_id ta_phone ta_email ta_name ta_contact, assert
duplicates drop
// Save
save "$work/TA Name Cleaning.dta", replace

// List of TA Standardizations
use "$work/TA Name Cleaning.dta", clear
keep if lower(ta_name) != lower(ta_name_clean)
keep group_id ta_name ta_name_clean
duplicates drop
quotewrap ta*
gen change = ta_name + " -> " + ta_name_clean
keep group_id change
// Export
toexcel "ta_name_changes"

// List of TA Standardizations
use "$work/TA Name Cleaning.dta", clear
keep if lower(ta_contact) != lower(ta_contact_clean)
keep group_id ta_contact ta_contact_clean
duplicates drop
quotewrap ta*
gen change = ta_contact + " -> " + ta_contact_clean
keep group_id change
// Export
toexcel "ta_cont_changes"

// Merge in Name Clean
use "$work/TA Group to Index.dta", clear
drop *_clean
merge m:1 group_id ta_phone ta_email ta_name ta_contact using "$work/TA Name Cleaning.dta"
replace ta_name = ta_name_clean if ta_name_clean != ""
replace ta_contact = ta_contact_clean if ta_contact_clean != ""
assert _merge != 2
drop _merge
drop ta_name_clean ta_contact_clean
order ta_contact, after(ta_name)
sort index
strtrim
// Unique Trade Ally ID
replace group_id = group_id * 17 + 100000 + index * 39
replace group_id = 199999 if ta_name == ""
sort ta_name ta_loc group_id
by ta_name ta_loc: replace group_id = group_id[1]
sort index
rename group_id ta_id
order index
// Save
save "$work/Index to TA ID Map.dta", replace

// TA Contact Level Data
use "$work/Index to TA ID Map.dta", clear
replace ta_contact = "Unknown" if (ta_phone != . | ta_phone_mob != . | ta_email != "") & ta_contact == ""
keep ta_id ta_contact
duplicates drop
drop if ta_contact == ""
dupit ta_contact, keep
gsort ta_id -dup ta_contact
by ta_id: gen rank = _n
gen ta_cont_id = 10 * ta_id + rank
order ta_cont_id, after(ta_id)
// Save
save "$work/Trade Ally Contact ID.dta", replace

*"$work/Index to TA ID Map.dta"
*"$work/Trade Ally Contact ID.dta"

// TA Contact Level Data
use "$work/Index to TA ID Map.dta", clear
replace ta_contact = "Unknown" if (ta_phone != . | ta_phone_mob != . | ta_email != "") & ta_contact == ""
merge m:1 ta_id ta_contact using "$work/Trade Ally Contact ID.dta"
assert _merge != 2
drop _merge
order ta_cont_id, after(ta_id)
order ta_loc, after(ta_name)
// Save
save "$work/Index to TA ID and TA Contact Map.dta", replace

// Dedupe Contacts
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep ta_id ta_cont_id ta_phone ta_phone_mob
rename (ta_phone ta_phone_mob)(phone1 phone2)
duplicates drop
gen index = _n
reshape long phone ext, i(index) j(type)
drop if phone == .
spreshape type, i(phone) dropmissing
shrink type?, gen(ph_type)
replacecmd ph_type
replace ph_type = "General" if ph_type == "1"
replace ph_type = "Mobile" if ph_type == "1/2"
replace ph_type = "Mobile" if ph_type == "2"
drop index
duplicates drop
dupit phone, keep
sort _all
set seed 12321
gen rand = runiform()
gsort ta_id ta_cont_id -ph_type -dup rand
by ta_id ta_cont_id: gen rank = _n
tab rank
drop dup rand
reshape wide phone ext ph_type, i(ta_id ta_cont_id) j(rank)
shrink phone3 phone4 phone5, gen(phone_dump) // phone6
drop *3 *4 *5 // *6
dropempty
drop ph_type2
rename *1 *
rename *2 *_alt
labelfix
// Save
save "$work/Phone Cleaned by TA Contacts.dta", replace

// Dedupe Emails
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep ta_id ta_cont_id ta_email
strtrim ta_email
duplicates drop
replace ta_email = "" if regexm(ta_email, "egineer")
replace ta_email = "" if regexm(ta_email, "elecric")
spreshape ta_email, i(ta_cont_id) dropmissing
swap ta_email1 ta_email2 if regexm(ta_email1, "incentives") & ta_email2 != ""
swap ta_email1 ta_email2 if regexm(ta_email1, "info") & !regexm(ta_email2, "info") & ta_email2 != ""
duplicates drop
shrink ta_email2 ta_email3 ta_email4, gen(ta_email_dump) // ta_email5
rename *1 *
labelfix
// Save
save "$work/Email Cleaned by TA Contacts.dta", replace

// TA ID Level Data
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep ta_id ta_cont_id vend_id ta_name ta_loc ta_contact
replace ta_name = "Unknown" if ta_name == ""
duplicates drop
merge m:1 ta_id ta_cont_id using "$work/Phone Cleaned by TA Contacts.dta"
assert _merge != 2
drop _merge
merge m:1 ta_id ta_cont_id using "$work/Email Cleaned by TA Contacts.dta"
assert _merge != 2
drop _merge
autoformat
unique ta_cont_id, by(ta_id) nomiss
rename ta_cont_id_ct ta_cont_ct
labelfix
merge m:1 vend_id using "$work/DEC DEP NR-Pres Vendor ID Map.dta"
order vend_name channel, after(vend_id)
labelfix
label variable ta_cont_ct "Unique Contacts by TA"
tab vend_name _merge
drop if _merge == 2
assert _merge == 3
drop _merge
// Save
save "$work/DEC DEP Trade Ally Level Data Cleaned V1.dta", replace
// Export
export excel using "$af/DEC DEP NRP Trade Ally List Contact Level V1.xlsx", sheet("Trade Allies") firstrow(variables) sheetreplace


// Count Projects and Savings
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
merge 1:1 index using "$work/Index to TA ID and TA Contact Map.dta"
drop if _merge == 2
drop _merge
keep ta_id vend_id vend_name channel bizprem proj_id proj_tech exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc
unique ta_id, by(proj_tech)
tab ta_id_ct
sort proj_id
gen overlap_proj = ta_id_ct != 1
labcollapse (sum) exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc (max) overlap_proj, by(bizprem proj_id ta_id vend_id vend_name channel)
labcollapse (sum) exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc overlap_proj (count) proj_id, by(bizprem ta_id vend_id vend_name channel)
labcollapse (sum) exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc proj_id overlap_proj (count) bizprem, by(ta_id vend_id vend_name channel)
label variable bizprem "Business Premises"
label variable proj_id "Projects"
label variable ta_id "Trade Ally ID"
order bizprem proj_id overlap_proj, after(ta_id)
gsort -proj_id ta_id
// Save
save "$work/DEC DEP Project Count and Savings by Trade Ally V1.dta", replace
// Export
export excel using "$af/DEC DEP NRP Trade Ally List Contact Level V1.xlsx", sheet("Project Counts") firstrow(variables) sheetreplace


// Wide Trade Ally
use "$work/DEC DEP Trade Ally Level Data Cleaned V1.dta", clear
dupit ta_id
sort ta_id ta_cont_id
set seed 32124
gen double rand = runiform()
sort ta_id
by ta_id: gen rank = _n
keep if rank < 3
drop rand
drop phone_dump ta_email_dump
keep if channel == "Main Channel"
drop ta_cont_ct
drop vend_id vend_name channel
reshape wide ta_cont_id ta_contact ph_type phone phone_alt ta_email, i(ta_id) j(rank)
strtrim
compress
autoformat
labelfix
format ta_cont_id* %12.0g
label variable ta_cont_id1 "Trade Ally Contact ID 1"
label variable ta_contact1 "Trade Ally Contact 1"
label variable ph_type1 "Phone Type 1"
label variable phone1 "Phone 1"
label variable phone_alt1 "Phone Alternate 1"
label variable ta_email1 "Trade Ally Email 1"
label variable ta_cont_id2 "Trade Ally Contact ID 2"
label variable ta_contact2 "Trade Ally Contact 2"
label variable ph_type2 "Phone Type 2"
label variable phone2 "Phone 2"
label variable phone_alt2 "Phone Alternate"
label variable ta_email2 "Trade Ally Email"
label variable ta_name "Trade Ally Business Name: Account Name"
label variable ta_loc "Trade Ally Loc"
order ta_name ta_loc, after(ta_id)
// Save
save "$work/DEC DEP Trade Ally ID Level Wide Limit 2.dta", replace

// Get Contact IDs
use "$work/Index to TA ID and TA Contact Map.dta", clear
// Most Likely Contact ID per TA ID
keep ta_id ta_cont_id
contract _all, freq(counts)
drop if ta_cont_id == .
set seed 123
gen rand = runiform()
gsort ta_id -counts rand ta_cont_id
bysort ta_id: gen rank = _n
drop rand
reshape wide ta_cont_id counts, i(ta_id) j(rank)
*bro if counts1 == counts2
keep ta_id ta_cont_id1
rename *1 *
// Save
save "$work/Random Selected TA Contact ID for Missings.dta", replace

// Get Mapping
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep index ta_id ta_cont_id vend_id
merge m:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", keepusing(proj_tech)
drop index
assert _merge == 3
drop _merge
keep proj_tech ta_id
drop if ta_id == 199999
spreshape ta_id, i(proj_tech)
drop if ta_id2 != .
drop ta_id2
rename *1 *
duplicates drop
// Save
save "$work/TA ID for Missings.dta", replace

// Index Level
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep index ta_id ta_cont_id vend_id
merge m:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", keepusing(proj_tech)
assert _merge == 3
drop _merge
recode ta_id (199999 = .)
merge m:1 proj_tech using "$work/TA ID for Missings.dta", update
assert _merge != 2
drop _merge
sort index
merge m:1 ta_id using "$work/Random Selected TA Contact ID for Missings.dta", update
assert _merge != 2
drop _merge
merge m:1 ta_id ta_cont_id vend_id using "$work/DEC DEP Trade Ally Level Data Cleaned V1.dta"
drop if _merge == 2
drop _merge
rename ph* ta_ph*
// Save
save "$work/DEC DEP Trade Ally Index Level Data Cleaned V1.dta", replace

// Project Tech
use "$work/Index to TA ID and TA Contact Map.dta", clear
keep index ta_id ta_cont_id vend_id
merge m:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", keepusing(proj_tech)
assert _merge == 3
drop _merge
recode ta_id (199999 = .)
merge m:1 proj_tech using "$work/TA ID for Missings.dta", update
assert _merge != 2
drop _merge
sort index
merge m:1 ta_id using "$work/Random Selected TA Contact ID for Missings.dta", update
assert _merge != 2
drop _merge
merge m:1 ta_id ta_cont_id vend_id using "$work/DEC DEP Trade Ally Level Data Cleaned V1.dta"
drop if _merge == 2
drop _merge
rename ph* ta_ph*
drop index
order proj_tech
duplicates drop
dupit proj_tech ta_id
sort proj_tech ta_id
by proj_tech: gen rank = _n
drop if ta_id == . & rank > 2
reshape wide ta_id ta_cont_id ta_cont_ct vend_name channel ta_name ta_loc ta_contact ta_ph_type ta_phone ta_phone_alt ta_phone_dump ta_email ta_email_dump, i(proj_tech) j(rank)
rename *1 *
drop ta_phone_dump ta_email_dump ta_cont_ct2 vend_name2 channel2 ta_name2 ta_loc2 ta_phone_dump2 ta_email_dump2 vend_id
// Save
save "$work/DEC DEP Trade Ally ProjTech Level Data Cleaned V1.dta", replace


// Continue
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
merge 1:1 index using "$work/Index to TA ID and TA Contact Map.dta"
drop if _merge == 2
tab vend_id channel
keep if channel == "Main Channel"
drop _merge
drop ta_id ta_cont_id ta_name ta_loc ta_contact ta_phone ta_phone_mob ta_email
*merge 1:1 index using "$work/DEC DEP Trade Ally Index Level Data Cleaned V1.dta"
merge m:1 proj_tech using "$work/DEC DEP Trade Ally ProjTech Level Data Cleaned V1.dta"
drop if _merge == 2
assert _merge != 2
recode ta_id (. = 199999)
replace ta_name = "N/A" if ta_id == 199999
collapse (sum) exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc incen, ///
	by(proj_tech proj_id vend_id vend_name channel tech ta_id ta_cont_id ta_name ///
	ta_loc ta_contact ta_ph_type ta_phone ta_phone_alt ta_email ///
	ta_id2 ta_cont_id2 ta_contact2 ta_ph_type2 ta_phone2 ta_phone_alt2 ta_email2)
order proj_tech ta_id proj_id tech
dupit proj_tech ta_id, assert
*merge m:1 ta_id using "$work/DEC DEP Trade Ally ID Level Wide Limit 2.dta"
*replace ta_name = "N/A" if ta_id == 199999
bysort proj_id: egen double proj_kwh = total(exag_kwh)
gen ta_pc_kwh = exag_kwh / proj_kwh
order ta_pc_kwh, after(exag_kwh)
drop proj_kwh
label variable exag_kwh "Total Gross kWh Savings"
label variable ta_pc_kwh "% kWh Savings Share Among Trade Allies"
label variable exag_kw_wint "Total Gross Winter kW Savings"
label variable exag_kw_sum "Total Gross Summer kW Savings"
label variable exag_kw_nc "Total Gross Non-Coincident kW Savings"
label variable incen "Total Incentives"
label variable ta_loc "Trade Ally Location"
// Save
save "$work/DEC DEP NRP Trade Ally List Contact Level V1.dta", replace
// Export
export excel using "$af/DEC DEP NRP Trade Ally List Contact Level V1.xlsx", sheet("Proj Tech TA") firstrow(variables) sheetreplace

// State
use "$work/DEC DEP NRP Trade Ally List Contact Level V1.dta", clear
merge 1:1 proj_tech using "$work/DEC DEP NRP - Project Tech Level Dataset V1.dta"
drop if _merge == 2
keep ta_id sstate
spreshape sstate, i(ta_id)
shrink sstate?, gen(state)
tab state
duplicates drop
replace state = "North Carolina" if state == "NC"
replace state = "North and South Carolina" if state == "NC/SC"
replace state = "South Carolina" if state == "SC"
// Save
save "$work/State by TA ID.dta", replace

   /*___  .__    .____________ __                                  ________  .__          __         ._____.           __                       
  /     \ |__| __| _/   _____//  |________   ____ _____    _____   \______ \ |__| _______/  |________|__\_ |__  __ ___/  |_  ___________  ______
 /  \ /  \|  |/ __ |\_____  \\   __\_  __ \_/ __ \\__  \  /     \   |    |  \|  |/  ___/\   __\_  __ \  || __ \|  |  \   __\/  _ \_  __ \/  ___/
/    Y    \  / /_/ |/        \|  |  |  | \/\  ___/ / __ \|  Y Y  \  |    `   \  |\___ \  |  |  |  | \/  || \_\ \  |  /|  | (  <_> )  | \/\___ \ 
\____|__  /__\____ /_______  /|__|  |__|    \___  >____  /__|_|  / /_______  /__/____  > |__|  |__|  |__||___  /____/ |__|  \____/|__|  /____  >
        \/        \/       \/                   \/     \/      \/          \/        \/                      \/                              */
// MidStream Distributors
use "$work/DEC DEP MidSt Variables To Add.dta", clear
keep vend_id payto paddr pcity pstate pzip ocname ocphone ocemail
dist_clean
split payto, parse(" ~ ")
rename (payto1 payto2)(vend_name vend_loc)
order vend_name vend_loc, after(payto)
drop payto
duplicates drop
drop if vend_id == .
sort vend_id
replace paddr = regexr(paddr, "\.", "")
replace paddr = regexr(paddr, "\.", "")
phoneclean ocphone
emailclean ocemail
drop ocemail_orig ocemail_valid
rename p* v_*
rename oc* v_*
// Save
save "$work/DEC DEP Vendor Distributor Contact Information.dta", replace
// Export
export excel using "$af/DEC DEP NRP Trade Ally List Contact Level V1.xlsx", sheet("MidSt Dist") firstrow(variables) sheetreplace




/*________      .__  .__      _________                     .__          
\______   \__ __|  | |  |    /   _____/____    _____ ______ |  |   ____  
 |     ___/  |  \  | |  |    \_____  \\__  \  /     \\____ \|  | _/ __ \ 
 |    |   |  |  /  |_|  |__  /        \/ __ \|  Y Y  \  |_> >  |_\  ___/ 
 |____|   |____/|____/____/ /_______  (____  /__|_|  /   __/|____/\___  >
                                    \/     \/      \/|__|             */
// Sample for TA Survey
use "$work/DEC DEP NRP Trade Ally List Contact Level V1.dta", clear
// Collapse
gen double count = 1
collapse (sum) count, by(ta_id ta_name ta_contact ta_phone ta_email)
// Drop reasons
gen drop = "Missing Phone" if ta_phone == .
	replace drop = "Missing Email" if ta_email == ""
dupit ta_phone, keep // bro
sort ta_phone count
replace drop = "Duplicate Phone" if dup >= 1 & ta_phone == ta_phone[_n+1] & drop == ""
drop dup
dupit ta_email, keep // bro
gsort -ta_email count
replace drop = "Duplicate Email" if dup >= 1 & ta_email == ta_email[_n+1] & drop == ""
drop dup
//Sample vars
gen program1 = "Smart Saver Prescriptive Incentive Program"
gen program2 = "Smart Saver Program"
merge m:1 ta_id using "$work/State by TA ID.dta"
assert _merge == 3
drop _merge
gen evalperiod = "January 1, 2017 to December 31, 2018"
rename (ta_contact ta_email ta_phone) (name email ta_phone)
order drop, last
order program1 program2 state evalperiod, after(name)
tab drop
// Export
export excel using "$af/DEC DEP NRP Trade Ally List Contact Level V1.xlsx", sheet("Sample") firstrow(variables) sheetreplace
