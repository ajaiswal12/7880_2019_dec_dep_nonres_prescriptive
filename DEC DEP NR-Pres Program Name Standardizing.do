/***************************************
Created By: Kai Zhou
Creation Date: 2018-04-11

Last Modified By: Kai Zhou
Modified Date: 2018-04-11

This files imports and cleans the DEC DEP NR-Prescriptive Data
For Desk Reviews and Impacts
***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
set excelxlsxlargefile on

// Set Useful Folder Paths
global main "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/5 - DEC-DEP 2019/Data Cleaning"
global raw "Q:/7880-Duke Energy Portfolio Evaluation/_Secure Data/7-Non-Residential Prescriptive/DEC-DEP 2019"
global work "$raw/Working Files"
global map "$main/Mapping Files"
global qc "$main/QC_Outputs"
global fin "$main/Final_Outputs"
global af "$main/Analysis Files"
global syntax "C:/Users/kzhou/Documents/OpDyn Projects/7880_2019_DEC_DEP_NonRes_Prescriptive"
global lut "D:/Report Resources/Lookup Tables"

// Setting Up Documentation
*DOC "Documentation for"
// Setting Up Excel File
global xso "DEC DEP NR-Pres Program Data Summary Output.xlsx"
exit

// Import Final Data
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
keep bizprem cust cust_alt saddr sunit scity sstate szip
duplicates drop
autoformat
// Save
save "$work/DEC DEP NRP Names Before Cleaning.dta", replace

// Cleaning
use "$work/DEC DEP NRP Names Before Cleaning.dta", clear
clonevar custc = cust
order custc, after(cust)
// Cut Numbers at the End
replace custc = trim(regexr(custc, "#[ ]?[0-9]+$", ""))
replace custc = trim(regexr(custc, " - [ ]?[0-9]+$", ""))
// Replace Entirety
replace custc = "Bath & Body Works" if regexm(custc, "Bath & Body Works")
replace custc = "Chipotle Mexican Grill" if regexm(custc, "Chipotle")
replace custc = "Comfort Inn" if regexm(custc, "Comfort Inn")
replace custc = "Starbucks" if regexm(custc, "Starbuck")
replace custc = "YMCA" if regexm(custc, "Ymca")
replace custc = "Kroger" if regexm(custc, "Kroger")
replace custc = "Target" if regexm(custc, "Target Store")
replace custc = "Walgreens" if regexm(custc, "Walgreen")
replace custc = "Wells Fargo Bank" if regexm(custc, "Wells Fargo Bank")
replace custc = trim(regexr(custc, "Mfg", "Manufacturing"))
// Acronyms
replace custc = regexr(custc, regexs(1) + " " + regexs(2) + " " + regexs(3), regexs(1) + regexs(2) + regexs(3)) if regexm(custc, "^([A-Z]) ([A-Z]) ([A-Z]) ")
replace custc = regexr(custc, regexs(1) + " " + regexs(2), regexs(1) + regexs(2)) if regexm(custc, "^([A-Z]) ([A-Z]) ")
// Suffixes
replace custc = trim(regexr(custc, "L[Ll][Cc]$", ""))
replace custc = trim(regexr(custc, "Inc[\.]?$", ""))
replace custc = trim(regexr(custc, "^Us ", "US "))
replace custc = trim(regexr(custc, " Na$", ""))
replace custc = trim(regexr(custc, regexs(1), "")) if regexm(custc, "( Lp.*)$")
replace custc = regexr(custc, ",$", "")
replace custc = regexr(custc, ",$", "")
replace custc = regexr(custc, "'M", "'m")
replace custc = regexr(custc, "'S", "'s")
replace custc = regexr(custc, "Nc Dot", "NC DOT")
replace custc = regexr(custc, "Asmbly", "Assembly")
replace custc = regexr(custc, "Aple Gold", "Apple Gold")
replace custc = regexr(custc, "Agcy", "Agency")
replace custc = regexr(custc, "Iii", "III")
replace custc = regexr(custc, "Ii", "II")
// Capitalizations Begin
local base Udf Sst Jp Cvs Kbr Ht Qsl Bbw Bww Js Jdmc Jm Jph Pgdt Ts Nsg Rpj Bmba Acme Tmi Hm Cds ///
	Usa Hds Cmc Kfc Rbhhl Mk Cmpc Rsm Ysf La Ips Tgc Kbr Grst Pbdb Abc Gmri Gmmj Gm Pnc Cmbe
foreach b of local base {
	replace custc = regexr(custc, "^`b' ", upper("`b' "))
	replace custc = regexr(custc, "^`b'$", upper("`b'"))
}
// Capitalizations End
local base Dds
foreach b of local base {
	replace custc = regexr(custc, " `b'$", upper(" `b'"))
}
// Errors
replace custc = trim(regexr(custc, "High School High School", "High School"))
replace custc = trim(regexr(custc, "Schl", "School"))
// Abbreviations
replace custc = trim(regexr(custc, "Bd of Ed", "Board of Ed"))
replace custc = trim(regexr(custc, "School Dist$", "School District"))
replace custc = trim(regexr(custc, "School Dist ", "School District "))
replace custc = trim(regexr(custc, "School Distric$", "School District"))
replace custc = trim(regexr(custc, regexs(1), "")) if regexm(custc, "(.*School District).* High School.*")
replace custc = trim(regexr(custc, regexs(1), "")) if regexm(custc, "(.*Community Schools).* High School.*")
replace custc = trim(regexr(custc, regexs(1), "")) + " School" if regexm(custc, "(.*School District).* Elementary$")
replace custc = trim(regexr(custc, regexs(1), "")) + " School" if regexm(custc, "(.*City Schools).* Elementary$")
replace custc = trim(regexr(custc, regexs(1), "")) + " School" if regexm(custc, "(.*Community Schools).* Elementary$")
replace custc = trim(regexr(custc, regexs(1), "")) + " School" if regexm(custc, "(.*School District).* Middle$")
replace custc = trim(regexr(custc, regexs(1), "")) + " School" if regexm(custc, "(.*Community Schools).* Middle$")
replace custc = trim(regexr(custc, regexs(1), "")) if regexm(custc, "(.*School District).* Elementary School")
replace custc = regexr(custc, " - a", " - A")
replace custc = regexr(custc, "Loveland Board of Education ", "") if regexm(custc, "Middle School")
// DBA
replace custc = regexr(custc, regexs(1), "") if regexm(custc, "(.* D[Bb][Aa]) ")
replace custc = trim(regexr(custc, regexs(1), "")) if regexm(custc, "(C/O .*)$")
replace custc = trim(regexr(custc, "#[ ]?[0-9]+$", ""))
// Periods
replace custc = regexr(custc, "^Dr ", "Dr. ")
// Reorder
replace custc = "City of " + regexs(1) if regexm(custc, "(.*), City of")
// Commas
replace custc = trim(regexr(custc, ",$", ""))
// Address
replace saddr = regexr(saddr, " Rt ", " Route ")
replace saddr = regexr(saddr, " Us ", " US ")
replace saddr = trim(regexr(saddr, regexs(1), "")) if regexm(saddr, "Route [0-9]+( .*)$")
gen address = saddr + " in " + scity
// Evan's final company name fixes
replace custc = "Walmart" if regexm(custc, "Wal-Mart")
replace custc = "Sherwin Williams" if regexm(custc, "Sherwin Williams")
replace custc = "CVS" if regexm(custc, "CVS")
replace custc = "Dollar General" if regexm(custc, "Dollar General")
replace custc = "Autozone" if regexm(custc, "Auto Zone")
replace custc = regexr(custc, "Mgmt", "Management")
replace custc = regexr(custc, " -$","")
replace custc = regexr(custc, " Co$", " Company")
strtrim custc
tab custc if regexm(custc, "Aldi")
replace custc = "Aldi" if regexm(custc, "Aldi")
replace custc = "O'Reilly Automotive" if regexm(custc, "O'Reil")
replace custc = "Family Dollar Stores" if regexm(custc, "Family Dollar Stores")
replace custc = "Bojangles Restaurants" if regexm(custc, "Bojang")
replace custc = "Sherwin Williams" if regexm(custc, "Sherwin.*Williams")
replace custc = "Autozone" if regexm(custc, "Autozone")
replace custc = "Papa John's Pizza" if regexm(custc, "Papa John")
drop cust cust_alt
// Save
save "$work/DEC DEP NRP Cleaned Names and Addresses.dta", replace


// Cross Walk for Measure
cd "$map"
import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEO18 MeasList") firstrow clear
keep Technology MeasureDescription MeasureReadIn
rename (_all)(tech_rev meas_desc meas_readin)
// Save
save "$work/DEO18 NRP Measure Read In Map.dta", replace

// MeasList
import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEC-DEP15 MeasList") cellrange(A2) firstrow clear
keep Technology MeasureDescription MeasureReadIn
rename (_all)(tech_rev meas_desc meas_readin)
// Save
save "$work/DEC-DEP15 NRP Measure Read In Map.dta", replace

// MeasList
import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEI16 MeasList") cellrange(A2) firstrow clear
keep MeasureDescription MeasureReadIn
rename (_all)(meas_desc meas_readin)
// Save
save "$work/DEI16 NRP Measure Read In Map.dta", replace

import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEI18 MeasList") firstrow clear
keep Technology MeasureDescription MeasureReadIn
rename (_all)(tech_rev meas_desc meas_readin)
// Save
save "$work/DEI18 NRP Measure Read In Map.dta", replace

import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEO18&DEI18 MeasList") firstrow clear
keep Technology MeasureDescription MeasureReadIn
rename (_all)(tech_rev meas_desc meas_readin)
// Save
save "$work/DEODEI18 NRP Measure Read In Map.dta", replace

// DEC-DEP 2019 List
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
keep tech meas_desc
duplicates drop
dupit meas_desc
// Save
save "$work/List of 2019 Measure Descriptions.dta", replace


// Append All
clear
append using "$work/DEO18 NRP Measure Read In Map.dta"
append using "$work/DEC-DEP15 NRP Measure Read In Map.dta"
append using "$work/DEI16 NRP Measure Read In Map.dta"
append using "$work/DEI18 NRP Measure Read In Map.dta"
append using "$work/DEODEI18 NRP Measure Read In Map.dta"
drop tech_rev
duplicates drop
merge m:1 meas_desc using "$work/List of 2019 Measure Descriptions.dta"
drop if _merge == 1
dupit meas_desc, keep bro
drop _merge
order tech
sort tech meas_desc
// Exported For Evan To Fill Out
*"Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\Data Cleaning\Mapping Files\Measure Mapping Crosswalk.xlsx"
openit "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\Data Cleaning\Mapping Files\Measure Mapping Crosswalk.xlsx"

// Import Map
cd "$map"
import excel using "Measure Mapping Crosswalk-ASF.xlsx", ///
	sheet("DEC DEP19 MeasList") firstrow clear
keep Technology MeasureDescription MeasureReadIn
rename (_all)(tech meas_desc meas_readin)
duplicates drop
dupit meas_desc, assert
// Save
save "$work/DEC DEP2019 NRP Measure Read In Map.dta", replace

// Original Enrollment ID
use "$work/DEC DEP NonRes Prescriptive Import Renames 20190313.dta", clear
keep index partid enr* acct_id
rename enr_no orig_enr_no
rename acct_id orig_acct_id
gen double acct_id_proj_tech = orig_acct_id
// Merge in Project Tech
merge 1:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", keepusing(proj_tech)
assert _merge == 3
drop _merge
spreshape acct_id_proj_tech, i(proj_tech)
tostr acct_id_proj_tech*
shrink acct_id_proj_tech*, gen(acct_id_proj_tech)
autoformat
// Save
save "$work/Original Enrollment IDs by Part ID.dta", replace


// Read-In Map
use "$work/DEC DEP NRP - Full Measure Level Dataset V1.dta", clear
clonevar date = date_proj
format date %tdM_CY
tostring date, force replace usedisplayformat
order date, after(date_proj)
// Cleaned Names
merge m:1 bizprem using "$work/DEC DEP NRP Cleaned Names and Addresses.dta", update replace
order address, before(saddr)
replace cust = custc
drop custc
assert _merge >= 3
drop _merge
// Read-In Measure Categories
merge m:1 tech meas_desc using "$work/DEC DEP2019 NRP Measure Read In Map.dta"
order meas_readin, after(meas_desc)
assert _merge == 3
drop _merge
// Trade Allies
merge 1:1 index using "$work/DEC DEP Trade Ally Index Level Data Cleaned V1.dta"
assert _merge == 3
drop _merge
merge m:1 vend_id using "$work/DEC DEP Vendor Distributor Contact Information.dta"
assert _merge != 2
drop _merge
autoformat
replace date_upd = round(date_upd)
replace date_sales = round(date_sales)
// Revert to Original
merge 1:1 index using "$work/Original Enrollment IDs by Part ID.dta"
drop partid
order orig_enr_no, before(enr_no)
order orig_acct_id, before(acct_id)
assert _merge == 3
drop _merge
// Variable Labels
label variable index "Index"
label variable bizprem "Unique Business and Premise"
label variable orig_acct_id "Original Account ID"
label variable acct_id "Account ID"
label variable cust "Customer Name"
label variable cust_alt "Alternate Customer Name"
label variable address "Address Read-In"
label variable saddr "Premise Address"
label variable sunit "Premise Unit"
label variable scity "Premise City"
label variable sstate "Premise State"
label variable szip "Premise Zip"
label variable longitude "Longitude"
label variable latitude "Latitude"
label variable geo_qual "Geocode Quality"
label variable proj_tech "Project Technology"
label variable contact_id "Contact ID"
label variable contact "Contact"
label variable contact_type "Primary Contact"
label variable phone "Phone"
label variable ph_type "Phone Type"
label variable phone_alt "Alternate Phone"
label variable email "Email 1"
label variable email_alt "Email 2"
label variable contact_id_alt "Alternate Contact ID"
label variable contact_alt "Alternate Contact Name"
label variable ph_type_alt "Alternate Phone Type"
label variable contact_type_alt "Alternate Contact Type"
label variable proj_id "Project ID - ODCID, Date, Vendor"
label variable meas_ord "Measure Order"
label variable proj_ct "Unique Count of Project IDs by Business Premise"
label variable date_proj "Project Start Date"
label variable date "Project Start Date"
label variable dates_ct "Unique Projects by ODCID"
label variable vend_id "Vendor ID"
label variable vend_name "Vendor Name"
label variable channel "Channel"
label variable orig_enr_no "Enrollment ID"
label variable enr_no "Associated Enrollment IDs"
label variable check_no "Associated Check Numbers"
label variable m_id "Measure ID"
label variable pm_id "Program Measure ID"
label variable spm_id "Source Program Measure ID"
label variable meas_unit "Unit of Measure"
label variable prod_code "Product Code"
label variable tech "Technology"
*label variable tech_rev "Revised Technology (Roll-Up)"
label variable meas_desc "Measure Name (Readable Detailed)"
label variable meas_readin "Measure Read-In"
label variable inst_type "Installation Type"
label variable incen "Customer Incentive Amount Dollars"
label variable qty_rev "Customer Measure Quantity"
label variable qty_unit "Customer Measure Quantity Unit Name"
label variable make "Make"
label variable model "Model"
label variable eer "EER"
label variable eff "EER/SEER"
label variable eff_type "EER/SEER Indicator"
label variable eul "Measure Life"
label variable fl_tons "Full Load EER/SEER/Kw/Ton"
label variable fr_yr1 "Free Rider % Year 1"
label variable op_hrs "Annual Operating Hours"
label variable op_hrs1 "Annual Operating Hours (Number 1)"
label variable op_hrs2 "Annual Operating Hours (Number 2)"
label variable pl_tons "Part Load EER/SEER/Kw/Ton"
label variable exag_kwh_unit "Gross Per Unit kWh Savings"
label variable exag_kw_wint_unit "Gross Per Unit Winter kW Savings"
label variable exag_kw_sum_unit "Gross Per Unit Summer kW Savings"
label variable exag_kw_nc_unit "Gross Per Unit Non-Coincident kW Savings"
label variable exag_kwh "Total Gross kWh Savings"
label variable exag_kw_wint "Total Gross Winter kW Savings"
label variable exag_kw_sum "Total Gross Summer kW Savings"
label variable exag_kw_nc "Total Gross Non-Coincident kW Savings"
// Variable Labels
label variable ta_id "Trade Ally ID"
label variable ta_cont_id "Trade Ally Contact ID"
label variable ta_cont_ct "Trade Ally Contact Count"
label variable ta_name "Trade Ally Business Name"
label variable ta_loc "Trade Ally Location"
label variable ta_contact "Trade Ally Contact Name"
label variable ta_ph_type "Trade Ally Phone Type"
label variable ta_phone "Trade Ally Phone"
label variable ta_phone_alt "Trade Ally Phone Alternate"
label variable ta_phone_dump "Trade Ally Phone Dump"
label variable ta_email "Trade Ally Email"
label variable ta_email_dump "Trade Ally Email Dump"
label variable v_addr "Vendor/Distributor Address"
label variable v_city "Vendor/Distributor City"
label variable v_state "Vendor/Distributor State"
label variable v_zip "Vendor/Distributor Zip Code"
label variable v_name "Vendor/Distributor Contact Name"
label variable v_phone "Vendor/Distributor Contact Phone Cleaned"
label variable v_email "Vendor/Distributor Email Cleaned"
// Save
save "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", replace


// Collapse to Project Level
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
gen meas_cat = meas_readin
spreshape meas_desc, i(proj_tech meas_cat)
rename qty_rev qty
labcollapse (sum) incen exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc qty, by(proj_tech meas_desc* meas_cat)
sort proj_tech meas_cat
by proj_tech: gen rank = _n
order meas_desc*, last
reshape wide incen exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc qty meas_cat meas_desc1 meas_desc2 meas_desc3 meas_desc4 meas_desc5 meas_desc6, i(proj_tech) j(rank)
egen double incen = rowtotal(incen*)
egen double exag_kwh = rowtotal(exag_kwh*)
egen double exag_kw_wint = rowtotal(exag_kw_wint*)
egen double exag_kw_sum = rowtotal(exag_kw_sum*)
egen double exag_kw_nc = rowtotal(exag_kw_nc*)
drop incen? exag_kwh? exag_kw_wint? exag_kw_sum? incen?? exag_kwh?? exag_kw_wint?? exag_kw_sum??
drop exag_kw_nc? exag_kw_nc??
dropempty
order incen exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc, after(proj_tech)
label variable incen "Customer Incentive Amount Dollars"
label variable exag_kwh "Total Gross kWh Savings"
label variable exag_kw_wint "Total Gross Winter kW Savings"
label variable exag_kw_sum "Total Gross Summer kW Savings"
label variable exag_kw_nc "Total Gross Non-Coincident kW Savings"
labelfix
displaylabel
// Labels
label variable meas_cat1 "Measure Category 1"
label variable qty1 "Quantity 1"
label variable meas_desc11 "Measure Category 1 Description 1"
label variable meas_desc21 "Measure Category 1 Description 2"
label variable meas_desc31 "Measure Category 1 Description 3"
label variable meas_desc41 "Measure Category 1 Description 4"
label variable meas_desc51 "Measure Category 1 Description 5"
label variable meas_cat2 "Measure Category 2"
label variable qty2 "Quantity 2"
label variable meas_desc12 "Measure Category 2 Description 1"
label variable meas_desc22 "Measure Category 2 Description 2"
label variable meas_desc32 "Measure Category 2 Description 3"
label variable meas_desc42 "Measure Category 2 Description 4"
label variable meas_desc52 "Measure Category 2 Description 5"
label variable meas_desc62 "Measure Category 2 Description 6"
label variable meas_cat3 "Measure Category 3"
label variable qty3 "Quantity 3"
label variable meas_desc13 "Measure Category 3 Description 1"
label variable meas_desc23 "Measure Category 3 Description 2"
label variable meas_desc33 "Measure Category 3 Description 3"
label variable meas_desc43 "Measure Category 3 Description 4"
label variable meas_desc53 "Measure Category 3 Description 5"
label variable meas_desc63 "Measure Category 3 Description 6"
label variable meas_cat4 "Measure Category 4"
label variable qty4 "Quantity 4"
label variable meas_desc14 "Measure Category 4 Description 1"
label variable meas_desc24 "Measure Category 4 Description 2"
label variable meas_desc34 "Measure Category 4 Description 3"
label variable meas_desc44 "Measure Category 4 Description 4"
label variable meas_cat5 "Measure Category 5"
label variable qty5 "Quantity 5"
label variable meas_desc15 "Measure Category 5 Description 1"
label variable meas_desc25 "Measure Category 5 Description 2"
label variable meas_desc35 "Measure Category 5 Description 3"
label variable meas_desc45 "Measure Category 5 Description 4"
label variable meas_cat6 "Measure Category 6"
label variable qty6 "Quantity 6"
label variable meas_desc16 "Measure Category 6 Description 1"
label variable meas_desc26 "Measure Category 6 Description 2"
label variable meas_desc36 "Measure Category 6 Description 3"
label variable meas_desc46 "Measure Category 6 Description 4"
label variable meas_cat7 "Measure Category 7"
label variable qty7 "Quantity 7"
label variable meas_desc17 "Measure Category 7 Description 1"
label variable meas_desc27 "Measure Category 7 Description 2"
label variable meas_cat8 "Measure Category 8"
label variable qty8 "Quantity 8"
label variable meas_desc18 "Measure Category 8 Description 1"
label variable meas_desc28 "Measure Category 8 Description 2"
label variable meas_cat9 "Measure Category 9"
label variable qty9 "Quantity 9"
label variable meas_desc19 "Measure Category 9 Description 1"
label variable meas_desc29 "Measure Category 9 Description 2"
label variable meas_desc39 "Measure Category 9 Description 3"
label variable meas_desc49 "Measure Category 9 Description 4"
label variable meas_desc59 "Measure Category 9 Description 5"
label variable meas_cat10 "Measure Category 10"
label variable qty10 "Quantity 10"
label variable meas_desc110 "Measure Category 10 Description 1"
// Save
save "$work/Project Level Savings and Incentives V4.dta", replace

// Collapse to Project Level
// Project Level
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
drop index orig_acct_id geo_qual meas_ord
drop inst_type qty_rev qty_unit make model eer eff eff_type eul fl_tons fr_yr1 op_hrs op_hrs1 op_hrs2 pl_tons
drop exag_kwh_unit exag_kw_wint_unit exag_kw_sum_unit exag_kw_nc_unit exag_kwh exag_kw_wint exag_kw_sum exag_kw_nc
drop orig_enr_no check_no m_id pm_id spm_id prod_code meas_desc incen meas_unit
drop meas_readin
drop orig_contact orig_contact_type orig_phone_gen orig_phone_mob orig_phone_biz orig_phone_prem orig_email_gen orig_email_biz
drop tg_kwh tg_kw_nc tg_kw_sum tg_kw_wint
drop ta_id ta_cont_id ta_cont_ct ta_name ta_loc ta_contact ta_ph_type ta_phone ta_phone_alt ta_phone_dump ta_email ta_email_dump
drop v_addr v_city v_state v_zip v_name v_phone v_email
spreshape date_upd, i(proj_tech) dropmissing
drop date_sales
order bizprem proj_id proj_tech tech
duplicates drop
dupit proj_id tech, assert
merge 1:1 proj_tech using "$work/Project Level Savings and Incentives V4.dta"
drop phone_alt email_alt contact_id_alt contact_alt ph_type_alt contact_type_alt
assert _merge == 3
drop _merge
// Save
save "$work/DEC DEP NRP - Project Tech Level Dataset V4.dta", replace


// Original Variables Names
use "$work/DEC DEP NR-Pres Manual Renames.dta", clear
keep var_new varlabel
rename (var_new varlabel)(variable orig_label)
duplicates drop
spreshape orig_label, i(variable)
duplicates drop
// Save
save "$work/Original Variable Map.dta", replace

// Manually Made
import excel using "$map/Original Variable Mapping.xlsx", sheet("Sheet1") firstrow clear
// Save
save "$work/Original Variable Map V4.dta", replace


// Export With PII
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
sumstat
keep order variable varlabel
merge 1:1 variable using "$work/Original Variable Map V4.dta", keep(master match) nogenerate
export excel using "$raw/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Variable Dictionary") cell(A3) firstrow(varlabels) sheetreplace
// Full Data
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
export excel using "$raw/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Cleaned Data") firstrow(variables) sheetreplace
// Project Tech
use "$work/DEC DEP NRP - Project Tech Level Dataset V4.dta", clear
export excel using "$raw/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("ProjTech Data") firstrow(variables) sheetreplace
// Facility Type
use "$work/DEC DEP NRP - Facility Type.dta", clear
export excel using "$raw/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Facility Type") firstrow(variables) sheetreplace


// Export Without PII
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
drop orig_acct_id acct_id_proj_tech acct_id cust cust_alt saddr sunit scity sstate szip contact contact_type phone ph_type phone_alt email email_alt contact_id_alt contact_alt ph_type_alt contact_type_alt
drop orig_contact orig_contact_type orig_phone_gen orig_phone_mob orig_phone_biz orig_phone_prem orig_email_gen orig_email_biz
sumstat
keep order variable varlabel
merge 1:1 variable using "$work/Original Variable Map V4.dta", keep(master match) nogenerate
export excel using "$af/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Variable Dictionary") cell(A3) firstrow(varlabels) sheetreplace
// Full Data
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
drop orig_acct_id acct_id cust cust_alt saddr sunit scity sstate szip contact contact_type phone ph_type phone_alt email email_alt contact_id_alt contact_alt ph_type_alt contact_type_alt
drop orig_contact orig_contact_type orig_phone_gen orig_phone_mob orig_phone_biz orig_phone_prem orig_email_gen orig_email_biz
export excel using "$af/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Cleaned Data") firstrow(variables) sheetreplace
// Project Tech
use "$work/DEC DEP NRP - Project Tech Level Dataset V4.dta", clear
drop acct_id cust cust_alt saddr sunit scity sstate szip contact contact_type phone ph_type email
export excel using "$af/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("ProjTech Data") firstrow(variables) sheetreplace
// Facility Type
use "$work/DEC DEP NRP - Facility Type.dta", clear
export excel using "$raw/DEC DEP NRP Full Measure Level Dataset V4.xlsx", sheet("Facility Type") firstrow(variables) sheetreplace


// Matt Request
cd "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\F-Gross Impact Analysis"
import excel using "Main Channel Projects for Sampling 2019-05-29.xlsx", ///
	sheet("ProjTech Data") cellrange(A3) firstrow clear
gen order = _n
dupit proj_tech
keep order proj_tech
// Save
save "$work/Main Channel ProjTech Order for Matt 0529.dta", replace

use "$work/DEC DEP NonRes Prescriptive Import Renames 20190513.dta", clear
keep util state util_state
duplicates drop
keep if util != ""
keep if util_state != ""
replace util_state = regexr(util_state, "Duke", "DEC")
drop state
// Save
save "$work/DEC DEP Utility Code Map.dta", replace


// Utility Definitions
use "$work/DEC DEP NonRes Prescriptive Raw Data With Project IDs.dta", clear
merge m:1 util using "$work/DEC DEP Utility Code Map.dta"
keep index state state_code util_state util
merge 1:1 index using "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", keepusing(proj_tech)
assert _merge == 3
contract proj_tech state_code state util_state util
drop state
drop _freq
duplicates drop
tab state_code util_state
drop util
gen util = substr(util_state, 1, 3)
replace util = "DEC" if proj_tech == "34044Lighting"
drop state_code util_state
duplicates drop
// Save
save "$work/DEC DEP Utility Map by Project Tech.dta", replace


// Identifying Information
use "$work/DEC DEP NRP - Project Tech Level Dataset V4.dta", clear
keep proj_tech cust cust_alt saddr sunit scity sstate szip
merge 1:1 proj_tech using "$work/Main Channel ProjTech Order for Matt 0529.dta"
keep if _merge == 3
drop _merge
order order
sort order
rename order _0529_File_Order
merge 1:1 proj_tech using "$work/DEC DEP Utility Map by Project Tech.dta"
keep if _merge == 3
drop _merge
order util, after(proj_tech)
sort _0529_File_Order
// Export
cd "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\F-Gross Impact Analysis"
export excel using "Main Channel Name and Address 2019-05-29.xlsx", ///
	sheet("PII Data") firstrow(variables) sheetreplace

// For Instrument
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
duplicates drop
order proj_tech
keep proj_tech cust saddr sunit scity szip
duplicates drop
cd "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019"
export excel using "DEC DEP Project Tech Level Names and Addresses.xlsx", ///
	sheet("Names and Addr") firstrow(variables) sheetreplace

// For Instrument
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
order proj_tech meas_readin meas_desc qty_rev qty_unit op_hrs
keep proj_tech meas_readin meas_desc qty_rev qty_unit op_hrs
cd "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019"
export excel using "DEC DEP Project Tech Level Meas and Qty.xlsx", ///
	sheet("Meas and Qty") firstrow(variables) sheetreplace


// For Instrument
use "$work/DEC DEP NRP - Full Measure Level Dataset V4.dta", clear
order proj_tech meas_readin meas_desc qty_rev qty_unit op_hrs
keep proj_tech meas_readin meas_desc qty_rev qty_unit op_hrs
levelsof proj_tech, local(pt)
foreach p of local pt {
	preserve
		keep if proj_tech == "`p'"
		cd "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019\Project Tech Exports"
		export excel using "DEC DEP Project Tech Meas - `p'.xlsx", ///
			sheet("MeasData") firstrow(variables) sheetreplace
	restore
}


