---
title: "2-Raw-Logger-Data-Plots"
author: "Drew Blumenthal"
date: "August 5, 2019"
output:
  html_document:
    df_print: paged
---


```{r,echo=FALSE, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

# Libraries, TZ, file paths, and functions --------------------------------

library(lubridate)
library(data.table)
library(xlsx)
library(haven)
library(ggplot2)
library(bit64)
library(dygraphs)
library(knitr)
library(kableExtra) # For not hideous kable html tables
library(magrittr)

#This is required for the kableExtra functions to work
#In theory one should be able to specify the html format from within the kable table
#But it was not working and unable to override the table defaults of pandoc (?)
options(knitr.table.format = "html") 

options(scipen = 1000)

source(here::here("./Logger Data/0-Paths.R"))

```

```{r,echo=FALSE, include=FALSE}
load(file.path(work, "Raw Formattted Logger Data.RData"))
setkey(logger, odcid, logsn, ts)
logger <- logger[!duplicated(logger[, c("logsn", "ts")])]

logger[, ts := ts + diff_time]


### remove one outlier date
logger[, date := as.IDate(ts)]
logger <- logger[date <= "2019-12-31"]
logger[, date := NULL]

```


```{r,echo=FALSE, include=FALSE}
### data for plots
# setkey(logger, logsn, ts)
# 
# logger[, ts_to := shift(ts, type = "lead"), by = .(logsn)]
# logger[, sec := as.integer(difftime(ts_to, ts, units = "secs"))]
# logger[sec == 0, sec := 1]
# 
# logger_plot <- vector("list")
# n = 0
# 
# system.time(for (i in 1:logger[, .N]) {
# #system.time(for (i in 1:9567) {
#   n = n + 1
#   print(i)
#   logger_plot_i <- logger[i]
#   j = logger_plot_i[, sec]
#   if (!is.na(j)) {
#     logger_plot_i <- logger_plot_i[rep(seq_len(nrow(logger_plot_i)), each = logger_plot_i$sec)]
#     logger_plot_i[, N := 1:unique(logger_plot_i[, sec])]
#     logger_plot_i[, ts_new := ts + (N - 1)]
#     logger_plot_i[, `:=`(date = as.IDate(ts_new, format = "%Y-%m-%d", tz = tz),
#                          hour = hour(ts_new),
#                          minute = minute(ts_new),
#                          second = second(ts_new))]
#     logger_plot_i <- logger_plot_i[!duplicated(logger_plot_i[, c("odcid", "logsn", "date", "hour", "minute", "second")])]
#     logger_plot_i <- logger_plot_i[, .(seconds = .N), keyby = .(logsn, odcid, date, hour, light)]
#     logger_plot_i[, N := n]
#     logger_plot[[n]] <- logger_plot_i
#     rm(logger_plot_i)
#   } else {
#     logger_plot_i[, `:=`(N = 1,
#                          ts_new = ts)]
#     logger_plot_i[, `:=`(date = as.IDate(ts_new, format = "%Y-%m-%d", tz = tz),
#                          hour = hour(ts_new),
#                          minute = minute(ts_new),
#                          second = second(ts_new))]
#     logger_plot_i <- logger_plot_i[!duplicated(logger_plot_i[, c("odcid", "logsn", "date", "hour", "minute", "second")])]
#     logger_plot_i <- logger_plot_i[, .(seconds = .N), keyby = .(logsn, odcid, date, hour, light)]
#     logger_plot_i[, N := n]
#     logger_plot[[n]] <- logger_plot_i
#     rm(logger_plot_i)
#   }
# })
# 
# logger_plot <- rbindlist(logger_plot)
# save(logger_plot, file = file.path(work, "Logger Plots.RData"))

load(file.path(work, "Logger Plots.RData"))

### merge site visit and retrieval dates
logger_dates <- logger[!duplicated(logger[, c("logsn")])]
logger_plot <- merge(logger_plot, logger_dates[, c("logsn", "date_sv", "date_ret", "site_name", "meas_desc")], by = "logsn")
rm(logger_dates)

logger_plot_orig = copy(logger_plot)

```


### Total Seconds per Hour and Day
```{r,echo=FALSE, include=FALSE}
# load(file.path(work, "Logger Plots.RData"))

logger_plot_hour <- logger_plot[, .(Seconds = sum(seconds, na.rm = TRUE)), keyby = .(logsn, odcid, date, hour, light, date_sv, date_ret, site_name, meas_desc)]
logger_plot_hour[, keep := as.integer(all(c(0, 1) %in% light)), by = .(logsn, odcid, date, hour)]
logger_plot_hour <- logger_plot_hour[!(light == 0 & keep == 1)]
logger_plot_hour[light == 0, Seconds := 0]

logger_plot_hour[, hour2 := as.character(hour)]
logger_plot_hour[hour %in% c(0:9), hour2 := paste0("0", hour2)]
logger_plot_hour[, datetime := paste0(date, " ", hour2, ":00:00")]
logger_plot_hour[, datetime := as.POSIXct(datetime, format = "%Y-%m-%d %H:%M:%S", tz = tz)]

setkey(logger_plot_hour, odcid, logsn, datetime, light)
logger_plot_hour[, group := .GRP, by = .(logsn)]

logger_plot_hour_orig = copy(logger_plot_hour)

### add a few variables from master engineering file
load(file.path(work, "Master Engineering File.RData"))
logger_plot_hour <- merge(logger_plot_hour, master_file[, c("logsn", "control_type", "light_spill", "logger_damage", "op_hours")], by = "logsn")

logger_plot_hour[, op_hours_day := op_hours/365]
setnames(logger_plot_hour, old = "op_hours", new = "op_hours_year")

```


```{r, results='asis', echo=FALSE, warning=FALSE}
plots2 <- lapply(1:logger_plot_hour[, uniqueN(group)], function(i) dygraph(logger_plot_hour[group == i, .(datetime, Seconds)],
                    main = paste0("Logger ID: ", logger_plot_hour[group == i, unique(logsn)], " - ",
                                  "Site: ", logger_plot_hour[group == i, unique(site_name)], " - ",
                                  "Measure Description: ", logger_plot_hour[group == i, unique(meas_desc)],
                                  "Control Type: ", logger_plot_hour[group == i, unique(control_type)], " - ",
                                  "Light Spillage: ", logger_plot_hour[group == i, unique(light_spill)], " - ",
                                  "Logger Damage: ", logger_plot_hour[group == i, unique(logger_damage)], " - ",
                                  "Operating HoursDay: ", logger_plot_hour[group == i, unique(op_hours_day)]), 
                    xlab = "Date and Time", 
                    ylab = "Seconds") %>%
                   dyAxis("y", valueRange = c(0, 4000)) %>%
                   dyRangeSelector() %>% 
                   dyOptions(useDataTimezone = TRUE))
htmltools::tagList(plots2)
```

```{r, results='asis', echo=FALSE, warning=FALSE}
# logger_plot_hour <- logger_plot_hour_orig[logsn %in% c("LC09080092", "LL07100119", "LL13080167")]
# plots2 <- lapply(1:logger_plot_hour[, uniqueN(group)], function(i) dygraph(logger_plot_hour[group == i, .(datetime, Seconds)],
#                     main = paste0("ODCID: ", 
#                                   logger_plot_hour[group == i, unique(odcid)], " - ", 
#                                   "Logger ID: ", logger_plot_hour[group == i, unique(logsn)], " - ", 
#                                   "Site Visit Date: ", logger_plot_hour[group == i, unique(date_sv)], " - ",
#                                   "Retrieval Date: ", logger_plot_hour[group == i, unique(date_ret)]), 
#                     xlab = "Date and Time", 
#                     ylab = "Seconds") %>%
#                    dyAxis("y", valueRange = c(0, 4000)) %>%
#                    dyRangeSelector() %>% 
#                    dyOptions(useDataTimezone = TRUE))
# htmltools::tagList(plots2)
```


```{r, results='asis', echo=FALSE, warning=FALSE}
# logger_plot_hour <- logger_plot_hour_orig[logsn %in% c()]
# plots2 <- lapply(1:logger_plot_hour[, uniqueN(group)], function(i) dygraph(logger_plot_hour[group == i, .(datetime, Seconds)],
#                     main = paste0("ODCID: ", 
#                                   logger_plot_hour[group == i, unique(odcid)], " - ", 
#                                   "Logger ID: ", logger_plot_hour[group == i, unique(logsn)], " - ", 
#                                   "Site Visit Date: ", logger_plot_hour[group == i, unique(date_sv)], " - ",
#                                   "Retrieval Date: ", logger_plot_hour[group == i, unique(date_ret)]), 
#                     xlab = "Date and Time", 
#                     ylab = "Seconds") %>%
#                    dyAxis("y", valueRange = c(0, 4000)) %>%
#                    dyRangeSelector() %>% 
#                    dyOptions(useDataTimezone = TRUE))
# htmltools::tagList(plots2)
```

```{r, results='asis', echo=FALSE, warning=FALSE}
# logger_plot_load <- logger_plot[light == 1, .(seconds = sum(seconds, na.rm = TRUE)), keyby = .(odcid, logsn, hour, date_sv, date_ret, site_name, meas_desc)]
# 
# 
# for(i in unique(logger_plot_load$logsn)) {
#   print(ggplot(logger_plot_load[logsn == i], aes(hour, seconds)) + 
#     geom_line() + 
#     scale_x_discrete(name = "Hour Beginning", 
#                      limits = c(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)) + 
#       labs(title = paste0("ODCID: ", 
#                          logger_plot_load[logsn == i, unique(odcid)], " - ", 
#                          "Logger ID: ", logger_plot_load[logsn == i, unique(logsn)]),
#            subtitle = paste0(
#                          "Site Visit Date: ", logger_plot_load[logsn == i, unique(date_sv)], " - ",
#                          "Retrieval Date: ", logger_plot_load[logsn == i, unique(date_ret)], " - ",
#                          "Site: ", logger_plot_load[logsn == i, unique(site_name)], " - ",
#                          "Measure Description: ", logger_plot_load[logsn == i, unique(meas_desc)])) +
#       ylab("Total Seconds"))
# }


```

