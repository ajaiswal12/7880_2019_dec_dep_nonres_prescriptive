# 0-Paths.R
# Project Folders

if (.Platform$OS.type == "unix") {
  project_folder = "/mnt/q/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/5 - DEC-DEP 2019"
} else {
  project_folder = "Q:/7880-Duke Energy Portfolio Evaluation/7-Non-Residential Prescriptive/5 - DEC-DEP 2019"
  Sys.setenv("R_ZIPCMD" = "C:/Rtools/bin/zip.exe")
}

raw_secure <- file.path("Q:/7880-Duke Energy Portfolio Evaluation/_Secure Data/7-Non-Residential Prescriptive/DEC-DEP 2019")
raw_engineering <- file.path("Q:/7880-Duke Energy Portfolio Evaluation/_Secure Data/7-Non-Residential Prescriptive/DEC-DEP 2019/E-Onsite Visits/Completed Data Collection Templates/2 - Retrieved Logger Files/xlsx files")
work <- file.path(project_folder, "E-Onsite Data Collection/Hours of Use Analysis/Working Files")
analysis <- file.path(project_folder, "E-Onsite Data Collection/Hours of Use Analysis/Analysis Files")


tz = "America/New_York"
Sys.setenv(TZ = tz)
