/***************************************
Created By: Evan Tincknell
Creation Date: 2019-09-16

Last Modified By: Sher Khashimov
Modified Date: 2019-10-31

Preparation of Duke DEC-DEP NRP midstream survey data for
--Part Survey Dispo Summary
--Part Survey FR Analysis
--Part Survey SO Analysis
--Part Survey Process Analysis
--Part Survey In-Service Rate Analysis
***************************************/

capture clear all
*capture log close
set more off
*set min_memory 4g
*set segmentsize 4g
*set maxvar 25000
*set excelxlsxlargefile on

// Set Useful Folder Paths ("Globals")
global main "Q:\7880-Duke Energy Portfolio Evaluation\_Secure Data\7-Non-Residential Prescriptive\DEC-DEP 2019"
global work "$main\B-Survey Data\Midstream\Working Files"
global map "$main\B-Survey Data\Midstream\Mapping Files"
global clean "$main\B-Survey Data\Midstream\Analysis Files"
global raw "$main\B-Survey Data\Midstream\Raw Files"
global sample "$main\B-Sampling\Participant Surveys"
global syntax "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\Data Cleaning\Syntax"
exit


***************
***FILE PREP***
***************

/*            /////NOT NECESSARY, ALREADY HAVE ALL VALID RESPONSES IN SINGLE CLOSEOUT DATA FILE//////
***Import final survey data (excludes data from before reminder went out with wrong link and ODCIDs were changed)***
import delimited using "$raw\Duke Energy Midstream Participant Full Survey Data.csv", clear
gen double odcid = sample_id-40000
order odcid
dropempty
save "$work/DEC-DEP Midstream Survey_temporary1.dta", replace

***Import completes from before the reminder with the wrong links***
import delimited using "$raw\Duke Energy Midstream Participant survey completes from 9.27.2019 to 9.30.2019.csv", clear
rename sample_id odcid
order odcid
keep if odcid == 13066
dropempty
save "$work/DEC-DEP Midstream Survey_temporary2.dta", replace

***Combine all data from before and after ODCIDs changed
use "$work/DEC-DEP Midstream Survey_temporary1.dta", clear
append using "$work/DEC-DEP Midstream Survey_temporary2.dta"

//Check for anyone who shows up more than once--drop those who completed in the window 
dupit odcid, keep bro
sort odcid end_time
drop if odcid == odcid[_n+1]
*/

//Get final raw data
import delimited using "$raw\Duke Energy Midstream Participant Full Survey Data.csv", clear

***Dropping and re-ordering variables***
drop ïxid start_device start_apple_device sample_pii_program1 sample_pii_program2 sample_pii_distributor sample_pii_date sample_pii_company sample_pii_name sample_pii_address ///
	program1 program2 language uas_start last_activity_time start_browser_type_version start_os_version uas_end end_browser_type_version end_os_version sample_provider_id reentry duration
rename (sample_pii_email name_pii address_pii sample_id) (email name address odcid)
order odcid name email 
save "$work/DEC-DEP Midstream Survey_RAW.dta", replace

***Import the final field tracker***
import excel using "$sample\DEC-DEP Midstream Survey Sample_FINAL_Fielding Tracker_2019-10-10.xlsx", clear firstrow sheet ("Tracking")
rename *, lower
gen bounced = 1 if bounced_20190926 == 1
	replace bounced = 1 if bounced_2019101 == 1
	replace bounced = 1 if bounced_20191008 == 1
keep odcid_v2 email name acct_id bounced donotcontact_20191010 autoreply_20191010 stratum priority wave company
rename (odcid_v2 donotcontact_20191010 autoreply_20191010) (odcid do_not_contact autoreply)
order odcid name email acct_id bounced autoreply do_not_contact
save "$work/DEC-DEP Midstream Survey_Fielding Tracker.dta", replace

***Merge the field tracker and survey data***
use "$work/DEC-DEP Midstream Survey_Fielding Tracker.dta", clear
merge 1:1 odcid using "$work/DEC-DEP Midstream Survey_RAW.dta", nogen
save "$work/DEC-DEP Midstream Survey_RAW_Tracker.dta", replace



*******************
***DISPO SUMMARY***
*******************

//Dispo summary coding 
use "$work/DEC-DEP Midstream Survey_RAW_Tracker.dta", clear
gen dispo = "Complete" if terminate_location == 0
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == 2
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -302
replace dispo = "Partial complete - survey eligibility unknown" if terminate_location == -1
replace dispo = "Partial complete - survey eligibility confirmed" if terminate_location == -2
replace dispo = "Screened out - no knowledgeable contact" if terminate_location == 1
replace dispo = "Screened out - no knowledgeable contact" if sc2 == 1 & sc3_pii_1_1 == ""
replace dispo = "Bounced" if bounced == 1 & dispo == ""
replace dispo = "No response" if autoreply == 1 & dispo == ""
replace dispo = "No response" if dispo == ""
replace dispo = "Added to DNC list" if do_not_contact == 1

//Dispo category coding
gen dispo_category = "I" if dispo == "Complete"
replace dispo_category = "N" if dispo == "Partial complete - survey eligibility confirmed"
replace dispo_category = "U1" if dispo == "Partial complete - survey eligibility unknown"
replace dispo_category = "U1" if dispo == "No response"
replace dispo_category = "U1" if dispo == "Added to DNC list"
replace dispo_category = "X1" if dispo == "Screened out - no knowledgeable contact"
replace dispo_category = "X2" if dispo == "Bounced"
drop if odcid > 999999
save "$work/DEC-DEP Midstream Survey_Dispos.dta", replace

//Exporting dispo summary
use "$work/DEC-DEP Midstream Survey_Dispos.dta", clear
order name email acct_id company dispo 
keep name - dispo
export excel using "Q:\7880-Duke Energy Portfolio Evaluation\7-Non-Residential Prescriptive\5 - DEC-DEP 2019\B-Process Data Collection\3_Midstream Part Survey\Dispo Summary\DEC-DEP Midstream Survey Dispo Summary.xlsx", firstrow(variable)



***************
***DATA PREP***
***************

use "$work/DEC-DEP Midstream Survey_Dispos.dta", clear
keep if dispo == "Complete" | sat3 != ""

//Incorporating DK/NA flags
*br *98* *96* *998* *9998* to see what variables need to be groupped together
replace i7_0 = "Unsure" if i7_98 == 1
rename i7_0 i7
drop i7_98

replace n5b_1 = 998 if n5b_998 == 1
rename n5b_1 n5b
drop n5b_998

replace sp3b_1_1 = 998 if sp3b_1_998 == 1
rename sp3b_1_1 sp3b_1
drop sp3b_1_998

replace sp3b_2_1 = 998 if sp3b_2_998 == 1
rename sp3b_2_1 sp3b_2
drop sp3b_2_998

replace sp3b_3_1 = 998 if sp3b_3_998 == 1
rename sp3b_3_1 sp3b_3
drop sp3b_3_998

replace sp3b_4_1 = 998 if sp3b_4_998 == 1
rename sp3b_4_1 sp3b_4
drop sp3b_4_998

replace sp3b_5_1 = 998 if sp3b_5_998 == 1
rename sp3b_5_1 sp3b_5
drop sp3b_5_998

replace sp3b_6_1 = 998 if sp3b_6_998 == 1
rename sp3b_6_1 sp3b_6
drop sp3b_6_998

replace sp3b_7_1 = 998 if sp3b_7_998 == 1
rename sp3b_7_1 sp3b_7
drop sp3b_7_998

replace f3a_1 = 9998 if f3a_9998 == 1
rename f3a_1 f3a
drop f3a_9998

replace sp4ff_1_1_1 = "Unsure" if sp4ff_1_2_98 == 1
rename sp4ff_1_1_1 sp4ff_1
drop sp4ff_1_2_98

replace sp4ff_2_1_1 = "Unsure" if sp4ff_2_2_98 == 1
rename sp4ff_2_1_1 sp4ff_2
drop sp4ff_2_2_98 

replace sp4ff_3_1_1 = "Unsure" if sp4ff_3_2_98 == 1
rename sp4ff_3_1_1 sp4ff_3
drop sp4ff_3_2_98    

replace sp4ff_4_1_1 = "Unsure" if sp4ff_4_2_98 == 1
rename sp4ff_4_1_1 sp4ff_4
drop sp4ff_4_2_98  

replace sp4ff_5_1_1 = 98 if sp4ff_5_2_98 == 1
rename sp4ff_5_1_1 sp4ff_5
drop sp4ff_5_2_98  

replace sp4ff_6_1_1 = 98 if sp4ff_6_2_98 == 1
rename sp4ff_6_1_1 sp4ff_6
drop sp4ff_6_2_98

replace sp4ff_7_1_1 = 98 if sp4ff_7_2_98 == 1
rename sp4ff_7_1_1 sp4ff_7
drop sp4ff_7_2_98 

replace sp4ff_8_1_1 = 98 if sp4ff_8_2_98 == 1
rename sp4ff_8_1_1 sp4ff_8
drop sp4ff_8_2_98    

replace sp4ff_9_1_1 = 98 if sp4ff_9_2_98 == 1
rename sp4ff_9_1_1 sp4ff_9
drop sp4ff_9_2_98  

replace sp4ff_10_1_1 = 98 if sp4ff_10_2_98 == 1
rename sp4ff_10_1_1 sp4ff_10
drop sp4ff_10_2_98  

replace sp4ff_11_1_1 = 98 if sp4ff_11_2_98 == 1
rename sp4ff_11_1_1 sp4ff_11
drop sp4ff_11_2_98

drop sc3_pii_96_1



//Export for backcoding
export excel using "$map\DEC-DEP Midstream Backcoding.xlsx", sheetreplace firstrow(variable) sheet("Original data")

//After the team manually recoded open end responses, we're importing recoded data back
import excel using "$map\DEC-DEP Midstream Backcoding_2019-10-18.xlsx", clear firstrow sheet("Recoded data")
save "$work/DEC-DEP Midstream Survey_Backcoded_Recoded.dta", replace

//Final formatting
import excel using "$sample\Working Files\DEC-DEP Midstream Survey Sample_FINAL_ALL.xlsx", clear firstrow
keep odcid DATE_PUR KWH NAME email proj_tech
rename (odcid NAME DATE_PUR KWH) (odcid_original name purchase_date ex_ante_savings)
order odcid_original name email
sort odcid_original
merge 1:1 name email using "$work/DEC-DEP Midstream Survey_Backcoded_Recoded.dta", nogen
drop if acct_id == ""
order odcid_original odcid proj_tech acct_id name email company 
*sort odcid_original
save "$work/DEC-DEP Midstream Survey_Final.dta", replace



******************************
***DATA EXPORT FOR FR/SO/PROCESS ANALYSIS***
******************************

//Final prep

use "$work/DEC-DEP Midstream Survey_Final.dta", clear

gen f3a_new_fl = 1 if f3a < 50 & f3a > 0
replace f3a_new_fl = 2 if f3a > 49 & f3a < 9998
replace f3a_new_fl = 1 if f3b < 3
replace f3a_new_fl = 2 if f3b > 2 & f3b < 8
replace f3a_new_fl = . if f3a == .
replace f3a_new_fl = . if f3a == 0 
replace f3a_new_fl = . if f3b == 8
order f3a_new_fl, after(f3b)

gen strata = 1 if stratum == "DEC-Mid1" | stratum == "DEP-Mid1"
replace strata = 2 if stratum == "DEC-Mid2" | stratum == "DEP-Mid2" 
replace strata = 3 if stratum == "DEC-Mid3" | stratum == "DEP-Mid3"

gen jurisdiction = 1 if stratum == "DEC-Mid1" 
replace jurisdiction = 1 if stratum == "DEC-Mid2"
replace jurisdiction = 1 if stratum == "DEC-Mid3"
replace jurisdiction = 2 if stratum == "DEP-Mid1"
replace jurisdiction = 2 if stratum == "DEP-Mid2"
replace jurisdiction = 2 if stratum == "DEP-Mid3"

gen weight = 0.35 if stratum == "DEC-Mid3" 
replace weight = 0.73 if stratum == "DEC-Mid2" 
replace weight = 2.5 if stratum == "DEC-Mid1"
replace weight = 0.30 if stratum == "DEP-Mid3" 
replace weight = 0.39 if stratum == "DEP-Mid2"
replace weight = 0.63 if stratum == "DEP-Mid1"

drop sample_pii_tech sample_pii_meas_a sample_pii_qty_a sample_pii_meas_b sample_pii_qty_b sample_pii_meas_c sample_pii_qty_c sample_pii_qty_d sample_pii_meas_e sample_pii_qty_e ///
	sample_pii_meas_d date wave tech dispo_category bounced autoreply do_not_contact priority sample_pii_original_odcid distributor address_pii terminate_location
	
save "$work/DEC-DEP Midstream Survey_Final Export.dta", replace


//Export for WINCROSS
use "$work/DEC-DEP Midstream Survey_Final Export.dta", clear
keep if dispo == "Complete"
drop acct_id name email company start_time end_time dispo
order stratum, before(strata)
export excel using "$clean\DEC-DEP Midstream Survey_WINCROSS Export.xlsx", replace firstrow(variable)


//Export for SO analysis
use "$work/DEC-DEP Midstream Survey_Final Export.dta", clear
drop if sp0 == .
drop acct_id name email company purchase_date stratum start_time end_time adjustedduration dispo strata jurisdiction weight passedfirstscreen sc1 sc2 sc3_pii_1_1 sc3_pii_1_2 ///
	quota_check i1_a i2_a i1_b i2_b i1_c i2_c i1_d i2_d i1_e i2_e vqty_1 vqty_2 vqty_3 vqty_4 vqty_5 i3_a i4_a i3_b i4_b i3_c i4_c i3_d i4_d i3_e i4_e iqty_1 iqty_2 iqty_3 iqty_4 ///
	iqty_5 i5_a i6_a i5_b i6_b i5_c i6_c i5_d i6_d i5_e i6_e fqty_1 fqty_2 fqty_3 fqty_4 fqty_5 i7 i8_a i8_a_0_other i8_b i8_b_0_other i8_c i8_c_0_other i8_d i8_d_0_other i8_e ///
	i8_e_0_other i9_a i9_b i9_c i9_d i9_e i10 v1 v1a v1b v2 v2_rec v2_0_other v3 n1a n1b n2 n2_rec n2_0_other n3_a n3_b n3_c1 n3_c2 n3_d n3_e n3_f n3_g n3_h n3_i n3_j n3_k n3_o ///
	n3oo n3dx n3ix n3ix_rec n3ix_0_other n3jx n3jx_rec n3jx_0_other n4 n5a n5b n_install n5c n6 n6a n6b cc1a cc1b n3b_new n4_new sat1_a sat1_b sat1_c sat1_d sat2a sat2b sat2c ///
	sat2d sat3 f1 f1_rec f1_0_other f2 f3a f3b f3a_new_fl f4 f4_rec f4_0_other
export excel using "$clean\DEC-DEP Midstream Survey_SO Export.xlsx", replace firstrow(variable)


//Export for FR analysis
use "$work/DEC-DEP Midstream Survey_Final Export.dta", clear
keep odcid stratum name email sample_pii_tech sample_pii_meas_a sample_pii_meas_b sample_pii_meas_c sample_pii_meas_d date v1 v1a v1b v2_rec v2_0_other v3 n1a n1b n2_rec n2_0_other n3_a ///
	 n3_b n3_c1 n3_c2 n3_d n3_e n3_f n3_g n3_h n3_i n3_j n3_k n3_o n3oo n3dx n3ix_rec n3ix_0_other n3jx_rec n3jx_0_other n4 n5a n5b n_install n5c n6 n6a n6b cc1a cc1b n3b_new n4_new
rename (sample_pii_tech sample_pii_meas_a sample_pii_meas_b sample_pii_meas_c sample_pii_meas_d) (tech meas1 meas2 meas3 meas4)
export excel using "$clean\DEC-DEP Midstream Survey_FR Export.xlsx", replace firstrow(variable)


//Export final data
use "$work/DEC-DEP Midstream Survey_Final Export.dta", clear
export excel using "$clean\DEC-DEP Midstream Survey_Final Data.xlsx", firstrow(variable)


******************************
***IN-SERVICE RATE ANALYSIS***
******************************

use "$work/DEC-DEP Midstream Survey_Final Export.dta", clear
drop if i10 == .

//Creating new measure and quantity variables for more clarity
rename (meas_1 meas_2 meas_3 meas_4 meas_5) (meas_a meas_b meas_c meas_d meas_e) 

clonevar qty_a = qty_1
clonevar qty_b = qty_2
clonevar qty_c = qty_3
clonevar qty_d = qty_4
clonevar qty_e = qty_5

clonevar real_qty_a = vqty_1
clonevar real_qty_b = vqty_2
clonevar real_qty_c = vqty_3
clonevar real_qty_d = vqty_4
clonevar real_qty_e = vqty_5

clonevar install_qty_a = iqty_1
clonevar install_qty_b = iqty_2
clonevar install_qty_c = iqty_3
clonevar install_qty_d = iqty_4
clonevar install_qty_e = iqty_5

clonevar remain_qty_a = fqty_1
clonevar remain_qty_b = fqty_2
clonevar remain_qty_c = fqty_3
clonevar remain_qty_d = fqty_4
clonevar remain_qty_e = fqty_5

order qty_a - qty_e, after(qty_5)
order real_qty_a - real_qty_e, after(vqty_5)
order install_qty_a - install_qty_e, after(iqty_5)
order remain_qty_a - remain_qty_e, after(fqty_5)


*Measure A
//Checking to make sure the survey calculated measure quantities correctly 
assert qty_a == real_qty_a if i1_a == 1
assert real_qty_a == i2_a if i1_a == 2 & i1_a != .

//Replacing the values
replace real_qty_a = . if i1_a == 98 & i2_a == .
replace real_qty_a = qty_a if real_qty_a > qty_a & real_qty_a != .

replace install_qty_a = real_qty_a if i3_a == 1
replace install_qty_a = . if real_qty_a == . | real_qty_a == 0
replace install_qty_a = real_qty_a if install_qty_a > real_qty_a & install_qty_a != .
replace install_qty_a = . if i3_a == 98

replace remain_qty_a = . if install_qty_a == .
replace remain_qty_a = . if i5_a == 98

*Measure B
//Checking to make sure the survey calculated measure quantities correctly 
assert qty_b == real_qty_b if i1_b == 1
assert real_qty_b == i2_b if i1_b == 2 & i1_b != .

//Replacing the values
replace real_qty_b = . if i1_b == 98 & i2_b == .
replace real_qty_b = qty_b if real_qty_b > qty_b & real_qty_b != .

replace install_qty_b = real_qty_b if i3_b == 1
replace install_qty_b = . if real_qty_b == . | real_qty_b == 0
replace install_qty_b = real_qty_b if install_qty_b > real_qty_b & install_qty_b != .
replace install_qty_b = . if i3_b == 98

replace remain_qty_b = . if install_qty_b == .
replace remain_qty_b = . if i5_b == 98

*Measure C
//Checking to make sure the survey calculated measure quantities correctly 
assert qty_c == real_qty_c if i1_c == 1
assert real_qty_c == i2_c if i1_c == 2 & i1_c != .

//Replacing the values
replace real_qty_c = . if i1_c == 98 & i2_c == .
replace real_qty_c = qty_c if real_qty_c > qty_c & real_qty_c != .

replace install_qty_c = real_qty_c if i3_c == 1
replace install_qty_c = . if real_qty_c == . | real_qty_c == 0
replace install_qty_c = real_qty_c if install_qty_c > real_qty_c & install_qty_c != .
replace install_qty_c = . if i3_c == 98

replace remain_qty_c = . if install_qty_c == .
replace remain_qty_c = . if i5_c == 98

*Measure D
//Checking to make sure the survey calculated measure quantities correctly 
assert qty_d == real_qty_d if i1_d == 1
assert real_qty_d == i2_d if i1_d == 2 & i1_d != .

//Replacing the values
replace real_qty_d = . if i1_d == 98 & i2_d == .
replace real_qty_d = qty_d if real_qty_d > qty_d & real_qty_d != .

replace install_qty_d = real_qty_d if i3_d == 1
replace install_qty_d = . if real_qty_d == . | real_qty_d == 0
replace install_qty_d = real_qty_d if install_qty_d > real_qty_d & install_qty_d != .
replace install_qty_d = . if i3_d == 98

replace remain_qty_d = . if install_qty_d == .
replace remain_qty_d = . if i5_d == 98

*Measure E
//Checking to make sure the survey calculated measure quantities correctly 
assert qty_e == real_qty_e if i1_e == 1
assert real_qty_e == i2_e if i1_e == 2 & i1_e != .

//Replacing the values
replace real_qty_e = . if i1_e == 98 & i2_e == .
replace real_qty_e = qty_e if real_qty_e > qty_e & real_qty_e != .

replace install_qty_e = real_qty_e if i3_e == 1
replace install_qty_e = . if real_qty_e == . | real_qty_e == 0
replace install_qty_e = real_qty_e if install_qty_e > real_qty_e & install_qty_e != .
replace install_qty_e = . if i3_e == 98

replace remain_qty_e = . if install_qty_e == .
replace remain_qty_e = . if i5_e == 98



save "$work/DEC-DEP Midstream Survey_IRS.dta", replace
